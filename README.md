# Codegen PHP

Generador de código de la herramienta Dia para generar una aplicación de PHP puro, sin framework.

El script consiste en que solamente declarando los diagramas de clase de una aplicación, al exportarla con la herramienta dia, el script me genera toda mi aplicación PHP con:

- Arquitectura MVC
- Template adaptados de la plantila de Nice Admin de Bootstrap
- Todo el código es orientado a objetos
- Rutas amigables
- Seguridad en rutas con archivo .htaccess
- Capa de seguridad para que las peticiones solo sean manejadas por las rutas definidas
- Conexión PDO para poder conectarse a cualquier SGBD soportado por PDO (mysql, postgresql, entre otros)
- Login completo
- Matriz de roles de permisos
- Listar todos los registros
- Formulario de mis clases declaradas
- Crear registros de mis formularios
- Estructura de BD sencilla para solo correr el .sql
- Soporta los tipos de datos: Relacional (muchos a uno), Float, Integer, Select, Checkbox, Textarea, Text, Date, Datetime
- Todos los registros son listados dinámicamente con AJAX
- Todos los formularios son insertados dinámicamente con AJAX
- Los registros creados pueden ser eliminados dinámicamente con AJAX
- Los registros son editados dinámicamente con AJAX
- Validaciones de seguridad contra inyección sql, ataques XSS y CSRF
- Implementación frontend de los Elegant Icon Font
- Implementación de templates para errores 401 y 404.
- Títulos de las páginas manejados dinámicamente.
- Capacidad de manejar el order by de las tablas consultadas.
- Crear una estructura sólida de base de datos para Postgresql

**TO-DO:**

- [ ] 1-Crear bitácora al exportar

Para probar el script solo pega archivo codegen_php.py, una vez descargado el programa DIA, en:

1. Para Windows: C:\Archivos de Programa\Dia
1. Para distribuciones GNU/Linux: /usr/share/dia/python

**CONTACTO**

Si quieres apoyar esta iniciativa puedes realizar una donación a esta cuenta [Paypal](https://www.paypal.me/HidalgoF)

Si quieres participar en el repositorio, escríbeme al correo de fhidalgo.dev@gmail.com
