# -*- coding: utf-8 -*-
# =====================================================================
#                           CODEGEN PHP
#
# Autor:  Franyer Hidalgo - Venezuela (fhidalgo.dev@gmail.com)
# Fecha:  Agosto-Septiembre 2020
#
# Este script genera todo el CRUD de una aplicación web PHP con tan solo
# declarar los diagramas de clase de un programa DIA.
# =====================================================================
import sys, dia, os
import zipfile
#
# Este código está inspirado en el codegen.py
#


class Klass:
    def __init__(self, name):
        self.name = name
        self.attributes = []
        # a list, as java/c++ support multiple methods with the same name
        self.stereotype = ""
        self.operations = []
        self.comment = ""
        self.parents = []
        self.templates = []
        self.inheritance_type = ""

    def AddAttribute(self, name, type, visibility, value, comment):
        self.attributes.append((name, (type, visibility, value, comment)))

    def AddOperation(self, name, type, visibility, params, inheritance_type, comment, class_scope):
        self.operations.append((name, (type, visibility, params, inheritance_type, comment, class_scope)))

    def SetComment(self, s):
        self.comment = s

    def AddParent(self, parent):
        self.parents.append(parent)

    def AddTemplate(self, template):
        self.templates.append(template)

    def SetInheritance_type(self, inheritance_type):
        self.inheritance_type = inheritance_type


class ObjRenderer:
    "Implements the Object Renderer Interface and transforms diagram into its internal representation"

    def __init__(self):
        # an empty dictionary of classes
        self.klasses = {}
        self.klass_names = []   # store class names to maintain order
        self.arrows = []
        self.filename = ""

    def begin_render(self, data, filename):
        self.filename = filename
        for layer in data.layers:
            # for the moment ignore layer info. But we could use this to spread accross different files
            for o in layer.objects:
                if o.type.name == "UML - Class":

                    k = Klass(o.properties["name"].value)
                    k.SetComment(o.properties["comment"].value)
                    k.stereotype = o.properties["stereotype"].value
                    if o.properties["abstract"].value:
                        k.SetInheritance_type("abstract")
                    if o.properties["template"].value:
                        k.SetInheritance_type("template")
                    for op in o.properties["operations"].value:
                        # op : a tuple with fixed placing, see: objects/UML/umloperations.c:umloperation_props
                        # (name, type, comment, stereotype, visibility, inheritance_type, class_scope, params)
                        params = []
                        for par in op[8]:
                            # par : again fixed placement, see objects/UML/umlparameter.c:umlparameter_props
                            params.append((par[0], par[1]))
                        k.AddOperation(op[0], op[1], op[4], params, op[5], op[2], op[7])
                    # print o.properties["attributes"].value
                    for attr in o.properties["attributes"].value:
                        # see objects/UML/umlattributes.c:umlattribute_props
                        # print "    ", attr[0], attr[1], attr[4]
                        k.AddAttribute(attr[0], attr[1], attr[4], attr[2], attr[3])
                    self.klasses[o.properties["name"].value] = k
                    self.klass_names += [o.properties["name"].value]
                    # Connections
                elif o.type.name == "UML - Association":
                    # should already have got attributes relation by names
                    pass
                # other UML objects which may be interesting
                # UML - Note, UML - LargePackage, UML - SmallPackage, UML - Dependency, ...

        edges = {}
        for layer in data.layers:
            for o in layer.objects:
                for c in o.connections:
                    for n in c.connected:
                        if not n.type.name in ("UML - Generalization", "UML - Realizes"):
                            continue
                        if str(n) in edges:
                            continue
                        edges[str(n)] = None
                        if not (n.handles[0].connected_to and n.handles[1].connected_to):
                            continue
                        par = n.handles[0].connected_to.object
                        chi = n.handles[1].connected_to.object
                        if not par.type.name == "UML - Class" and chi.type.name == "UML - Class":
                            continue
                        par_name = par.properties["name"].value
                        chi_name = chi.properties["name"].value
                        if n.type.name == "UML - Generalization":
                            self.klasses[chi_name].AddParent(par_name)
                        else:
                            self.klasses[chi_name].AddTemplate(par_name)

    def end_render(self):
        # without this we would accumulate info from every pass
        self.attributes = []
        self.operations = {}


class PHPRenderer(ObjRenderer):
    def __init__(self):
        ObjRenderer.__init__(self)

    def data_get(self):
        return {
            'file': self.filename,
            'module': os.path.basename(self.filename).split('.')[-2]
        }

    def class_name(self, sk):
        """Gestiona la data de los nombres de las clases"""
        cname = sk.replace('.', ' ').replace('_', ' ')
        cname = cname.title().replace(' ', '')
        data = {'cname': cname,
                'lcname': sk.replace('.', '_').lower(),
                'ucname': sk.replace('.', '_').replace(' ', '_').upper(),
                'porcentaje': '%', }
        return data

    def format_attributes(self, value_field):
        """Formatea los atributos de las clases para que sean ensamblados
           en el código dinámicamente

           Parameters:
           :value_field (str): valor del atributo
        """
        value_attr = ['string', 'model', 'option_value', 'option_ver', 'options']
        value = value_field
        for i in value_attr:
            format_attr = "'%s'" % i
            value = value.replace(i, format_attr)
        value = value.replace('=', ':').replace(",", ", ").replace(",  ", ", ").replace("' ,", "',")
        value = '{%s}' % (value)
        attributes = eval(value)
        return attributes

    def content_form(self, campo, tipo, value, class_name, udp):
        """Formatea el campo de la clase para convertirlo en la vista
           HTML correspondiente

           Parameters:
           :field (str): nombre del campo
           :type_field (str): tipo de dato del campo
           :value (str): valor del campo
           :class_name (str): nombre de la clase
           :upd (boolean): si es la vista de edición se establece como True, en caso contrario y por defecto False
        """
        attributes = self.format_attributes(value)
        attributes.update({'campo': campo,
                           'tipo': tipo
                           })
        type_input = ['text', 'number', 'step', 'date', 'datetime-local', 'file', 'checkbox']
        if tipo != 'textarea':
            if not udp:
                result = """                        <div class="form-group col-lg-6">\n"""
            else:
                result = """                                <div class="form-group col-lg-6">\n"""
        else:
            if not udp:
                result = """                        <div class="form-group col-lg-12">\n"""
            else:
                result = """                                <div class="form-group col-lg-12">\n"""
        if not udp:
            result += """                            <label class="control-label" for="%(campo)s">%(string)s</label>\n""" % (attributes)
        else:
            result += """                                    <label class="control-label" for="%(campo)s">%(string)s</label>\n""" % (attributes)
        if not udp:
            if tipo in type_input:
                if tipo != 'step' and tipo != 'textarea' and tipo != 'select':
                    result += """                            <input type="%(tipo)s" id="%(campo)s" name="%(campo)s" class="form-control"/>\n""" % (attributes)
                elif tipo == 'step':
                    result += """                            <input type="number" id="%(campo)s" name="%(campo)s" step="any" class="form-control"/>\n""" % (attributes)
            elif tipo == 'textarea':
                result += """                            <textarea id="%(campo)s" name="%(campo)s" class="form-control"></textarea>\n""" % (attributes)
            elif tipo == 'Many2one':
                result += """                            <select name="%(campo)s" id="%(campo)s" class="form-control">
                            <option id="default" disabled selected>Selecciona una opción</option>
                            <?php for ($i=0; $i < sizeof($%(campo)s); $i++) {
                                echo '<option value="'.$%(campo)s[$i]->%(option_value)s.'">'.$%(campo)s[$i]->%(option_ver)s.'</option>';
                                }
                            ?>
                            </select>\n""" % (attributes)
            elif tipo == 'select':
                options = attributes.get('options')
                result += """                            <select name="%(campo)s" id="%(campo)s" class="form-control">
                            <option id="default" disabled selected>Selecciona una opción</option>\n""" % (attributes)
                for i in options:
                    attributes.update({
                        'value': i[0],
                        'ver': i[1]
                    })
                    result += """                            <option value="%(value)s">%(ver)s</option>\n""" % attributes
                result += """                            </select>\n"""
        else:
            attributes.update({'clase': class_name})
            if tipo in type_input:
                if tipo != 'step' and tipo != 'textarea' and tipo != 'select' and tipo != 'checkbox':
                    result += """                                    <input type="%(tipo)s" id="%(campo)s" name="%(campo)s" readonly disabled value="<?php print $%(clase)s[0]->%(campo)s ?>" class="form-control"/>\n""" % (attributes)
                elif tipo == 'step':
                    result += """                                    <input type="number" id="%(campo)s" name="%(campo)s" step="any" readonly disabled value="<?php print $%(clase)s[0]->%(campo)s ?>" class="form-control"/>\n""" % (attributes)
                elif tipo == 'checkbox':
                    result += """                                    <input type="%(tipo)s" id="%(campo)s" name="%(campo)s" readonly disabled <?php if ($%(clase)s[0]->%(campo)s == true) print 'checked="checked"' ?> class="form-control"/>\n""" % (attributes)
            elif tipo == 'textarea':
                result += """                                    <textarea id="%(campo)s" name="%(campo)s" readonly disabled class="form-control"><?php print $%(clase)s[0]->%(campo)s ?></textarea>\n""" % (attributes)
            elif tipo == 'Many2one':
                result += """                                        <select name="%(campo)s" id="%(campo)s"  readonly disabled class="form-control">
                                        <option id="default" disabled selected>Selecciona una opción</option>
                                        <?php for ($i=0; $i < sizeof($%(campo)s); $i++) {
                                        if ($%(clase)s[0]->%(campo)s == $%(campo)s[$i]->id)
                                            {$selected = 'selected';}
                                        else {$selected = '';}
                                        echo '<option value="'.$%(campo)s[$i]->%(option_value)s.'"'.$selected.'>'.$%(campo)s[$i]->%(option_ver)s.'</option>';
                                        }
                                        ?>
                                        </select>\n""" % (attributes)
            elif tipo == 'select':
                options = attributes.get('options')
                result += """                                    <select name="%(campo)s" id="%(campo)s" readonly disabled class="form-control">
                                        <option id="default" disabled selected>Selecciona una opción</option>\n""" % (attributes)
                for i in options:
                    attributes.update({
                        'value': i[0],
                        'ver': i[1]
                    })
                    result += """                                        <option value="%(value)s" <?php if ($%(clase)s[0]->%(campo)s == '%(value)s'){print 'selected';} ?>>%(ver)s</option>\n""" % attributes
                result += """                                    </select>\n"""
        if not udp:
            result += """                        </div>\n"""
        else:
            result += """                                </div>\n"""
        return result

    def atributte_format(self, field, type_field, value, class_name, udp=False):
        """Formatea el campo de la clase para convertirlo en la vista
           HTML correspondiente

           Parameters:
           :field (str): nombre del campo
           :type_field (str): tipo de dato del campo
           :value (str): valor del campo
           :class_name (str): nombre de la clase
           :upd (boolean): si es la vista de edición se establece como True, en caso contrario y por defecto False
        """
        type_html = dict(Char='text',
                         Text='textarea',
                         Integer='number',
                         Float='step',
                         Date='date',
                         Datetime='datetime-local',
                         Binary='file',
                         Boolean='checkbox',
                         Select='select',
                         )
        for i in type_html.keys():
            if i == type_field:
                type_field = type_html.get(i)
                return self.content_form(field, type_field, value, class_name, udp)
            elif type_field == 'Many2one':
                return self.content_form(field, type_field, value, class_name, udp)

    def attributes_controller(self, attributes, data, attr_type, attr_m2o, field):
        """Formatea los atributos de las clases de los controladores PHP

           Parameters:
           :attributes (dict): valor del atributo
           :data (str): valor del atributo
           :attr_type (str): valor del atributo
           :attr_m2o (str): valor del atributo
           :field (str): valor del atributo
        """
        record_m2o = {}
        if 'model' in attributes:
            model = attributes.get('model').replace('.', ' ').replace('_', ' ').title().replace(' ', '')
            attributes.update({'cname': data.get('cname'),
                               'lcname': data.get('lcname'),
                               })
        if attr_type == 'Many2one':
            attr_m2o = True
            record_m2o.update({
                field: [field.title().replace('_', ''), model]
            })
        return attributes, attr_m2o, record_m2o

# ==================================================================
#
#  AQUI SE DEFINEN LOS ARCHIVOS QUE SE GENERAN EN LA RUTA app/core
#
# ==================================================================

    def connection_BD(self):
        """Retorna el contenido del archivo de configuración de conección
           PDO de PHP con cualquier Sistema Gestor de Base de Datos"""

        result = """<?php

class Conexion {

    private $conexion;
    private $configuracion = [
        "driver"   => "",  // pgsql-mysql
        "host"     => "localhost",
        "database" => "name_database",
        "port"     => "3306",
        "username" => "root",
        "password" => "123456",
        "charset"  => "utf8mb4"
    ];

    public function __construct() {

    }

    public function conectar() {
        try {
            $CONTROLADOR = $this->configuracion["driver"];
            $SERVIDOR = $this->configuracion["host"];
            $BASE_DATOS = $this->configuracion["database"];
            $PUERTO = $this->configuracion["port"];
            $USUARIO = $this->configuracion["username"];
            $CLAVE = $this->configuracion["password"];
            $CODIFICACION = $this->configuracion["charset"];
            if ($CONTROLADOR === "pgsql") {
                $url = "{$CONTROLADOR}:host={$SERVIDOR};port={$PUERTO};"
                    ."dbname={$BASE_DATOS}";
            }
            else {
                $url = "{$CONTROLADOR}:host={$SERVIDOR}:{$PUERTO};"
                    ."dbname={$BASE_DATOS};charset={$CODIFICACION}";
            }
            //Se crea la conexión.
            $this->conexion = new PDO($url, $USUARIO, $CLAVE);
            return $this->conexion;
        }
        catch (Exception $excepcion) {
            print $excepcion->getTraceAsString();
        }
    }

}

?>\n"""
        return result

    def crud(self):
        """Retorna el contenido del archivo CRUD de PHP que implementa
           la conexión a la tabla de cada modelo y un ORM básico"""

        result = """<?php

class Crud {

    protected $tabla;
    protected $order_by;
    protected $conexion;
    protected $wheres = "";
    protected $esquema = "";
    protected $pgsql = false;
    protected $sql = null;

    public function __construct($tabla = null, $order_by = "id DESC") {
        $this->conexion = (new Conexion())->conectar();
        $this->order_by = $order_by;
        if ($this->esquema == "") {
            $this->tabla = $tabla;
        }
        else {
          $this->tabla = $this->esquema . "." . $tabla;
        }
    }

    public function get() {
        try {
            $this->sql = "SELECT * FROM {$this->tabla} {$this->wheres} ORDER BY {$this->order_by}";
            $transaccion = $this->conexion->prepare($this->sql);
            $transaccion->execute();
            return $transaccion->fetchAll(PDO::FETCH_OBJ);
        }
        catch (Exception $exepcion) {
            print $exepcion->getTraceAsString();
        }
    }

    public function insert($objeto) {
        try {
            $objeto = $this->parsearValor($objeto);
            $campos = ($this->pgsql == false) ? implode("`, `", array_keys($objeto))
                                              : implode(", ", array_keys($objeto));
            $valores = ":" . implode(", :", array_keys($objeto));
            $this->sql = ($this->pgsql == false) ? "INSERT INTO {$this->tabla} (`{$campos}`) VALUES ({$valores})"
                                                 : "INSERT INTO {$this->tabla} ({$campos}) VALUES ({$valores})";
            $this->ejecutar($objeto);
            $id = $this->conexion->lastInsertId();
            return $id;
        }
        catch (Exception $exepcion) {
            echo $exepcion->getTraceAsString();
        }
    }

    public function update($objeto) {
        try {
            $objeto = $this->parsearValor($objeto);
            $campos = "";
            foreach ($objeto as $llave => $valor) {
                $campos .= ($this->pgsql == false) ? "`$llave`=:$llave,"
                                                   : "$llave=:$llave,";
            }
            $campos = rtrim($campos, ",");
            $this->sql = "UPDATE {$this->tabla} SET {$campos} {$this->wheres}";
            $filasAfectadas = $this->ejecutar($objeto);
            return $filasAfectadas;
        }
        catch (Exception $exepcion) {
            echo $exepcion->getTraceAsString();
        }
    }

    public function delete() {
        try {
            $this->sql = "DELETE FROM {$this->tabla} {$this->wheres}";
            $filesAfectadas = $this->ejecutar();
            return $filesAfectadas;
        }
        catch (Exception $exepcion) {
            echo $exepcion->getTraceAsString();
        }
    }

    public function where($llave, $condicion, $valor) {
        $this->wheres .= (strpos($this->wheres, "WHERE")) ? " AND " : " WHERE ";
        $this->wheres .= ($this->pgsql == false)
                         ? "`$llave` $condicion " . ((is_string($valor)) ? "'" . $valor . "'" : $valor) . " "
                         : "$llave $condicion " . ((is_string($valor)) ? "'" . $valor . "'" : $valor) . " ";
        return $this;
    }

    public function orWhere($llave, $condicion, $valor) {
        $this->wheres .= (strpos($this->wheres, "WHERE")) ? " OR " : " WHERE ";
        $this->wheres .= ($this->pgsql == false)
                         ? "`$llave` $condicion " . ((is_string($valor)) ? "'" . $valor . "'" : $valor) . " "
                         : "$llave $condicion " . ((is_string($valor)) ? "'" . $valor . "'" : $valor) . " ";
        return $this;
    }

    private function ejecutar($objeto = null) {
        $transaccion = $this->conexion->prepare($this->sql);
        if ($objeto !== null) {
            foreach ($objeto as $llave => $valor) {
                if (empty($valor)) {
                    $valor = NULL;
                }
                $valor = xssInput::validacionXss($valor);
                $transaccion->bindValue(":$llave", $valor);
            }
        }
        $transaccion->execute();
        $this->reiniciarValores();
        return $transaccion->rowCount();
    }

    public function first() {
        $lista = $this->get();
        if(count($lista) > 0) {
            return $lista[0];
        }
        else {
            return null;
        }
    }

    private function reiniciarValores() {
        $this->wheres = "";
        $this->sql = null;
    }

    public function parsearValor($objeto) {
        foreach ($objeto as $key => $value) {
            if ($value == "") {
                unset($objeto[$key]);
            }
        }
        return $objeto;
    }

}

?>\n"""
        return result

    def generic_model(self):
        """Retorna el contenido del archivo modelogenerico de PHP que extiende
           las funcionalidades de la clase CRUD para manejar los métodos públicos
           en los modelos"""

        result = """<?php

class ModeloGenerico extends Crud {

    private $className;
    private $excluir = ["className", "tabla", "conexion", "wheres", "sql", "excluir", "order_by"];

    function __construct($tabla, $className, $propiedades = null, $order_by = "id DESC") {
        parent::__construct($tabla, $order_by);
        $this->className = $className;

        if (empty($propiedades)) {
            return;
        }

        foreach ($propiedades as $llave => $valor) {
            $this->{$llaves} = $valor;
        }
    }

    protected function obtenerAtributos() {
        $variables = get_class_vars($this->className);
        $atributos = [];
        $max = count($variables);
        foreach ($variables as $llave => $valor) {
            if (!in_array($llave, $this->excluir)) {
                $atributos[] = $llave;
            }
        }
        return $atributos;
    }

    protected function parsear($objeto = null) {
        try {
            $atributos = $this->obtenerAtributos();
            $objetoFinal = [];
            // Obtenes el objeto desde el modelo.
            if ($objeto == null) {
                foreach ($atributos as $indice => $llave) {
                    if (isset($this->{$llave})) {
                        $objetoFinal[$llave] = $this->{$llave};
                    }
                }
                return $objetoFinal;
            }

            // Corregir el objeto que recibimos con los atributos del modelo.
            foreach ($atributos as $indice => $llave) {
                if (isset($objeto[$llave])) {
                    $objetoFinal[$llave] = $objeto[$llave];
                }
            }
            return $objetoFinal;
        }
        catch (Exception $excepcion) {
            throw new Exception("Error en " . $this->className . ".parsear() => " . $excepcion->getMessage());
        }
    }

    public function fill($objeto) {
        try {
            $atributos = $this->obtenerAtributos();
            foreach ($atributos as $indice => $llave) {
                if (isset($objeto[$llave])) {
                    $this->{$llave} = $objeto[$llave];
                }
            }
        }
        catch (Exception $excepcion) {
            throw new Exception("Error en " . $this->className . ".fill() => " . $excepcion->getMessage());
        }
    }

    public function insert($objeto = null) {
        $objeto = $this->parsear($objeto);
        return parent::insert($objeto);
    }

    public function update($objeto) {
        $objeto = $this->parsear($objeto);
        return parent::update($objeto);
    }

    public function __get($nombreAtributo) {
        return $this->{$nombreAtributo};
    }

    public function __set($nombreAtributo, $valor) {
        $this->{$nombreAtributo} = $valor;
    }

}

?>\n"""
        return result

    def emensajes_get(self):
        """Retorna el archivo de PHP que maneja los mensajes al cliente"""
        return """<?php

class EMensajes {

    const CORRECTO = "CORRECTO";
    const ERROR = "ERROR";
    const LOGIN_EXITOSO = "LOGIN_EXITOSO";
    const INSERCION_EXITOSA = "INSERSION_EXITOSA";
    const ACTUALIZACION_EXITOSA = "ACTUALIZACION_EXITOSA";
    const ELIMINACION_EXITOSA = "ELIMINACION_EXITOSA";
    const LOGIN_FALLIDO = "LOGIN_FALLIDO";
    const CAMPOS_VACIOS = "CAMPOS_VACIOS";
    const ERROR_CORREO = "ERROR_CORREO";
    const USUARIO_INACTIVO = "USUARIO_INACTIVO";
    const SIN_PERMISOS = "SIN_PERMISOS";
    const ERROR_INSERSION = "ERROR_INSERSION";
    const ERROR_ACTUALIZACION = "ERROR_ACTUALIZACION";
    const ERROR_ELIMINACION = "ERROR_ELIMINACION";
    const NO_HAY_REGISTROS = "NO_HAY_REGISTROS";
    const ERROR_CONEXION_BD = "ERROR_CONEXION_BD";

    public static function getMensaje($codigo) {
        switch ($codigo) {
            case EMensajes::CORRECTO:
                return new Respuesta(1, "Se ha realizado la operación de manera correcta.");
            case EMensajes::INSERCION_EXITOSA:
                return new Respuesta(1, "Se ha insertado el registro con éxito.");
            case EMensajes::LOGIN_EXITOSO:
                return new Respuesta(1, "Datos correctos, puede continuar.");
            case EMensajes::ACTUALIZACION_EXITOSA:
                return new Respuesta(1, "Se ha actualizado el registro con éxito.");
            case EMensajes::ELIMINACION_EXITOSA:
                return new Respuesta(1, "Se ha eliminado el registro con éxito.");
            case EMensajes::ERROR_INSERSION:
                return new Respuesta(-1, "Se ha producido un error al insertar el registro.");
            case EMensajes::ERROR:
                return new Respuesta(-1, "Se ha producido un error al realizar la operación.");
            case EMensajes::LOGIN_FALLIDO:
                return new Respuesta(-1, "Usuario y/o contraseña incorrectos.");
            case EMensajes::CAMPOS_VACIOS:
                return new Respuesta(-1, "Por favor, rellene todos los campos.");
            case EMensajes::ERROR_CORREO:
                return new Respuesta(-1, "Correo electrónico inválido.");
            case EMensajes::SIN_PERMISOS:
                return new Respuesta(-1, "No tienes permisos suficientes para realizar esta acción.");
            case EMensajes::USUARIO_INACTIVO:
                return new Respuesta(-1, "Usuario inactivo, comuníquese con su administrador.");
            case EMensajes::ERROR_ACTUALIZACION:
                return new Respuesta(-1, "Se ha producido un error al actualizar el registro.");
            case EMensajes::ERROR_ELIMINACION:
                return new Respuesta(-1, "Se ha producido un error al eliminar el registro.");
            case EMensajes::NO_HAY_REGISTROS:
                return new Respuesta(0, "No hay registros.");
            case EMensajes::ERROR_CONEXION_BD:
                return new Respuesta(-1, "Error al conectar con la base de datos.");
        }
    }

}
?>\n"""

    def rec_name_get(self):
        """Retorna el archivo de PHP que cambia la descripción de los campos relacionales
           en una vista lista"""
        return """<?php

class recName {

    function __construct() {
    }

    public static function oneName($registro, $campos_id, $models) {
        for ($i=0; $i < sizeof($registro); $i++) {
            foreach ($campos_id as $clave => $valor) {
                if ($registro[$i]->{$clave} != null OR $registro[$i]->{$clave} == "") {
                    $campo_id = $registro[$i]->{$clave};
                    foreach ($models as $campo => $model) {
                        for ($j=0; $j < sizeof($model); $j++) {
                            if (property_exists($model[$j], $valor) && $model[$j]->id == $campo_id && $clave == $campo) {
                                $registro[$i]->{$clave} = $model[$j]->{$valor};
                            }
                        }
                    }
                }
                else {
                    $registro[$i]->{$clave} = "";
                }
            }
        }
        return $registro;
    }

    public static function parsearDatos($registro) {
        for ($i=0; $i < sizeof($registro); $i++) {
            foreach ($registro[$i] as $clave => $valor) {
                if ($valor == null) {
                    $registro[$i]->{$clave} = "";
                }
                if ($valor === true) {
                    $registro[$i]->{$clave} = '<i style="color: green;" class="fa fa-check"></i>';
                }
                else if ($valor === false) {
                    $registro[$i]->{$clave} = '<i style="color: red;" class="fa fa-times"></i>';
                }
            }
        }
        return $registro;
    }

}

?>"""

# =============================
#
#   CONTROLADORES ESTÁTICOS
#
# =============================

    def controller_core_get(self):
        """Retorna la clase principal donde heredan los controladores en PHP"""
        return """<?php

class Controller {

    protected $request;
    private $view;

    function __construct() {

    }

    function view($file, $variables = null) {
        if (empty($this->view)) {
            $this->view = new View();
        }
        return $this->view->render($file, $variables);
    }

    function template($file, $variables = null) {
        if (empty($this->view)) {
            $this->view = new View();
        }
        return $this->view->render($file, $variables, true);
    }

    function getRequest() {
        return $this->request;
    }

    function setRequest($request) {
        $this->request = $request;
    }

}

?>\n"""

    def login_controller(self):
        """Controlador de la clase: LOGIN"""
        return """<?php

class LoginController extends Controller {

    function __construct() {
    }

    public function index() {
        return $this->view("login/welcome_login");
    }

    public function crearSesion(Request $request) {
        $correo = $request->__get("correo");
        $password = $request->__get("password");
        $check = $this->validarCampos($correo, $password);
        if ($check != false) { return $check; }
        $respuesta = $this->validarUsuario($correo, $password);
        return $respuesta;
    }

    public function cerrarSesion() {
        session_start();
        session_destroy();
        header("Location:" . URL::to('login'));
    }

    public function ultimaConexion($id) {
        date_default_timezone_set('America/Caracas');
        $id = intval($id);
        $date = array('ultima_conexion' => date("Y/m/d h:i:s"));
        $usuariosModel = new Usuarios();
        $actualizar = $usuariosModel->where("id", "=", $id)->update($date);
    }

    public function validarUsuario($correo, $password) {
        $usuariosModel = new Usuarios();
        $registro = $usuariosModel->where("UPPER(correo)", "=", strtoupper($correo))->get();
        $check = count($registro);
        if ($check) {
            if (!password_verify($password, $registro[0]->password)) {
                return new Respuesta(EMensajes::LOGIN_FALLIDO);
            }
            else if ($registro[0]->activo == false) {
                return new Respuesta(EMensajes::USUARIO_INACTIVO);
            }
            else {
                $this->validarSesion($registro[0]);
                return new Respuesta(EMensajes::LOGIN_EXITOSO);
            }
        }
        else {
            return new Respuesta(EMensajes::LOGIN_FALLIDO);
        }
    }

    public function validarSesion($data) {
        $privilegios = New Privilegios();
        $privilegios = $privilegios->where('rol_id', '=', $data->rol_id)->get();
        $admin = New Roles();
        $admin = $admin->where('id', '=', $data->rol_id)->get();
        $usuario = array('id' => $data->id,
                         'nombre' => $data->nombre,
                         'rol_id' => $data->rol_id,
                         'administrador' => $admin[0]->administrador,
                         'privilegios' => $privilegios);
        $this->ultimaConexion($data->id);
        session_start();
        $_SESSION['usuario'] = $usuario;
    }

    public function validarCampos($correo, $password) {
        if (empty($correo) || empty($password)) {
            $respuesta = new Respuesta(EMensajes::CAMPOS_VACIOS);
            return $respuesta;
        }
        else if (!filter_var($correo, FILTER_VALIDATE_EMAIL)) {
            $respuesta = new Respuesta(EMensajes::ERROR_CORREO);
            return $respuesta;
        }
        return false;
    }

}

?>"""

    def usuario_controller(self):
        """Controlador de la clase: USUARIO"""
        return """<?php

class UsuariosController extends Controller {

    function __construct() {
    }

    public function index() {
        if (!Permisos::permisosCRUD('Usuarios', 'listar')) {
            return $this->template('401');
        }
        return $this->view("usuarios/welcome_usuarios");
    }

    public function formCrearUsuarios() {
        if (!Permisos::permisosCRUD('Usuarios', 'crear')) {
            return $this->template('401');
        }
        $rol_id = New Usuarios();
        return $this->view("usuarios/registrar_usuarios",
                            array(
                                    "rol_id"=>$rol_id->getRolId(),
                                )
                        );
    }

    public function formEditarUsuarios(Request $request) {
        if (!is_numeric($request->__get("id"))) { return $this->template('404'); }
        $id = intval($request->__get("id"));
        if (!Permisos::login_user($id)) {
            return $this->template('401');
        }
        $usuariosModel = new Usuarios();
        $registro = $usuariosModel->where("id", "=", $id)->get();
        return $this->view("usuarios/editar_usuarios",
                            array(
                                    "rol_id"=>$usuariosModel->getRolId(),
                                    "usuarios"=>$registro,
                                )
                        );
    }

    public function formVerUsuarios(Request $request) {
        if (!is_numeric($request->__get("id"))) { return $this->template('404'); }
        $id = intval($request->__get("id"));
        if (!Permisos::login_user($id)) {
            return $this->template('401');
        }
        $usuariosModel = new Usuarios();
        $registro = $usuariosModel->where("id", "=", $id)->get();
        return $this->view("usuarios/ver_usuarios",
                            array(
                                    "rol_id"=>$usuariosModel->getRolId(),
                                    "usuarios"=>$registro,
                                )
                        );
    }

    public function insertarUsuarios(Request $request) {
        if (!csrfToken::validarCsrf()) {return new Respuesta(EMensajes::ERROR_INSERSION);}
        $usuariosModel = new Usuarios();
        $correoCheck = $this->restriccionCorreo($request->__get("correo"));
        if ($correoCheck != false) { return $correoCheck; }
        $request->__set("password", password_hash($request->__get("password"), PASSWORD_DEFAULT));
        $id = $usuariosModel->insert($request->all());
        $insersionExitosa = ($id > 0);
        $respuesta = new Respuesta($insersionExitosa ? EMensajes::INSERCION_EXITOSA : EMensajes::ERROR_INSERSION);
        $respuesta->setDatos($id);
        return $respuesta;
    }

    public function listarUsuarios() {
        if (!Permisos::permisosCRUD('Usuarios', 'listar')) {
            header("Location:" . URL::to('login'));
        }
        $usuariosModel = new Usuarios();
        $lista = $usuariosModel->get();
        $cantidad = count($lista);
        $respuesta = new Respuesta($cantidad ? EMensajes::CORRECTO : EMensajes::ERROR);
        if ($cantidad) {
            $lista = recName::parsearDatos($lista);
            $lista = recName::oneName($lista,
                                      array('rol_id' => 'nombre'),
                                      array( 'rol_id' => $usuariosModel->getRolId())
                                    );
        }
        $respuesta->setDatos($lista);
        return $respuesta;
    }

    public function actualizarUsuarios(Request $request) {
        if (!csrfToken::validarCsrf()) {return new Respuesta(EMensajes::ERROR_ACTUALIZACION);}
        $id = intval($request->__get("id"));
        $usuariosModel = new Usuarios();
        $correoCheck = $this->restriccionCorreo($request->__get("correo"), $id);
        if ($correoCheck != false) { return $correoCheck; }
        $actualizados = $usuariosModel->where("id", "=", $id)->update($request->all());
        $check = ($actualizados > 0);
        $respuesta = new Respuesta($check ? EMensajes::ACTUALIZACION_EXITOSA : EMensajes::ERROR_ACTUALIZACION);
        return $respuesta;
    }

    public function eliminarUsuarios(Request $request) {
        if (!Permisos::permisosCRUD('Usuarios', 'eliminar')) {
            return new Respuesta(EMensajes::SIN_PERMISOS);
        }
        $usuariosModel = new Usuarios();
        $idUsuarios = intval($request->__get("id"));
        $eliminados = $usuariosModel->where("id", "=", $idUsuarios)->delete();
        $check = ($eliminados > 0);
        $respuesta = new Respuesta($check ? EMensajes::ELIMINACION_EXITOSA : EMensajes::ERROR_ELIMINACION);
        return $respuesta;
    }

    public function buscarUsuariosPorId($idUsuarios) {
        $usuariosModel = new Usuarios();
        $usuarios = $usuariosModel->where("id", "=", $idUsuarios)->first();
        $check = ($usuarios != null);
        $respuesta = new Respuesta($check ? EMensajes::CORRECTO : EMensajes::NO_HAY_REGISTROS);
        return $respuesta;
    }

    public function restriccionCorreo($correo, $id = 0) {
        $usuariosModel = new Usuarios();
        $correoCheck = $usuariosModel->where("correo", "=", $correo)->where("id", "!=", $id)->first();
        if ($correoCheck != null) {
            return new Respuesta(-1, "Este correo ya está registrado.");
        }
        return false;
    }

    public function cambiarClave(Request $request) {
        $respuesta = new Respuesta(EMensajes::CORRECTO);
        $vista = $this->view("usuarios/cambiar_clave",
                             array(
                                    "id" => $request->__get("id"),
                                  )
                            );
        $respuesta->setDatos($vista);
        return $respuesta;
    }

    public function actualizarClave(Request $request) {
        $id = intval($request->__get("id"));
        if ($request->__get("password") != $request->__get("password2")) {
            return new Respuesta(-1, "Las contraseñas no coinciden.");
        }
        $usuariosModel = new Usuarios();
        $request->__set("password", password_hash($request->__get("password"), PASSWORD_DEFAULT));
        $actualizados = $usuariosModel->where("id", "=", $id)->update($request->all());
        $check = ($actualizados > 0);
        $respuesta = new Respuesta($check ? EMensajes::ACTUALIZACION_EXITOSA : EMensajes::ERROR_ACTUALIZACION);
        return $respuesta;

    }

}
?>"""

    def privilegios_controller(self):
        """Controlador de la clase: PRIVILEGIOS"""
        return """<?php

class PrivilegiosController extends Controller {

    function __construct() {
    }

    public function index() {
        if (!Permisos::permisosCRUD('Privilegios', 'listar')) {
            return $this->template('401');
        }
        return $this->view("privilegios/welcome_privilegios");
    }

    public function formCrearPrivilegios() {
        if (!Permisos::permisosCRUD('Privilegios', 'crear')) {
            return $this->template('401');
        }
        $rol_id = New Privilegios();
        return $this->view("privilegios/registrar_privilegios",
                            array(
                                    "rol_id"=>$rol_id->getRolId(),
                                )
                        );
    }

    public function formEditarPrivilegios(Request $request) {
        if (!is_numeric($request->__get("id"))) { return $this->template('404'); }
        if (!Permisos::permisosCRUD('Privilegios', 'editar')) {
            return $this->template('401');
        }
        $id = intval($request->__get("id"));
        $privilegiosModel = new Privilegios();
        $registro = $privilegiosModel->where("id", "=", $id)->get();
        return $this->view("privilegios/editar_privilegios",
                            array(
                                    "rol_id"=>$privilegiosModel->getRolId(),
                                    "privilegios"=>$registro,
                                )
                        );
    }

    public function formVerPrivilegios(Request $request) {
        if (!is_numeric($request->__get("id"))) { return $this->template('404'); }
        if (!Permisos::permisosCRUD('Privilegios', 'ver')) {
            return $this->template('401');
        }
        $id = intval($request->__get("id"));
        $privilegiosModel = new Privilegios();
        $registro = $privilegiosModel->where("id", "=", $id)->get();
        return $this->view("privilegios/ver_privilegios",
                            array(
                                    "rol_id"=>$privilegiosModel->getRolId(),
                                    "privilegios"=>$registro,
                                )
                        );
    }

    public function insertarPrivilegios(Request $request) {
        if (!csrfToken::validarCsrf()) {return new Respuesta(EMensajes::ERROR_INSERSION);}
        $privilegiosModel = new Privilegios();
        $id = $privilegiosModel->insert($request->all());
        $insersionExitosa = ($id > 0);
        $respuesta = new Respuesta($insersionExitosa ? EMensajes::INSERCION_EXITOSA : EMensajes::ERROR_INSERSION);
        $respuesta->setDatos($id);
        return $respuesta;
    }

    public function listarPrivilegios() {
        if (!Permisos::permisosCRUD('Privilegios', 'listar')) {
            header("Location:" . URL::to('login'));
        }
        $privilegiosModel = new Privilegios();
        $lista = $privilegiosModel->get();
        $cantidad = count($lista);
        $respuesta = new Respuesta($cantidad ? EMensajes::CORRECTO : EMensajes::ERROR);
        if ($cantidad) {
            $lista = recName::parsearDatos($lista);
            $lista = recName::oneName($lista,
                                      array('rol_id' => 'nombre'),
                                      array('rol_id' => $privilegiosModel->getRolId())
                                    );
        }
        $respuesta->setDatos($lista);
        return $respuesta;
    }

    public function actualizarPrivilegios(Request $request) {
        if (!csrfToken::validarCsrf()) {return new Respuesta(EMensajes::ERROR_ACTUALIZACION);}
        $privilegiosModel = new Privilegios();
        $actualizados = $privilegiosModel->where("id", "=", intval($request->__get("id")))->update($request->all());
        $check = ($actualizados > 0);
        $respuesta = new Respuesta($check ? EMensajes::ACTUALIZACION_EXITOSA : EMensajes::ERROR_ACTUALIZACION);
        return $respuesta;
    }

    public function eliminarPrivilegios(Request $request) {
        if (!Permisos::permisosCRUD('Privilegios', 'eliminar')) {
            return new Respuesta(EMensajes::SIN_PERMISOS);
        }
        $privilegiosModel = new Privilegios();
        $idPrivilegios = intval($request->__get("id"));
        $eliminados = $privilegiosModel->where("id", "=", $idPrivilegios)->delete();
        $check = ($eliminados > 0);
        $respuesta = new Respuesta($check ? EMensajes::ELIMINACION_EXITOSA : EMensajes::ERROR_ELIMINACION);
        return $respuesta;
    }

    public function buscarPrivilegiosPorId($idPrivilegios) {
        $privilegiosModel = new Privilegios();
        $privilegios = $privilegiosModel->where("id", "=", $idPrivilegios)->first();
        $check = ($privilegios != null);
        $respuesta = new Respuesta($check ? EMensajes::CORRECTO : EMensajes::NO_HAY_REGISTROS);
        return $respuesta;
    }

}
?>"""

    def roles_controller(self):
        """Controlador de la clase: ROLES"""
        return """<?php

class RolesController extends Controller {

    function __construct() {
    }

    public function index() {
        if (!Permisos::permisosCRUD('Roles', 'listar')) {
            return $this->template('401');
        }
        return $this->view("roles/welcome_roles");
    }

    public function formCrearRoles() {
        if (!Permisos::permisosCRUD('Roles', 'crear')) {
            return $this->template('401');
        }
        return $this->view("roles/registrar_roles");
    }

    public function formEditarRoles(Request $request) {
        if (!is_numeric($request->__get("id"))) { return $this->template('404'); }
        if (!Permisos::permisosCRUD('Roles', 'editar')) {
            return $this->template('401');
        }
        $id = intval($request->__get("id"));
        $rolesModel = new Roles();
        $registro = $rolesModel->where("id", "=", $id)->get();
        return $this->view("roles/editar_roles",
                            array(
                                    "roles"=>$registro,
                                )
                        );
    }

    public function formVerRoles(Request $request) {
        if (!is_numeric($request->__get("id"))) { return $this->template('404'); }
        if (!Permisos::permisosCRUD('Roles', 'ver')) {
            return $this->template('401');
        }
        $id = intval($request->__get("id"));
        $rolesModel = new Roles();
        $registro = $rolesModel->where("id", "=", $id)->get();
        return $this->view("roles/ver_roles",
                            array(
                                    "roles"=>$registro,
                                )
                        );
    }

    public function insertarRoles(Request $request) {
        if (!csrfToken::validarCsrf()) {return new Respuesta(EMensajes::ERROR_INSERSION);}
        $rolesModel = new Roles();
        $id = $rolesModel->insert($request->all());
        $insersionExitosa = ($id > 0);
        $respuesta = new Respuesta($insersionExitosa ? EMensajes::INSERCION_EXITOSA : EMensajes::ERROR_INSERSION);
        $respuesta->setDatos($id);
        return $respuesta;
    }

    public function listarRoles() {
        if (!Permisos::permisosCRUD('Roles', 'listar')) {
            header("Location:" . URL::to('login'));
        }
        $rolesModel = new Roles();
        $lista = $rolesModel->get();
        $cantidad = count($lista);
        $respuesta = new Respuesta($cantidad ? EMensajes::CORRECTO : EMensajes::ERROR);
        if ($cantidad) {
            $lista = recName::parsearDatos($lista);
        }
        $respuesta->setDatos($lista);
        return $respuesta;
    }

    public function actualizarRoles(Request $request) {
        if (!csrfToken::validarCsrf()) {return new Respuesta(EMensajes::ERROR_ACTUALIZACION);}
        $rolesModel = new Roles();
        $actualizados = $rolesModel->where("id", "=", intval($request->__get("id")))->update($request->all());
        $check = ($actualizados > 0);
        $respuesta = new Respuesta($check ? EMensajes::ACTUALIZACION_EXITOSA : EMensajes::ERROR_ACTUALIZACION);
        return $respuesta;
    }

    public function eliminarRoles(Request $request) {
        if (!Permisos::permisosCRUD('Roles', 'eliminar')) {
            return new Respuesta(EMensajes::SIN_PERMISOS);
        }
        $rolesModel = new Roles();
        $idRoles = intval($request->__get("id"));
        $eliminados = $rolesModel->where("id", "=", $idRoles)->delete();
        $check = ($eliminados > 0);
        $respuesta = new Respuesta($check ? EMensajes::ELIMINACION_EXITOSA : EMensajes::ERROR_ELIMINACION);
        return $respuesta;
    }

    public function buscarRolesPorId($idRoles) {
        $rolesModel = new Roles();
        $roles = $rolesModel->where("id", "=", $idRoles)->first();
        $check = ($roles != null);
        $respuesta = new Respuesta($check ? EMensajes::CORRECTO : EMensajes::NO_HAY_REGISTROS);
        return $respuesta;
    }

}
?>"""

    def welcome_controller_get(self):
        """Controlador de la vista de bienvenida"""
        return """<?php

class WelcomeController extends Controller {

    function __construct() {
    }

    public function index() {
        return $this->view("welcome");
    }
}

?>\n"""

# =============================
#
#   MODELS ESTÁTICOS
#
# =============================

    def model_usuario(self):
        """Modelo de la clase: USUARIO"""
        return """<?php

class Usuarios extends ModeloGenerico {
    protected $id;
    protected $nombre;
    protected $correo;
    protected $password;
    protected $activo;
    protected $ultima_conexion;
    protected $rol_id;

    public function __construct($propiedades = null, $order_by = "ultima_conexion DESC") {
        parent::__construct("usuarios", Usuarios::class, $propiedades, $order_by);
    }

    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getCorreo() {
        return $this->correo;
    }

    function getPassword() {
        return $this->password;
    }

    function getActivo() {
        return $this->activo;
    }

    function getUltimaConexion() {
        return $this->ultima_conexion;
    }

    function getRolId() {
        $this->rol_id = New RolesController();
        return $this->rol_id->listarRoles()->datos;
    }
    function setId() {
        return $this->id;
    }

    function setNombre() {
        return $this->nombre;
    }

    function setCorreo() {
        return $this->correo;
    }

    function setPassword() {
        return $this->password;
    }

    function setActivo() {
        return $this->activo;
    }

    function setUltimaConexion() {
        return $this->ultima_conexion;
    }

    function setRolId() {
        return $this->rol_id;
    }

}
?>"""

    def model_privilegios(self):
        """Modelo de la clase: PRIVILEGIOS"""
        return """<?php

class Privilegios extends ModeloGenerico {
    protected $id;
    protected $entidad;
    protected $rol_id;
    protected $ver;
    protected $listar;
    protected $crear;
    protected $editar;
    protected $eliminar;

    public function __construct($propiedades = null, $order_by = "id DESC") {
        parent::__construct("privilegios", Privilegios::class, $propiedades, $order_by);
    }

    function getId() {
        return $this->id;
    }

    function getEntidad() {
        return $this->entidad;
    }

    function getRolId() {
        $this->rol_id = New RolesController();
        return $this->rol_id->listarRoles()->datos;
    }
    function getVer() {
        return $this->ver;
    }

    function getListar() {
        return $this->listar;
    }

    function getCrear() {
        return $this->crear;
    }

    function getEditar() {
        return $this->editar;
    }

    function getEliminar() {
        return $this->eliminar;
    }

    function setId($valor) {
        return $this->id;
    }

    function setEntidad($valor) {
        return $this->entidad;
    }

    function setRolId($valor) {
        return $this->rol_id;
    }

    function setVer($valor) {
        return $this->ver;
    }

    function setListar($valor) {
        return $this->listar;
    }

    function setCrear($valor) {
        return $this->crear;
    }

    function setEditar($valor) {
        return $this->editar;
    }

    function setEliminar($valor) {
        return $this->eliminar;
    }

}
?>"""

    def model_roles(self):
        """Modelo de la clase: ROLES"""
        return """<?php

class Roles extends ModeloGenerico {
    protected $id;
    protected $nombre;
    protected $descripcion;
    protected $administrador;
    protected $activo;

    public function __construct($propiedades = null, $order_by = "id DESC") {
        parent::__construct("roles", Roles::class, $propiedades, $order_by);
    }

    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function getAdministrador() {
        return $this->administrador;
    }

    function getActivo() {
        return $this->activo;
    }

    function setId($valor) {
        return $this->id;
    }

    function setNombre($valor) {
        return $this->nombre;
    }

    function setDescripcion($valor) {
        return $this->descripcion;
    }

    function setAdministrador($valor) {
        return $this->administrador;
    }

    function setActivo($valor) {
        return $this->activo;
    }

}
?>"""

# =============================
#
#   VISTAS ESTÁTICAS
#
# =============================

    def welcome_login_view(self):
        """Vista de Inicio de Sesión"""
        return """<?php
    session_start();
    if (isset($_SESSION['usuario'])) {
        header("Location:" . URL::to('/'));
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Iniciar Sesión</title>
    <meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0, maximum-scale=3.0, minimum-scale=1.0">
    <link href="<?= URL::to('assets/css/font-awesome.min.css') ?>" rel="stylesheet" />
    <link rel="stylesheet" href="<?= URL::to('assets/css/login/style.css') ?>">
</head>
<body data-urlbase="<?= URL::base() ?>">
    <form id="formLogin" action="iniciar/sesion" class="formulario">
        <h1>Iniciar Sesión</h1>
        <div class="contenedor">
            <div class="input-contenedor">
                <i class="fa fa-envelope icon"></i>
                <input type="text" id="correo" name="correo" required="required" placeholder="Correo Electrónico">
            </div>
            <div class="input-contenedor">
                <i class="fa fa-key icon"></i>
                <input type="password" id="password" name="password" required="required" placeholder="Contraseña">
            </div>
            <input type="submit" value="Iniciar Sesión" class="button">
            <p>¿No tienes una cuenta? <a class="link" href="#">Registrate</a></p>
        </div>
    </form>
    <script src="<?= URL::to('assets/plugins/jquery/jquery.js') ?>" type="text/javascript"></script>
    <script src="<?= URL::to('assets/js/global/helperform.js') ?>" type="text/javascript"></script>
    <script src="<?= URL::to('assets/js/global/rutas.api.js') ?>" type="text/javascript"></script>
    <script src="<?= URL::to('assets/js/global/app.global.js') ?>" type="text/javascript"></script>
    <script src="<?= URL::to('assets/plugins/sweetalert/sweetalert.js') ?>" type="text/javascript"></script>
    <script src="<?= URL::to('assets/js/modulos/login.js') ?>" type="text/javascript"></script>
</body>
</html>"""

    def cambiar_clave_view(self):
        """Vista de Cambiar Clave de Usuario"""
        return """    <!--Contenido start-->
    <div class="container">
        <div class="card mt-5">
            <div class="card-body">
            <h3 class="card-title mb-4 text-dark"><b>Cambiar Contraseña</b></h3>
            <form id="formCambiarClave" action="usuarios/actualizar" method="POST">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <input type="hidden" id="id" name="id" disabled readonly value="<?php print $id ?>"/>
                            <div class="form-group col-lg-6">
                                <label class="control-label" for="password">Contraseña</label>
                                <input type="password" id="password" required name="password" class="form-control"/>
                            </div>
                            <div class="form-group col-lg-6">
                                <label class="control-label" for="password2">Confirmar Contraseña</label>
                                <input type="password" id="password2" required name="password2" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="form-group text-right clearfix">
                        <button type="submit" class="btn btn-primary">
                            Guardar
                            <span class="fa fa-long-arrow-right" />
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--Contenido end-->
    <script src="<?= URL::to("assets/plugins/jquery/jquery.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/helperform.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/rutas.api.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/app.global.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/plugins/sweetalert/sweetalert.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/modulos/usuarios/cambiar.clave.js") ?>" type="text/javascript"></script>"""

    def editar_usuarios_view(self):
        """Vista de Editar Usuario"""
        return """<?= (new View())->includeFile(PATH_TEMPLATES . 'header'); ?>
<div id="esqueleto">
    <!--Contenido start-->
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i>Usuarios</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="<?= URL::base() ?>">Inicio</a></li>
                    <li><i class="fa fa-bars"></i><a href="<?= URL::to('usuarios') ?>">Usuarios</a></li>
                    <li><i class="fa fa-bars"></i><a href="<?= URL::to("usuarios/form/ver?id=" . $usuarios[0]->id) ?>">Ver Usuario</a></li>
                    <li><i class="fa fa-bars"></i>Editar Usuarios</li>
                </ol>
            </div>
        </div>
        <div class="container" id="contenido">
            <div class="card mt-5">
                <div id="bodyUsuarios" class="card-body">
                <h3 class="card-title mb-4 text-dark"><b>Editar Registro</b></h3>
                <div class="btn-group mb-4" >
                    <button type="button" value="<?= $usuarios[0]->id ?>" class="btn btn-default cambiar-clave">
                        <i class="fa fa-lock"></i>
                        <b>Cambiar Contraseña</b>
                    </button>
                </div>
                <form id="formUsuarios" action="usuarios/actualizar" method="POST">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <input type="hidden" id="id" name="id" value="<?php print $usuarios[0]->id ?>"/>
                                <input type="hidden" id="_token" name="_token" value="<?= $_SESSION['csrf_token'] ?>"/>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="nombre">Nombre</label>
                                    <input type="text" id="nombre" name="nombre" required value="<?php print $usuarios[0]->nombre ?>" class="form-control"/>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="correo">Correo</label>
                                    <input type="text" id="correo" name="correo" required value="<?php print $usuarios[0]->correo ?>" class="form-control"/>
                                </div>
                                <?php if ($_SESSION['usuario']['administrador']) { ?>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="activo">¿Activo?</label>
                                    <input type="checkbox" id="activo" name="activo" <?php if ($usuarios[0]->activo == true) print 'checked="checked"' ?> class="form-control"/>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="ultima_conexion">Última Conexión</label>
                                    <input type="text" id="ultima_conexion" readonly disabled name="ultima_conexion" value="<?php print $usuarios[0]->ultima_conexion ?>" class="form-control"/>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="rol_id">Rol del Usuario</label>
                                        <select name="rol_id" id="rol_id" class="form-control" required>
                                        <option id="default" disabled selected>Selecciona una opción</option>
                                        <?php for ($i=0; $i < sizeof($rol_id); $i++) {
                                        if ($usuarios[0]->rol_id == $rol_id[$i]->id)
                                            {$selected = 'selected';}
                                        else {$selected = '';}
                                        echo '<option value="'.$rol_id[$i]->id.'"'.$selected.'>'.$rol_id[$i]->nombre.'</option>';
                                        }
                                        ?>
                                        </select>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        </div>
                        <div class="form-group text-right clearfix">
                            <button type="submit" class="btn btn-primary">
                                Guardar
                                <span class="fa fa-long-arrow-right" />
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </section>
    </section>
    <!--Contenido end-->
    <script src="<?= URL::to("assets/plugins/jquery/jquery.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/helperform.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/rutas.api.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/app.global.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/plugins/sweetalert/sweetalert.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/modulos/usuarios/editar.usuarios.js") ?>" type="text/javascript"></script>
</div>
<?= (new View())->includeFile(PATH_TEMPLATES . 'footer'); ?>"""

    def registrar_usuarios_view(self):
        """Vista de Registrar Usuario"""
        return """<?= (new View())->includeFile(PATH_TEMPLATES . 'header'); ?>
    <!--Contenido start-->
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i>Usuarios</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="<?= URL::base() ?>">Inicio</a></li>
                    <li><i class="fa fa-bars"></i><a href="<?= URL::to('usuarios') ?>">Usuarios</a></li>
                    <li><i class="fa fa-bars"></i>Crear Usuarios</li>
                </ol>
            </div>
        </div>
        <div class="card-body">
        <h3 class="card-title mb-4 text-dark"><b>Insertar Registro</b></h3>
        <form id="formUsuarios" action="usuarios/registrar" method="POST">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <input type="hidden" id="_token" name="_token" value="<?= $_SESSION['csrf_token'] ?>"/>
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="nombre">Nombre</label>
                            <input type="text" id="nombre" name="nombre" required class="form-control"/>
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="correo">Correo</label>
                            <input type="email" id="correo" name="correo" required class="form-control"/>
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="password">Password</label>
                            <input type="password" id="password" name="password" required class="form-control"/>
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="activo">¿Activo?</label>
                            <input type="checkbox" id="activo" name="activo" class="form-control"/>
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="ultima_conexion">Última Conexión</label>
                            <input type="datetime-local" id="ultima_conexion" name="ultima_conexion" disabled readonly class="form-control"/>
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="rol_id">Rol del Usuario</label>
                            <select name="rol_id" id="rol_id" class="form-control" required>
                            <option id="default" disabled selected>Selecciona una opción</option>
                            <?php for ($i=0; $i < sizeof($rol_id); $i++) {
                                echo '<option value="'.$rol_id[$i]->id.'">'.$rol_id[$i]->nombre.'</option>';
                                }
                            ?>
                            </select>
                        </div>
                    </div>
                </div>
                </div>
                <div class="form-group text-right clearfix">
                    <button type="submit" class="btn btn-primary">
                        Guardar
                        <span class="fa fa-long-arrow-right" />
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <script src="<?= URL::to("assets/plugins/jquery/jquery.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/helperform.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/rutas.api.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/app.global.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/plugins/sweetalert/sweetalert.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/modulos/usuarios/registrar.usuarios.js") ?>" type="text/javascript"></script>
<?= (new View())->includeFile(PATH_TEMPLATES . 'footer'); ?>"""

    def ver_usuarios_view(self):
        """Vista de ver Usuarios"""
        return """<?= (new View())->includeFile(PATH_TEMPLATES . 'header'); ?>
    <!--Contenido start-->
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i>Usuarios</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="<?= URL::base() ?>">Inicio</a></li>
                    <li><i class="fa fa-bars"></i><a href="<?= URL::to('usuarios') ?>">Usuarios</a></li>
                    <li><i class="fa fa-bars"></i>Ver Usuarios</li>
                </ol>
            </div>
        </div>
        <div class="container" id="contenido">
            <div class="card mt-5">
                <div id="bodyUsuarios" class="card-body">
                <div class="btn-group float-right">
                    <a href="<?= URL::to("usuarios/form/editar?id=" . $usuarios[0]->id) ?>" class="btn btn-primary">Editar
                    </a>
                </div>
                <h3 class="card-title mb-4 text-dark"><b>Ver Registro</b></h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="nombre">Nombre</label>
                                    <input type="text" id="nombre" name="nombre" readonly disabled value="<?php print $usuarios[0]->nombre ?>" class="form-control"/>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="correo">Correo</label>
                                    <input type="text" id="correo" name="correo" readonly disabled value="<?php print $usuarios[0]->correo ?>" class="form-control"/>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="activo">¿Activo?</label>
                                    <input type="checkbox" id="activo" name="activo" readonly disabled <?php if ($usuarios[0]->activo == true) print 'checked="checked"' ?> class="form-control"/>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="ultima_conexion">Última Conexión</label>
                                    <input type="text" id="ultima_conexion" readonly disabled name="ultima_conexion" value="<?php print $usuarios[0]->ultima_conexion ?>" class="form-control"/>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="rol_id">Rol del Usuario</label>
                                        <select name="rol_id" id="rol_id" readonly disabled class="form-control">
                                        <option id="default" disabled selected>Selecciona una opción</option>
                                        <?php for ($i=0; $i < sizeof($rol_id); $i++) {
                                        if ($usuarios[0]->rol_id == $rol_id[$i]->id)
                                            {$selected = 'selected';}
                                        else {$selected = '';}
                                        echo '<option value="'.$rol_id[$i]->id.'"'.$selected.'>'.$rol_id[$i]->nombre.'</option>';
                                        }
                                        ?>
                                        </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </section>
    </section>
    <!--Contenido end-->
    <script src="<?= URL::to("assets/plugins/jquery/jquery.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/plugins/sweetalert/sweetalert.js") ?>" type="text/javascript"></script>
<?= (new View())->includeFile(PATH_TEMPLATES . 'footer'); ?>"""

    def welcome_usuarios_view(self):
        """Vista lista de Usuarios"""
        return """<?= (new View())->includeFile(PATH_TEMPLATES . 'header'); ?>
    <!--Contenido start-->
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i>Usuarios</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="<?= URL::base() ?>">Inicio</a></li>
                    <li><i class="fa fa-bars"></i>Usuarios</li>
                </ol>
            </div>
        </div>
        <div class="row">
                <div class="card-body">
                    <div class="btn-group">
                        <a href="<?= URL::to("usuarios/form/crear") ?>" class="btn btn-primary">Crear Usuarios</a>
                    </div>
                    <hr/>
                    <h2 class="card-title mb-4 text-dark"><b>Listar Usuarios</b></h4>
                    <table class="table table-condensed table-advance table-hover table-striped table-responsive-lg" width="100%" id="tablaListaUsuarios">
                        <thead class="text-white bg-dark">
                            <tr>
                                <th>Nombre</th>
                                <th>Correo</th>
                                <th>¿Activo?</th>
                                <th>Última Conexión</th>
                                <th>Rol del Usuario</th>
                                <th><i class="icon_cogs"></i> Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="6">Consultando...</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

        </div>
        </section>
    </section>
    <!--Contenido end-->

    <script src="<?= URL::to("assets/plugins/jquery/jquery.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/plugins/sweetalert/sweetalert.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/helperform.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/rutas.api.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/app.global.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/modulos/usuarios/listar.usuarios.js") ?>" type="text/javascript"></script>
<?= (new View())->includeFile(PATH_TEMPLATES . 'footer'); ?>"""

    def editar_roles_view(self):
        """Vista de editar Roles"""
        return """<?= (new View())->includeFile(PATH_TEMPLATES . 'header'); ?>
    <!--Contenido start-->
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i>Roles</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="<?= URL::base() ?>">Inicio</a></li>
                    <li><i class="fa fa-bars"></i><a href="<?= URL::to('roles') ?>">Roles</a></li>
                    <li><i class="fa fa-bars"></i><a href="<?= URL::to("roles/form/ver?id=" . $roles[0]->id) ?>">Ver Roles</a></li>
                    <li><i class="fa fa-bars"></i>Editar Roles</li>
                </ol>
            </div>
        </div>
        <div class="container">
            <div class="card mt-5">
                <div id="bodyRoles" class="card-body">
                <h3 class="card-title mb-4 text-dark"><b>Editar Registro</b></h3>
                <form id="formRoles" action="roles/actualizar" method="POST">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <input type="hidden" id="id" name="id" value="<?php print $roles[0]->id ?>"/>
                                <input type="hidden" id="_token" name="_token" value="<?= $_SESSION['csrf_token'] ?>"/>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="nombre">Nombre del Rol</label>
                                    <input type="text" id="nombre" name="nombre" readonly required disabled value="<?php print $roles[0]->nombre ?>" class="form-control"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="control-label" for="descripcion">Descripción del Rol</label>
                                    <textarea id="descripcion" name="descripcion" readonly required disabled class="form-control"><?php print $roles[0]->descripcion ?></textarea>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="administrador">¿Administrador?</label>
                                    <input type="checkbox" id="administrador" name="administrador" readonly disabled <?php if ($roles[0]->administrador == true) print 'checked="checked"' ?> class="form-control"/>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="activo">¿Activo?</label>
                                    <input type="checkbox" id="activo" name="activo" readonly disabled <?php if ($roles[0]->activo == true) print 'checked="checked"' ?> class="form-control"/>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="form-group text-right clearfix">
                            <button type="submit" class="btn btn-primary">
                                Guardar
                                <span class="fa fa-long-arrow-right" />
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </section>
    </section>
    <!--Contenido end-->
    <script src="<?= URL::to("assets/plugins/jquery/jquery.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/helperform.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/rutas.api.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/app.global.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/plugins/sweetalert/sweetalert.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/modulos/roles/editar.roles.js") ?>" type="text/javascript"></script>
<?= (new View())->includeFile(PATH_TEMPLATES . 'footer'); ?>"""

    def registrar_roles_view(self):
        """Vista de registrar Roles"""
        return """<?= (new View())->includeFile(PATH_TEMPLATES . 'header'); ?>
    <!--Contenido start-->
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i>Roles</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="<?= URL::base() ?>">Inicio</a></li>
                    <li><i class="fa fa-bars"></i><a href="<?= URL::to('roles') ?>">Roles</a></li>
                    <li><i class="fa fa-bars"></i>Crear Roles</li>
                </ol>
            </div>
        </div>
        <div class="card-body">
        <h3 class="card-title mb-4 text-dark"><b>Insertar Registro</b></h3>
        <form id="formRoles" action="roles/registrar" method="POST">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <input type="hidden" id="_token" name="_token" value="<?= $_SESSION['csrf_token'] ?>"/>
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="nombre">Nombre del Rol</label>
                            <input type="text" id="nombre" required name="nombre" class="form-control"/>
                        </div>
                        <div class="form-group col-lg-12">
                            <label class="control-label" for="descripcion">Descripción del Rol</label>
                            <textarea id="descripcion" required name="descripcion" class="form-control"></textarea>
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="administrador">¿Administrador?</label>
                            <input type="checkbox" id="administrador" name="administrador" class="form-control"/>
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="activo">¿Activo?</label>
                            <input type="checkbox" id="activo" name="activo" class="form-control"/>
                        </div>
                    </div>
                </div>
                </div>
                <div class="form-group text-right clearfix">
                    <button type="submit" class="btn btn-primary">
                        Guardar
                        <span class="fa fa-long-arrow-right" />
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <script src="<?= URL::to("assets/plugins/jquery/jquery.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/helperform.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/rutas.api.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/app.global.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/plugins/sweetalert/sweetalert.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/modulos/roles/registrar.roles.js") ?>" type="text/javascript"></script>
<?= (new View())->includeFile(PATH_TEMPLATES . 'footer'); ?>"""

    def ver_roles_view(self):
        """Vista de ver Roles"""
        return """<?= (new View())->includeFile(PATH_TEMPLATES . 'header'); ?>
    <!--Contenido start-->
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i>Roles</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="<?= URL::base() ?>">Inicio</a></li>
                    <li><i class="fa fa-bars"></i><a href="<?= URL::to('roles') ?>">Roles</a></li>
                    <li><i class="fa fa-bars"></i>Ver Roles</li>
                </ol>
            </div>
        </div>
        <div class="container">
            <div class="card mt-5">
                <div id="bodyRoles" class="card-body">
                <div class="btn-group float-right">
                    <a href="<?= URL::to("roles/form/editar?id=" . $roles[0]->id) ?>" class="btn btn-primary">Editar
                    </a>
                </div>
                <h3 class="card-title mb-4 text-dark"><b>Editar Registro</b></h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="nombre">Nombre del Rol</label>
                                    <input type="text" id="nombre" name="nombre" readonly disabled value="<?php print $roles[0]->nombre ?>" class="form-control"/>
                                </div>
                                <div class="form-group col-lg-12">
                                    <label class="control-label" for="descripcion">Descripción del Rol</label>
                                    <textarea id="descripcion" name="descripcion" readonly disabled class="form-control"><?php print $roles[0]->descripcion ?></textarea>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="administrador">¿Administrador?</label>
                                    <input type="checkbox" id="administrador" name="administrador" readonly disabled <?php if ($roles[0]->administrador == true) print 'checked="checked"' ?> class="form-control"/>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="activo">¿Activo?</label>
                                    <input type="checkbox" id="activo" name="activo" readonly disabled <?php if ($roles[0]->activo == true) print 'checked="checked"' ?> class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </section>
    </section>
    <!--Contenido end-->
    <script src="<?= URL::to("assets/plugins/jquery/jquery.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/plugins/sweetalert/sweetalert.js") ?>" type="text/javascript"></script>
<?= (new View())->includeFile(PATH_TEMPLATES . 'footer'); ?>"""

    def welcome_roles_view(self):
        """Vista lista de Roles"""
        return """<?= (new View())->includeFile(PATH_TEMPLATES . 'header'); ?>
    <!--Contenido start-->
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i>Roles</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="<?= URL::base() ?>">Inicio</a></li>
                    <li><i class="fa fa-bars"></i>Roles</li>
                </ol>
            </div>
        </div>
        <div class="row">
                <div class="card-body">
                    <div class="btn-group">
                        <a href="<?= URL::to("roles/form/crear") ?>" class="btn btn-primary">Crear Roles</a>
                    </div>
                    <hr/>
                    <h2 class="card-title mb-4 text-dark"><b>Listar Roles</b></h4>
                    <table class="table table-condensed table-advance table-hover table-striped table-responsive-lg" width="100%" id="tablaListaRoles">
                        <thead class="text-white bg-dark">
                            <tr>
                                <th>Nombre del Rol</th>
                                <th>Descripción del Rol</th>
                                <th>¿Administrador?</th>
                                <th>¿Activo?</th>
                                <th><i class="icon_cogs"></i> Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="6">Consultando...</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

        </div>
        </section>
    </section>
    <!--Contenido end-->

    <script src="<?= URL::to("assets/plugins/jquery/jquery.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/plugins/sweetalert/sweetalert.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/helperform.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/rutas.api.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/app.global.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/modulos/roles/listar.roles.js") ?>" type="text/javascript"></script>
<?= (new View())->includeFile(PATH_TEMPLATES . 'footer'); ?>"""

# =============================
#
#   VISTAS DINÁMICAS
#
# =============================

    def editar_privilegios_view(self):
        """Vista de editar Privilegios"""
        result = """<?= (new View())->includeFile(PATH_TEMPLATES . 'header'); ?>
    <!--Contenido start-->
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i>Privilegios</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="<?= URL::base() ?>">Inicio</a></li>
                    <li><i class="fa fa-bars"></i><a href="<?= URL::to('privilegios') ?>">Privilegios</a></li>
                    <li><i class="fa fa-bars"></i><a href="<?= URL::to("privilegios/form/ver?id=" . $privilegios[0]->id) ?>">Ver Privilegios</a></li>
                    <li><i class="fa fa-bars"></i>Editar Privilegios</li>
                </ol>
            </div>
        </div>
        <div class="container">
            <div class="card mt-5">
                <div id="bodyPrivilegios" class="card-body">
                <h3 class="card-title mb-4 text-dark"><b>Editar Registro</b></h3>
                <form id="formPrivilegios" action="privilegios/actualizar" method="POST">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <input type="hidden" id="id" name="id" value="<?php print $privilegios[0]->id ?>"/>
                                <input type="hidden" id="_token" name="_token" value="<?= $_SESSION['csrf_token'] ?>"/>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="entidad">Entidad</label>
                                    <select name="entidad" id="entidad" readonly required disabled class="form-control">
                                        <option id="default" disabled selected>Selecciona una opción</option>
                                        <option value="Usuarios" <?php if ($privilegios[0]->entidad == 'Usuarios'){print 'selected';} ?>>Usuarios</option>
                                        <option value="Roles" <?php if ($privilegios[0]->entidad == 'Roles'){print 'selected';} ?>>Roles</option>
                                        <option value="Privilegios" <?php if ($privilegios[0]->entidad == 'Privilegios'){print 'selected';} ?>>Privilegios</option>\n"""
        for sk in self.klass_names:
            data = self.class_name(sk)
            result += """                                        <option value="%(ucname)s" <?php if ($privilegios[0]->entidad == '%(ucname)s'){print 'selected';} ?>>%(ucname)s</option>\n""" % data
        result += """                                    </select>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="rol_id">Rol</label>
                                        <select name="rol_id" id="rol_id"  readonly required disabled class="form-control">
                                        <option id="default" disabled selected>Selecciona una opción</option>
                                        <?php for ($i=0; $i < sizeof($rol_id); $i++) {
                                        if ($privilegios[0]->rol_id == $rol_id[$i]->id)
                                            {$selected = 'selected';}
                                        else {$selected = '';}
                                        echo '<option value="'.$rol_id[$i]->id.'"'.$selected.'>'.$rol_id[$i]->nombre.'</option>';
                                        }
                                        ?>
                                        </select>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="ver">Ver</label>
                                    <input type="checkbox" id="ver" name="ver" readonly disabled <?php if ($privilegios[0]->ver == true) print 'checked="checked"' ?> class="form-control"/>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="listar">Listar</label>
                                    <input type="checkbox" id="listar" name="listar" readonly disabled <?php if ($privilegios[0]->listar == true) print 'checked="checked"' ?> class="form-control"/>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="crear">Crear</label>
                                    <input type="checkbox" id="crear" name="crear" readonly disabled <?php if ($privilegios[0]->crear == true) print 'checked="checked"' ?> class="form-control"/>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="editar">Editar</label>
                                    <input type="checkbox" id="editar" name="editar" readonly disabled <?php if ($privilegios[0]->editar == true) print 'checked="checked"' ?> class="form-control"/>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="eliminar">Eliminar</label>
                                    <input type="checkbox" id="eliminar" name="eliminar" readonly disabled <?php if ($privilegios[0]->eliminar == true) print 'checked="checked"' ?> class="form-control"/>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="form-group text-right clearfix">
                            <button type="submit" class="btn btn-primary">
                                Guardar
                                <span class="fa fa-long-arrow-right" />
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </section>
    </section>
    <!--Contenido end-->
    <script src="<?= URL::to("assets/plugins/jquery/jquery.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/helperform.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/rutas.api.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/app.global.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/plugins/sweetalert/sweetalert.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/modulos/privilegios/editar.privilegios.js") ?>" type="text/javascript"></script>
<?= (new View())->includeFile(PATH_TEMPLATES . 'footer'); ?>"""
        return result

    def registrar_privilegios_view(self):
        """Vista de registrar Privilegios"""
        result = """<?= (new View())->includeFile(PATH_TEMPLATES . 'header'); ?>
    <!--Contenido start-->
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i>Privilegios</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="<?= URL::base() ?>">Inicio</a></li>
                    <li><i class="fa fa-bars"></i><a href="<?= URL::to('privilegios') ?>">Privilegios</a></li>
                    <li><i class="fa fa-bars"></i>Crear Privilegios</li>
                </ol>
            </div>
        </div>
        <div class="card-body">
        <h3 class="card-title mb-4 text-dark"><b>Insertar Registro</b></h3>
        <form id="formPrivilegios" action="privilegios/registrar" method="POST">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <input type="hidden" id="_token" name="_token" value="<?= $_SESSION['csrf_token'] ?>"/>
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="entidad">Entidad</label>
                            <select name="entidad" id="entidad" required class="form-control">
                                <option id="default" disabled selected>Selecciona una opción</option>
                                <option value="Usuarios">Usuarios</option>
                                <option value="Roles">Roles</option>
                                <option value="Privilegios">Privilegios</option>\n"""
        for sk in self.klass_names:
            data = self.class_name(sk)
            result += """                                <option value="%(ucname)s">%(ucname)s</option>\n""" % data
        result += """                            </select>
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="rol_id">Rol</label>
                            <select name="rol_id" id="rol_id" required class="form-control">
                            <option id="default" disabled selected>Selecciona una opción</option>
                            <?php for ($i=0; $i < sizeof($rol_id); $i++) {
                                echo '<option value="'.$rol_id[$i]->id.'">'.$rol_id[$i]->nombre.'</option>';
                                }
                            ?>
                            </select>
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="ver">Ver</label>
                            <input type="checkbox" id="ver" name="ver" class="form-control"/>
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="listar">Listar</label>
                            <input type="checkbox" id="listar" name="listar" class="form-control"/>
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="crear">Crear</label>
                            <input type="checkbox" id="crear" name="crear" class="form-control"/>
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="editar">Editar</label>
                            <input type="checkbox" id="editar" name="editar" class="form-control"/>
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="control-label" for="eliminar">Eliminar</label>
                            <input type="checkbox" id="eliminar" name="eliminar" class="form-control"/>
                        </div>
                    </div>
                </div>
                </div>
                <div class="form-group text-right clearfix">
                    <button type="submit" class="btn btn-primary">
                        Guardar
                        <span class="fa fa-long-arrow-right" />
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <script src="<?= URL::to("assets/plugins/jquery/jquery.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/helperform.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/rutas.api.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/app.global.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/plugins/sweetalert/sweetalert.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/modulos/privilegios/registrar.privilegios.js") ?>" type="text/javascript"></script>
<?= (new View())->includeFile(PATH_TEMPLATES . 'footer'); ?>"""
        return result

    def ver_privilegios_view(self):
        """Vista lista de Privilegios"""
        result = """<?= (new View())->includeFile(PATH_TEMPLATES . 'header'); ?>
    <!--Contenido start-->
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i>Privilegios</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="<?= URL::base() ?>">Inicio</a></li>
                    <li><i class="fa fa-bars"></i><a href="<?= URL::to('privilegios') ?>">Privilegios</a></li>
                    <li><i class="fa fa-bars"></i>Ver Privilegios</li>
                </ol>
            </div>
        </div>
        <div class="container">
            <div class="card mt-5">
                <div id="bodyPrivilegios" class="card-body">
                <div class="btn-group float-right">
                    <a href="<?= URL::to("privilegios/form/editar?id=" . $privilegios[0]->id) ?>" class="btn btn-primary">Editar
                    </a>
                </div>
                <h3 class="card-title mb-4 text-dark"><b>Editar Registro</b></h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="entidad">Entidad</label>
                                    <select name="entidad" id="entidad" readonly disabled class="form-control">
                                        <option id="default" disabled selected>Selecciona una opción</option>
                                        <option id="default" disabled selected>Selecciona una opción</option>
                                        <option value="Usuarios" <?php if ($privilegios[0]->entidad == 'Usuarios'){print 'selected';} ?>>Usuarios</option>
                                        <option value="Roles" <?php if ($privilegios[0]->entidad == 'Roles'){print 'selected';} ?>>Roles</option>
                                        <option value="Privilegios" <?php if ($privilegios[0]->entidad == 'Privilegios'){print 'selected';} ?>>Privilegios</option>\n"""
        for sk in self.klass_names:
            data = self.class_name(sk)
            result += """                                        <option value="%(ucname)s" <?php if ($privilegios[0]->entidad == '%(ucname)s'){print 'selected';} ?>>%(ucname)s</option>\n""" % data
        result += """                                    </select>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="rol_id">Rol</label>
                                        <select name="rol_id" id="rol_id"  readonly disabled class="form-control">
                                        <option id="default" disabled selected>Selecciona una opción</option>
                                        <?php for ($i=0; $i < sizeof($rol_id); $i++) {
                                        if ($privilegios[0]->rol_id == $rol_id[$i]->id)
                                            {$selected = 'selected';}
                                        else {$selected = '';}
                                        echo '<option value="'.$rol_id[$i]->id.'"'.$selected.'>'.$rol_id[$i]->nombre.'</option>';
                                        }
                                        ?>
                                        </select>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="ver">Ver</label>
                                    <input type="checkbox" id="ver" name="ver" readonly disabled <?php if ($privilegios[0]->ver == true) print 'checked="checked"' ?> class="form-control"/>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="listar">Listar</label>
                                    <input type="checkbox" id="listar" name="listar" readonly disabled <?php if ($privilegios[0]->listar == true) print 'checked="checked"' ?> class="form-control"/>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="crear">Crear</label>
                                    <input type="checkbox" id="crear" name="crear" readonly disabled <?php if ($privilegios[0]->crear == true) print 'checked="checked"' ?> class="form-control"/>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="editar">Editar</label>
                                    <input type="checkbox" id="editar" name="editar" readonly disabled <?php if ($privilegios[0]->editar == true) print 'checked="checked"' ?> class="form-control"/>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label class="control-label" for="eliminar">Eliminar</label>
                                    <input type="checkbox" id="eliminar" name="eliminar" readonly disabled <?php if ($privilegios[0]->eliminar == true) print 'checked="checked"' ?> class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </section>
    </section>
    <!--Contenido end-->
    <script src="<?= URL::to("assets/plugins/jquery/jquery.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/plugins/sweetalert/sweetalert.js") ?>" type="text/javascript"></script>
<?= (new View())->includeFile(PATH_TEMPLATES . 'footer'); ?>"""
        return result

    def welcome_privilegios_view(self):
        """Vista de Bienvenida de Privilegios"""
        return """<?= (new View())->includeFile(PATH_TEMPLATES . 'header'); ?>
    <!--Contenido start-->
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i>Privilegios</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="<?= URL::base() ?>">Inicio</a></li>
                    <li><i class="fa fa-bars"></i>Privilegios</li>
                </ol>
            </div>
        </div>
        <div class="row">
                <div class="card-body">
                    <div class="btn-group">
                        <a href="<?= URL::to("privilegios/form/crear") ?>" class="btn btn-primary">Crear Privilegios</a>
                    </div>
                    <hr/>
                    <h2 class="card-title mb-4 text-dark"><b>Listar Privilegios</b></h4>
                    <table class="table table-condensed table-advance table-hover table-striped table-responsive-lg" width="100%" id="tablaListaPrivilegios">
                        <thead class="text-white bg-dark">
                            <tr>
                                <th>Entidad</th>
                                <th>Rol</th>
                                <th>Ver</th>
                                <th>Listar</th>
                                <th>Crear</th>
                                <th>Editar</th>
                                <th>Eliminar</th>
                                <th><i class="icon_cogs"></i> Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="6">Consultando...</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

        </div>
        </section>
    </section>
    <!--Contenido end-->

    <script src="<?= URL::to("assets/plugins/jquery/jquery.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/plugins/sweetalert/sweetalert.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/helperform.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/rutas.api.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/app.global.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/modulos/privilegios/listar.privilegios.js") ?>" type="text/javascript"></script>
<?= (new View())->includeFile(PATH_TEMPLATES . 'footer'); ?>"""

    def welcome_get(self):
        """Tablero Principal"""
        result = """<?= (new View())->includeFile(PATH_TEMPLATES . 'header'); ?>
    <!--Contenido start-->
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i>Tablero</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="<?= URL::base() ?>">Inicio</a></li>
                    <li><i class="fa fa-bars"></i>Tablero</li>
                </ol>
            </div>
        </div>
        <div class="row">\n"""
        contador = 0
        colores = ['blue', 'dark', 'red', 'green']
        for sk in self.klass_names:
            color = colores[contador]
            data = self.class_name(sk)
            data.update({'color': color})
            result += """            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="info-box %(color)s-bg">
                    <i class="icon_documents_alt"></i>
                    <div class="title">
                        <a class="text-white" href="<?= URL::to('%(lcname)s') ?>">%(ucname)s</a>
                    </div>
                </div>
            </div>\n""" % data
            contador += 1
            if contador > 3:
                contador = 0
        result += """        </div>
        </section>
    </section>
    <!--Contenido end-->
<?= (new View())->includeFile(PATH_TEMPLATES . 'footer'); ?>"""
        return result

# =============================
#
#     TEMPLATES
#
# =============================

    def header_get(self):
        """Cabecera de todas las vistas"""
        module = self.data_get()['module'].replace('_', ' ').title()
        module = {'module': module}
        result = """<?php
    if (!isset($_SESSION)) { session_start(); }
    if (!isset($_SESSION['usuario'])) {
        header("Location:" . URL::to('login'));
    }
    else {
        csrfToken::csrfToken();
    }
    if (!isset($_title)) { $_title = "Proyecto | %(module)s"; }
    if (!isset($_content)) { $_content = "Proyecto | %(module)s"; }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?= $_content ?>">
    <meta name="author" content="Franyer Hidalgo - VE">
    <meta name="keyword" content="mitis, unexca, creatividad, programacion">
    <link rel="shortcut icon" href="<?= URL::to('assets/img/venezuela.png') ?>">

    <title><?= $_title ?></title>

    <!-- Bootstrap CSS -->
    <link href="<?= URL::to('assets/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="<?= URL::to('assets/bootstrap/css/bootstrap-theme.css') ?>" rel="stylesheet">
    <!--sweetalert-->
    <link href="<?= URL::to('assets/plugins/sweetalert/sweetalert.css') ?>" rel="stylesheet" type="text/css"/>
    <!--external css-->
    <!-- font icon -->
    <link href="<?= URL::to('assets/css/elegant-icons-style.css') ?>" rel="stylesheet" />
    <link href="<?= URL::to('assets/css/font-awesome.min.css') ?>" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="<?= URL::to('assets/css/panel/style.css') ?>" rel="stylesheet" />
    <link href="<?= URL::to('assets/css/panel/style-responsive.css') ?>" rel="stylesheet" />
</head>

<body data-urlbase="<?= URL::base() ?>">
    <!-- container section start -->
    <section id="container" class="">

    <!--header start-->
    <header class="header dark-bg">
        <div class="toggle-nav">
            <div class="icon-reorder tooltips" data-original-title="Barra de Navegación" data-placement="bottom"><i class="icon_menu"></i></div>
        </div>

        <!--logo start-->
        <a href="<?= URL::base() ?>" class="logo">
            <img height="42" width="96" alt="MITIS" src="<?= URL::to('assets/img/mitis_blanco.png') ?>">
        </a>
        <!--logo end-->

        <div class="top-nav notification-row">
        <!-- notificatoin dropdown start-->
            <ul class="nav pull-right top-menu">

                <!-- alert notification start-->
                <li id="alert_notificatoin_bar" class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon-bell-l"></i>
                        <span class="badge bg-important">0</span>
                    </a>

                    <ul class="dropdown-menu extended notification">
                        <div class="notify-arrow notify-arrow-blue"></div>
                        <li>
                            <p class="blue">Tiene 0 notificaciones</p>
                        </li>
                        <li>
                            <a href="#">
                                <span class="label label-primary"><i class="icon_profile"></i></span>
                                Ejemplo de una notificación
                                <span class="small italic pull-right">5 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">Ver todas las notificaciones</a>
                        </li>
                    </ul>
                </li>
                <!-- alert notification end-->

                <!-- user login dropdown start-->
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="profile-ava">
                            <img alt="" height="30" width="30" src="<?= URL::to('assets/img/avatar1_small.jpg') ?>">
                        </span>
                        <span class="username"><?= $_SESSION['usuario']['nombre'] ?></span>
                    </a>
                    <ul class="dropdown-menu extended logout">
                        <div class="log-arrow-up"></div>
                        <li class="eborder-top">
                            <a href="<?= URL::to('/usuarios/form/ver?id=' . $_SESSION['usuario']['id']) ?>"><i class="icon_profile"></i>Mi Perfil</a>
                        </li>
                        <li>
                            <a href="<?= URL::to('/cerrar/sesion') ?>"><i class="icon_key_alt"></i>Cerrar sesión</a>
                        </li>
                    </ul>
                </li>
                <!-- user login dropdown end -->

            </ul>
        <!-- notificatoin dropdown end-->
        </div>
    </header>
    <!--header end-->
    <?= (new View())->includeFile(PATH_TEMPLATES . 'menus'); ?>""" % module
        return result

    def menus_get(self):
        """Menús del panel lateral izquierdo según las clases declaradas"""
        result = """    <!--sidebar start-->
    <aside>
        <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
            <li class="">
                <a class="" href="<?= URL::base() ?>">
                    <i class="icon_house_alt"></i>
                    <span>Tablero</span>
                </a>
            </li>
            <li class="sub-menu">
                <a href="javascript:;" class="">
                    <i class="icon_document_alt"></i>
                    <span>Formularios</span>
                    <span class="menu-arrow arrow_carrot-right"></span>
                </a>
                <ul class="sub">\n"""
        for sk in self.klass_names:
            data = self.class_name(sk)
            result += """                    <li><a class="" href="<?= URL::to('%(lcname)s') ?>">%(ucname)s</a></li>\n""" % data
        result += """                </ul>
            </li>
            <?php if ($_SESSION['usuario']['administrador']) { ?>
            <li class="sub-menu">
                <a href="javascript:;" class="">
                    <i class="icon_cog"></i>
                    <span>Configuración</span>
                    <span class="menu-arrow arrow_carrot-right"></span>
                </a>
                <ul class="sub">
                    <li><a class="" href="<?= URL::to('usuarios') ?>">Usuarios</a></li>
                    <li><a class="" href="<?= URL::to('roles') ?>">Roles</a></li>
                    <li><a class="" href="<?= URL::to('privilegios') ?>">Privilegios</a></li>
                </ul>
            </li>
            <?php } ?>
        </ul>
        <!-- sidebar menu end-->
        </div>
    </aside>
    <!--sidebar end-->"""
        return result

    def footer_get(self):
        """Pie de página de todas las vistas"""
        return """    <!-- javascripts -->
    <script src="<?= URL::to('assets/plugins/jquery/jquery-1.10.2.min.map.js') ?>"></script>
    <script src="<?= URL::to('assets/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
    <!-- nice scroll -->
    <script src="<?= URL::to('assets/plugins/jquery/jquery.scrollTo.min.js') ?>"></script>
    <script src="<?= URL::to('assets/plugins/jquery/jquery.nicescroll.js') ?>"></script>
    <!--custome script for all page-->
    <script src="<?= URL::to('assets/js/panel/scripts.js') ?>"></script>

</body>
</html>"""

    def error_404_get(self):
        """Mensaje de error con el código de estado 404"""
        return """<?php
    if (!isset($mensaje)) {
        $mensaje = "La página que estás solicitando no existe.";
    }
?>
<?= (new View())->includeFile(PATH_TEMPLATES . 'header'); ?>
    <div class="page-404">
        <p class="text-404">404</p>
        <h2>¡Error!</h2>
        <p><br><?= $mensaje ?><br><a href="<?= URL::base() ?>">Retorna al inicio</a></p>
    </div>
<?= (new View())->includeFile(PATH_TEMPLATES . 'footer'); ?>"""

    def error_401_get(self):
        """Mensaje de error con el código de estado 401"""
        return """<?php
    if (!isset($mensaje)) {
        $mensaje = "No estás autorizado para ejecutar esta acción.";
    }
?>
<?= (new View())->includeFile(PATH_TEMPLATES . 'header'); ?>
    <div class="page-404">
        <p class="text-404">401</p>
        <h2>¡Sin Autorización!</h2>
        <p><br><?= $mensaje ?><br><a href="<?= URL::base() ?>">Retorna al inicio</a></p>
    </div>
<?= (new View())->includeFile(PATH_TEMPLATES . 'footer'); ?>"""

# =============================
#
# ARCHIVOS DE CONFIGURACIÓN SRC
#
# =============================

    def roots_get(self):
        """Rutas globales para el autoloader"""
        return """<?php

define('PATH_APP', './app/');
define('PATH_CONFIG', './config/');
define('PATH_SRC', './src/');
define('PATH_ROUTES', './routes/');
define('PATH_CONTROLLERS', './app/controllers/');
define('PATH_VIEWS', './app/views/');
define('PATH_TEMPLATES', './templates/');

?>\n"""

    def launcher_get(self):
        """Lanzador del sistema"""
        return """<?php

    require './src/roots.php';
    require PATH_SRC . 'autoloader/autoloader.php';

    Autoloader::registrar();

    $rutas = scandir(PATH_ROUTES);

    foreach ($rutas as $archivo) {
        $rutaArchivo = realpath(PATH_ROUTES . $archivo);
        if (is_file($rutaArchivo)) {
            require $rutaArchivo;
        }
    }

    Route::submit();

?>\n"""

    def autoloader_get(self):
        """Autocargador de clases"""
        return """<?php

class Autoloader {

    public static function registrar() {
        if (function_exists('__autoload')) {
            spl_autoload_register('__autoload');
            return;
        }

        if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
            spl_autoload_register(array('Autoloader', 'cargar'), true, true);
        }
        else {
            spl_autoload_register(array('Autoloader', 'cargar'));
        }
    }

    public static function snake($value, $delimiter = '_')
    {
        if (! ctype_lower($value)) {
            $value = preg_replace('/\\s+/u', '', ucwords($value));
            $value = mb_strtolower(preg_replace('/(.)(?=[A-Z])/u', '$1'.$delimiter, $value));
        }

        return $value;
    }

    public static function cargar($clase) {
        if ($clase != 'EMensajes' and $clase != 'URL') {
            $clase = self::snake($clase);
        }
        if ($clase === 'ModeloGenerico') {
            $clase = 'modelo_generico';
        }
        if (strpos($clase, 'Controller') !== false and $clase != 'Controller') {
            $clase = str_replace('Controller', '_controller', $clase);
        }
        $nombreArchivo = strtolower($clase) . '.php';
        if ($clase === 'EMensajes') {
            $nombreArchivo = $clase . '.php';
        }
        $carpetas = require PATH_CONFIG . 'autoloader.php';
        foreach ($carpetas as $carpeta) {
            if (self::buscarArchivo($carpeta, $nombreArchivo)) {
                return true;
            }
        }
        return false;
    }

    private static function buscarArchivo($carpeta, $nombreArchivo) {
        $archivos = scandir($carpeta);
        if (strpos($carpeta, 'models') !== false) {
            $archivoModel = str_replace('.php', '_model.php', $nombreArchivo);
            $nombreArchivo = $archivoModel;
        }
        foreach ($archivos as $archivo) {
            $rutaArchivo = realpath($carpeta . DIRECTORY_SEPARATOR . $archivo);
            if (is_file($rutaArchivo)) {
                if ($nombreArchivo == $archivo) {
                    require_once $rutaArchivo;
                    return true;
                }
            }
            else if ($archivo != '.' && $archivo != '..') {
                self::buscarArchivo($rutaArchivo, $nombreArchivo);
            }
        }
        return false;
    }

}
?>\n"""

    def request_get(self):
        """Permite definir el archivo que parsea los datos enviados desde POST, GET
           u cualquier otro método"""
        return """<?php

class Request {

    protected $request;
    protected $data;
    public $method;

    public function __construct($request, $flag = true) {
        $this->request = $request;
        $this->extractData();
        $this->setExtraData($flag);
    }

    public function extractData() {
        $this->data = array();
        foreach ($this->request as $key => $value) {
            if (is_object($value) || is_array($value)) {
                $this->data[$key] = new Request($value, false);
            }
            else {
                if ($key != "http_referer") {
                    $this->data[$key] = $value;
                }
            }
        }
    }

    public function setExtraData($flag) {
        if ($flag == true) {
            $this->method = $_SERVER["REQUEST_METHOD"];
            $this->data["http_referer"] = isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : null;
            $headers = apache_request_headers();
            $this->data["headers"] = new Request($headers, false);
        }
    }

    public function __get($key) {
        return isset($this->data[$key]) ? $this->data[$key] : null;
    }

    public function __set($key, $value) {
        $this->data[$key] = $value;
    }

    public function all() {
        return $this->data;
    }

}

?>\n"""

    def route_get(self):
        """Archivo de configuración de las rutas personalizadas"""
        return """<?php

class Route {

    public function __construct() {

    }

    private static $uris = array();

    public static function add($method, $uri, $function = null) {
        Route::$uris[] = new Uri(self::parseUri($uri), $method, $function);
        // Retornará un Middleware...
        return;
    }

    public static function get($uri, $function = null) {
        return Route::add("GET", $uri, $function);
    }

    public static function post($uri, $function = null) {
        return Route::add("POST", $uri, $function);
    }

    public static function put($uri, $function = null) {
        return Route::add("PUT", $uri, $function);
    }

    public static function delete($uri, $function = null) {
        return Route::add("DELETE", $uri, $function);
    }

    public static function any($uri, $function = null) {
        return Route::add("ANY", $uri, $function);
    }

    private function parseUri($uri) {
        $uri = trim($uri, '/');
        $uri = (strlen($uri) > 0) ? $uri : '/';
        return $uri;
    }

    public static function submit() {
        $method = $_SERVER['REQUEST_METHOD'];
        $uri = isset($_GET['uri']) ? $_GET['uri'] : '';
        $uri = self::parseUri($uri);

        // Verifica si la uri que está pidiendo el usuario se encuentra registrada...
        foreach (Route::$uris as $key => $recordUri) {
            if ($recordUri->match($uri)) {
                return $recordUri->call();
            }
        }

        // Muestra el mensaje de error 404...
        $mensaje = 'La URL que está solicitando no existe.';
        (new View())->includeFile(PATH_TEMPLATES . '404', array('mensaje' => $mensaje));
        /*echo 'La uri (<a href="' . $uri . '">' . $uri . '</a>) no se encuentra registrada en el método ' . $method . '.';*/
    }

}

?>\n"""

    def uri_get(self):
        """Archivo que configura, llama y parsea los request y response al servidor"""
        return """<?php

class Uri {

    public $uri;
    public $method;
    public $function;
    public $matches;
    protected $request;
    protected $response;

    public function __construct($uri, $method, $function) {
        $this->uri = $uri;
        $this->method = $method;
        $this->function = $function;
    }

    public static function snake($value, $delimiter = '_')
    {
        if (! ctype_lower($value)) {
            $value = preg_replace('/\\s+/u', '', ucwords($value));
            $value = mb_strtolower(preg_replace('/(.)(?=[A-Z])/u', '$1'.$delimiter, $value));
        }

        return $value;
    }

    public function match($url) {
        $path = preg_replace('#:([\\w]+)#', '([^/]+)', $this->uri);
        $regex = "#^$path$#i";
        if (!preg_match($regex, $url, $matches)) {
            return false;
        }
        if ($this->method != $_SERVER['REQUEST_METHOD'] && $this->method != "ANY") {
            return false;
        }
        array_shift($matches);
        $this->matches = $matches;
        return true;
    }

    private function execFunction() {
        $this->parseRequest();
        $this->response = call_user_func_array($this->function, $this->matches);
    }

    private function formatCamelCase($string) {
        $parts = preg_split("[-|_]", strtolower($string));
        $finalString = "";
        $i = 0;
        foreach ($parts as $parts) {
            $finalString .= ($i = 0) ? strtolower($parts) : ucfirst($parts);
            $i++;
        }
        return $finalString;
    }

    private function getParts() {
        $parts = array();
        if (strpos($this->function, "@")) {
            $methodParts = explode("@", $this->function);
            $parts["class"] = $methodParts[0];
            $parts["method"] = $methodParts[1];
        } else {
            $parts["class"] = $this->function;
            $parts["method"] = ($this->uri == "/") ? "index" : $this->formatCamelCase($this->uri);
        }
        return $parts;
    }

    private function functionFromController() {
        $parts = $this->getParts();
        $class = $parts["class"];
        $method = $parts["method"];
        // Importamos el controlador...
        if (!$this->importController($class)) {
            return;
        }
        // Preparar la ejecución...
        $this->parseRequest();
        $classInstance = new $class();
        $classInstance->setRequest($this->request);
        // Lanzamos el método...
        $launch = array($classInstance, $method);
        if (is_callable($launch)) {
            $this->response = call_user_func_array($launch, $this->matches);
        }
        else {
            throw new Exception("El método $class.$method no existe.", -1);
        }
    }

    public function call() {
        try {
            $this->request = $_REQUEST;
            if (is_string($this->function)) {
                $this->functionFromController();
            }
            else {
                $this->execFunction();
            }
            $this->printResponse();
        }
        catch (Exception $excepcion) {
            echo "ERROR: " . $excepcion->getMessage();
        }
    }

    private function parseRequest() {
        $this->request = new Request($this->request);
        $this->matches[] = $this->request;
    }

    private function printResponse() {
        if (is_string($this->response)) {
            echo $this->response;
        }
        else if (is_object($this->response) || is_array($this->response)) {
            $res = new Respuesta();
            echo $res->json($this->response);
        }
    }

    public function importController($class) {
        $class = self::snake($class);
        $file = PATH_CONTROLLERS . strtolower(str_replace('_controller', "_controller.php", $class));
        if (!file_exists($file)) {
            throw new Exception("El controlador ($file) no existe.");
            return false;
        }
        require_once $file;
        return true;
    }

}

?>\n"""

    def respuesta_get(self):
        """Archivo que ensambla las respuestas ante las peticiones del cliente"""
        return """<?php

class Respuesta {

    public $codigo;
    public $mensaje;
    public $datos;

    function __construct($codigo = null, $mensaje = null, $datos = null) {
        // Obtener la respuesta por defecto por código.
        if (isset($codigo) && empty($mensaje)) {
            $respuesta = EMensajes::getMensaje($codigo);
            $this->codigo = $respuesta->codigo;
            $this->mensaje = $respuesta->mensaje;
            $this->datos = $respuesta->datos;
            return;
        }

        if (is_string($codigo)) {
            $temp = EMensajes::getMensaje($codigo);
            $codigo = $temp->codigo;
        }

        $this->codigo = $codigo;
        $this->mensaje = $mensaje;
        $this->datos = $datos;
    }

    public function json($objeto = null) {
        header('Content-Type: application/json');
        if (is_array($objeto) || is_object($objeto)) {
            return json_encode($objeto);
        }
        return json_encode($this);
    }

    function getCodigo() {
        return $this->codigo;
    }

    function getMensaje() {
        return $this->mensaje;
    }

    function getDatos() {
        return $this->datos;
    }

    function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    function setMensaje($mensaje) {
        $this->mensaje = $mensaje;
    }

    function setDatos($datos) {
        $this->datos = $datos;
    }

}
?>\n"""

    def url_get(self):
        """Archivo que reconoce y devuelve la URL y las rutas de la aplicación"""
        return """<?php

class URL {

    public static function base() {
        $base_dir = str_replace(basename($_SERVER["SCRIPT_NAME"]), "", $_SERVER["SCRIPT_NAME"]);
        $baseURL = (isset($_SERVER["HTTPS"]) ? "https" : "http") . "://{$_SERVER["HTTP_HOST"]}{$base_dir}";
        return trim($baseURL, "/");
    }

    public static function to($url) {
        $url = trim($url, "/");
        return URL::base() . "/{$url}";
    }

    public static function getFull() {
        return (isset($_SERVER["HTTPS"]) ? "https" : "http") . "://{$_SERVER["HTTP_HOST"]}{$_SERVER["REQUEST_URI"]}";
    }

}

?>\n"""

    def view_get(self):
        """Devuelve el archivo de configuración para renderizar las vistas"""
        return """<?php

class View {

    protected $variables;
    protected $ouput;

    function __construct() {

    }

    public function render($file, $variables = null, $template = false) {
        $this->variables = $variables;
        $file = ($template != true) ? PATH_VIEWS . $file : PATH_TEMPLATES . $file;
        ob_start();
        $this->includeFile($file);
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }

    public function includeFile($file, $variables = null) {
        if ($variables != null) {
            $this->variables = $variables;
        }
        // Creamos las variables en el contexto actual...
        if (isset($this->variables) && is_array($this->variables)) {
            foreach ($this->variables as $key => $value) {
                global ${$key};
                ${$key} = $value;
            }
        }

        if (file_exists($file)) {
            return include $file;
        }
        else
            if (file_exists($file . ".php")) {
                return include $file . ".php";
            }
            else
                if (file_exists($file . ".html")) {
                    return include $file . ".html";
                }
                else
                    if (file_exists($file . ".htm")) {
                            return include $file . ".html";
                    }
                    else {
                            $mensaje = "No existe el archivo: $file";
                            return $this->includeFile(PATH_TEMPLATES . '404', array('mensaje' => $mensaje));
                    }
    }

}

?>\n"""

    def csrf_token_get(self):
        """Archivo que configura la validación de seguridad contra
           falsificación de petición en sitios cruzados"""
        return """<?php

class csrfToken {

    public function __construct() {
    }

    public static function csrfToken() {
        if (!isset($_SESSION['usuario'])) {
            return false;
        }
        if (isset($_SESSION['csrf_token'])) {
            unset($_SESSION['csrf_token']);
        }
        $csrf_token = md5(uniqid(mt_rand(), true));
        $_SESSION['csrf_token'] = $csrf_token;
    }

    public static function validarCsrf() {
        session_start();
        if ($_SESSION['csrf_token'] != $_REQUEST["_token"]) {
            return false;
        }
        return true;
    }

}

?>"""

    def xss_input_get(self):
        """Archivo que establece la validación contra comandos en sitios cruzados
           al momento de recibir datos por el método POST"""
        return """<?php

class xssInput {

    public function __construct() {
    }

    public static function validacionXss($valor) {
        $valor = trim($valor);
        if (is_string($valor)) {
            // $valor = mb_strtoupper($valor, 'UTF-8');  // Descomenta si quieres guardar en BD los strings en MAYUSCULAS
            $valor = str_replace("'", "", $valor);
            $valor = str_replace("\\"", "", $valor);
            $valor = stripcslashes($valor);
            $valor = strip_tags($valor);
        }
        return $valor;
    }

}

?>"""

    def privileges_get(self):
        """Archivo que establece la configuración de permisos según los
           privilegios y el rol del usuario logueado"""
        return """<?php

class Permisos {

    function __construct() {
    }

    public static function permisosCRUD($entidad, $opcion) {
        if (!isset($_SESSION)) {
            session_start();
        }
        $login = array("Privilegios", "Roles", "Usuarios");
        if (!isset($_SESSION['usuario']['privilegios'])) { return false; }
        if ($_SESSION['usuario']['administrador']) { return true; }
        if (in_array($entidad, $login)) { return false; }
        $conteo = count($_SESSION['usuario']['privilegios']);
        for ($i = 0; $i < $conteo; $i++)
        {
            $existe = ($_SESSION['usuario']['privilegios'][$i]->entidad == $entidad);
            if ($existe) {
                if ($_SESSION['usuario']['privilegios'][$i]->{$opcion} == 1) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function login_user($id) {
        if (!isset($_SESSION)) {
            session_start();
        }
        if ($id == $_SESSION['usuario']['id'] OR $_SESSION['usuario']['administrador']) {
            return true;
        }
        else {
            return false;
        }
    }


}

?>"""

# =============================
#
#    ARCHIVOS DE RECURSOS JS
#
# =============================

    def app_global_get(self):
        """Establece las funciones necesarias para recibir y enviar
           DATA al servidor, con AJAX"""
        return """var __app = {
    urlbase: null,
    init: function () {
        __app.urlbase = ($('body').attr('data-urlbase')) ? $('body').attr('data-urlbase').trim('/') + '/' : '/';
    },
    urlTo: function (url) {
        return __app.urlbase + url;
    },
    validarRespuesta: function (respuesta) {
        if ((respuesta && !respuesta.codigo) || respuesta.codigo < 0) {
            respuesta = false;
        }
        return respuesta;
    },
    parsearRespuesta: function (respuesta) {
        var datos = __app.validarRespuesta(respuesta);
        if (datos) {
            return datos.datos;
        } else {
            return false;
        }
    },
    detenerEvento: function (e) {
        if (e) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            if (e.stopPropagation) {
                e.stopPropagation();
            }
            if (!!e.returnValue) {
                e.returnValue = false;
            }
        }
        return;
    },
    get: function (url, data) {
        var ajax = __app.getObjectAjax(url, data, "GET");
        return $.extend({ajax: ajax}, __app.methods);
    },
    post: function (url, data) {
        var ajax = __app.getObjectAjax(url, data, "POST");
        return $.extend({ajax: ajax}, __app.methods);
    },
    methods: {
        beforeSend: function (callback) {
            this.ajax.beforeSend = callback;
            return this;
        },
        complete: function (callback) {
            this.ajax.complete = callback;
            return this;
        },
        success: function (callback) {
            this.ajax.success = callback;
            return this;
        },
        error: function (callback) {
            this.ajax.error = callback;
            return this;
        },
        send: function () {
            __app.ajax(this.ajax);
        }
    },
    getObjectAjax: function (url, data, method) {
        var ajax = new Object();
        ajax.url = url;
        ajax.data = data;
        ajax.type = method;
        return ajax;
    },
    ajax: function (args) {
        var ajax = new Object();
        ajax.url = (__app.urlbase + args.url);
        ajax.type = (args.type) ? args.type : "POST";
        ajax.data = (args.data);
        ajax.dataType = (args.dataType) ? args.dataType : "json";
        ajax.beforeSend = args.beforeSend;
        ajax.complete = args.complete;
        ajax.success = args.success;
        ajax.error = args.error;
        $.ajax(ajax);
    },
};
$(__app.init());"""

    def helperform_get(self):
        """Archivo de JS que gestiona la DATA enviada desde un <form> HTML para
           ensamblarlos como objetos"""
        return """/*
 @author: Starlly
 Creado por Starlly Software : http://starlly.com.
 Licencia gratuita, mantenga este encabezado de archivo para no infringir los derechos
 de autor.
 */
var HelperForm = {
    CONSTANTS: {
        FILL_FORM: 'FILL_FORM',
        GET_FORM_DATA: 'GET_FORM_DATA'
    },
    configuration: {
        checkboxTrueValue: true,
        checkboxFalseValue: false,
    },
    utils: {
        /**
         * Recibe dos objetos, donde el dos será copiado en el objeto 1.
         * Generalmente la función de JQuery $.extends hace lo mismo, pero he visto que
         * tiene problemas al tratar arreglos ya que simplemente reemplaza los del objeto 1
         * con los del objeto 2.
         * @param {type} obj1
         * @param {type} obj2
         * @returns mergedObject
         */
        mergeObj: function (obj1, obj2) {
            for (var key in obj2) {
                if (typeof obj2[key] != "number" && typeof obj2[key] != "undefined" && obj2[key] != null && obj2[key].constructor()) {
                    if (Array.isArray(obj2[key])) {
                        if (!obj1[key]) {
                            obj1[key] = [];
                        }
                        for (var i = 0; i < obj2[key].length; i++) {
                            obj1[key].push(obj2[key][i]);
                        }
                    } else {
                        if (typeof obj1[key] == "undefined") {
                            obj1[key] = new Object();
                        }
                        HelperForm.utils.mergeObj(obj1[key], obj2[key]);
                    }
                } else {
                    obj1[key] = obj2[key];
                }
            }
        },
        /**
         * Comprueba si un string es válido y hace un UpperCase a su contenido.
         * @param {type} string
         * @returns {String}
         */
        upperCase: function (string) {
            if (typeof string === 'string') {
                return string.toUpperCase();
            }
        }
    }
};

/**
 * Había creado un objeto para ejecutar las acciones del algoritmo pero supuce que
 * tendría problemas de referencia si se llama asincronamente desde varias sentencias
 * por tanto empaqueté el objeto en un prototipo que me devuelve una instancia nueva
 * por cada vez que se invoque el plugin.
 * Lo que para este algoritmo es escencial ya que cada formulario debe manejar
 * una instancia diferente.
 * @returns {Objeto}
 */
HelperFormPrototype = function () {
    var HForm = {
        CONSTANTS: HelperForm.CONSTANTS,
        ELEMENT_TYPES: {
            SELECT: 'SELECT',
            CHECKBOX: 'CHECKBOX',
            RADIO: 'RADIO'
        },
        configuration: HelperForm.configuration,
        fillForm: {
            variables: {
                data: null
            },
            controls: {
                form: null
            },
            init: function (form, data) {
                HForm.fillForm.controls.form = form;
                HForm.fillForm.variables.data = data;
                HForm.fillForm.finder(form, '', data);
            },
            finder: function (form, parseKey, data) {
                if (data == null) {
                    return;
                }
                $.each(data, function (name, val) {
                    var $el = form.find('[name="' + parseKey + name + '"]');
                    var type = $el.attr('type');
                    if (typeof val === "object") {
                        var key = parseKey + "" + name + ".";
                        HForm.fillForm.finder(form, key, val);
                    } else {
                        HForm.fillForm.fill(type, $el, val);
                    }
                });
            },
            fill: function (type, el, val) {
                HForm.fillForm.utils.processMultiple(el, val);
                type = HelperForm.utils.upperCase(type);
                switch (type) {
                    case HForm.ELEMENT_TYPES.CHECKBOX:
                        HForm.fillForm.utils.process.checkbox(el, val);
                        break;
                    case HForm.ELEMENT_TYPES.RADIO:
                        HForm.fillForm.utils.process.radio(el, val);
                        break;
                    default:
                        HForm.fillForm.utils.process.defaultElement(el, val);
                        break;
                }
            },
            utils: {
                processMultiple: function ($el, val) {
                    if ($el.length > 1) {
                        for (var i = 0; i < $el.length; i++) {
                            var $elTemp = $($el[i]);
                            HForm.fillForm.fill($elTemp.attr('type'), $elTemp, val);
                        }
                    }
                },
                process: {
                    checkbox: function ($el, val) {
                        if (val == true || val == 1) {
                            $el.prop('checked', true);
                        } else {
                            $el.prop('checked', false);
                        }
                    },
                    radio: function ($el, val) {
                        $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    },
                    defaultElement: function ($el, val) {
                        var callback = $el.attr('data-callback');
                        if (callback) {
                            val = eval(callback + '("' + val + '", "' + HForm.CONSTANTS.FILL_FORM + '")');
                        }
                        $el.val(val);
                    }
                }
            }
        },
        getFormData: {
            variables: {
                object: new Object()
            },
            controls: {
                form: null,
                fields: null
            },
            init: function (form) {
                HForm.getFormData.controls.form = form;
                HForm.getFormData.controls.fields = form.find('input,select,textarea');
                HForm.getFormData.run();
                return HForm.getFormData.variables.object;
            },
            utils: {
                pushObject: function (name, val, nameEntity) {
                    if (!name) {
                        return;
                    }
                    var hasClass = typeof nameEntity === "string";
                    var parts = name.split(".");
                    var objFinal = null;
                    if (parts.length > 1) {
                        objFinal = HForm.getFormData.utils.processParts(name, val, objFinal, hasClass, nameEntity);
                    } else {
                        objFinal = HForm.getFormData.utils.processSimple(objFinal, hasClass, name, nameEntity, val);
                    }
                    HForm.utils.mergeObj(HForm.getFormData.variables.object, objFinal);
                },
                processParts: function (name, val, objFinal, hasClass, nameEntity) {
                    var parts = name.split(".");
                    var objTemp = new Object();
                    var temp = null;

                    for (var i = 0; i < parts.length; i++) {
                        if (i == 0) {
                            objTemp[parts[i]] = new Object();
                            temp = objTemp[parts[i]];
                        } else if (i == (parts.length - 1)) {
                            if (name.indexOf('[]') >= 0) {
                                if (!Array.isArray(temp[parts[i]])) {
                                    temp[parts[i]] = [];
                                }
                                temp[parts[i]].push(val);
                            } else {
                                temp[parts[i]] = val;
                            }
                        } else {
                            temp[parts[i]] = new Object();
                            temp = temp[parts[i]];
                        }
                    }
                    objFinal = new Object();
                    if (hasClass) {
                        objFinal[nameEntity] = objTemp;
                    } else {
                        objFinal = objTemp;
                    }
                    return objFinal;
                },
                processSimple: function (objFinal, hasClass, name, nameEntity, val) {
                    objFinal = new Object();
                    if (hasClass) {
                        if (name.indexOf('[]') >= 0) {
                            if (!HForm.getFormData.variables.object[nameEntity]) {
                                HForm.getFormData.variables.object[nameEntity] = new Object();
                            }
                            if (!Array.isArray(HForm.getFormData.variables.object[nameEntity][name])) {
                                HForm.getFormData.variables.object[nameEntity][name] = [];
                            }
                            HForm.getFormData.variables.object[nameEntity][name].push(val);
                        } else {
                            objFinal[nameEntity] = new Object();
                            objFinal[nameEntity][name] = val;
                        }
                    } else {
                        if (name.indexOf('[]') >= 0) {
                            if (!Array.isArray(HForm.getFormData.variables.object[name])) {
                                HForm.getFormData.variables.object[name] = [];
                            }
                            HForm.getFormData.variables.object[name].push(val);
                        } else {
                            objFinal[name] = val;
                        }
                    }
                    return objFinal;
                },
                processElement: function (form, el) {
                    var elementName = el.attr('name');
                    var hasClass = ((el.attr("data-class")) ? true : false);
                    var entityName = ((hasClass) ? el.attr("data-class") + "." : null);
                    var type = HelperForm.utils.upperCase(el.attr('type'));
                    switch (type) {
                        case HForm.ELEMENT_TYPES.RADIO:
                            HForm.getFormData.utils.process.radio(form, elementName, entityName);
                            break;
                        case HForm.ELEMENT_TYPES.CHECKBOX:
                            HForm.getFormData.utils.process.checkbox(el, elementName, entityName);
                            break;
                        default:
                            HForm.getFormData.utils.process.defaultElement(el, elementName, entityName);
                            break;
                    }
                },
                process: {
                    checkbox: function (el, elementName, entityName) {
                        HForm.getFormData.utils.pushObject(elementName, ((el.is(":checked")) ? HForm.configuration.checkboxTrueValue : HForm.configuration.checkboxFalseValue), entityName);
                    },
                    radio: function (form, elementName, entityName) {
                        var valTemp = form.find('[name="' + elementName + '"]:checked').val();
                        HForm.getFormData.utils.pushObject(elementName, valTemp, entityName);
                    },
                    defaultElement: function (el, elementName, entityName) {
                        var val = el.val();
                        var callback = el.attr('data-callback');
                        if (callback) {
                            val = eval(callback + '("' + val + '", "' + HForm.CONSTANTS.GET_FORM_DATA + '")');
                        }
                        if (el.is(HForm.ELEMENT_TYPES.SELECT)) {
                            if (el.attr('data-value') && val.trim() == "") {
                                val = el.attr('data-value');
                            }
                        }
                        HForm.getFormData.utils.pushObject(elementName, val, entityName);
                    }
                }
            },
            run: function () {
                $.each(HForm.getFormData.controls.fields, function (i, el) {
                    el = $(el);
                    HForm.getFormData.utils.processElement(HForm.getFormData.controls.form, el);
                });
            }
        },
        utils: HelperForm.utils
    };
    this.instance = HForm;
};


/**
 * Recibe un objeto y llena el formulario o contenedor que ha invocado el prototipo.
 * @param {type} obj : Recibe el objeto que se usará para llenar el formulario.
 * @returns {Element}
 */
$.fn.fillForm = function (data) {
    if (typeof this !== "object") {
        console.error("Error fillForm: El objeto seleccionado no es un elemento del DOM.");
        return;
    }
    var HForm = new HelperFormPrototype();
    return HForm.instance.fillForm.init($(this), data);
};

/**
 * Al invocarse desde un formulario, este prototipo obtendrá el modelo de datos en un objeto JSON.
 * @returns {Object}
 */
$.fn.getFormData = function () {
    var HForm = new HelperFormPrototype();
    return HForm.instance.getFormData.init($(this));
};"""

    def routes_api(self):
        """Ensambla las rutas de los métodos en backend que serán llamadas
           desde el cliente"""
        result = """var RUTAS_API = {
    USUARIOS: {
        LISTAR: 'listar_usuarios',
        REGISTRAR_USUARIOS: 'usuarios/registrar',
        ACTUALIZAR_USUARIOS: 'usuarios/actualizar',
        ELIMINAR: 'usuarios/eliminar',
        FORM_EDITAR: 'usuarios/form/editar',
        LOGIN: 'iniciar/sesion',
        CAMBIAR_CLAVE: 'usuarios/cambiar/clave',
        ACTUALIZAR_CLAVE: 'usuarios/actualizar/clave',
    },

    ROLES: {
        LISTAR: 'listar_roles',
        REGISTRAR_ROLES: 'roles/registrar',
        ACTUALIZAR_ROLES: 'roles/actualizar',
        ELIMINAR: 'roles/eliminar',
        FORM_EDITAR: 'roles/form/editar',
    },

    PRIVILEGIOS: {
        LISTAR: 'listar_privilegios',
        REGISTRAR_PRIVILEGIOS: 'privilegios/registrar',
        ACTUALIZAR_PRIVILEGIOS: 'privilegios/actualizar',
        ELIMINAR: 'privilegios/eliminar',
        FORM_EDITAR: 'privilegios/form/editar',
    },\n\n"""
        for sk in self.klass_names:
            data = self.class_name(sk)
            result += """    %(ucname)s: {
        LISTAR: 'listar_%(lcname)s',
        REGISTRAR_%(ucname)s: '%(lcname)s/registrar',
        ACTUALIZAR_%(ucname)s: '%(lcname)s/actualizar',
        ELIMINAR: '%(lcname)s/eliminar',
        FORM_EDITAR: '%(lcname)s/form/editar',
    },\n\n""" % data
        result += "};"
        return result

    def font_awesone_get(self):
        """Archivo JS minificado de la librería de font awesone"""
        return """window.FontAwesomeKitConfig = {"asyncLoading":{"enabled":false},"autoA11y":{"enabled":true},"baseUrl":"https://kit-free.fontawesome.com","detectConflictsUntil":null,"license":"free","method":"css","minify":{"enabled":true},"v4FontFaceShim":{"enabled":true},"v4shim":{"enabled":true},"version":"latest"};
!function(){function r(e){var t,n=[],i=document,o=i.documentElement.doScroll,r="DOMContentLoaded",a=(o?/^loaded|^c/:/^loaded|^i|^c/).test(i.readyState);a||i.addEventListener(r,t=function(){for(i.removeEventListener(r,t),a=1;t=n.shift();)t()}),a?setTimeout(e,0):n.push(e)}!function(){if(!(void 0===window.Element||"classList"in document.documentElement)){var e,t,n,i=Array.prototype,o=i.push,r=i.splice,a=i.join;d.prototype={add:function(e){this.contains(e)||(o.call(this,e),this.el.className=this.toString())},contains:function(e){return-1!=this.el.className.indexOf(e)},item:function(e){return this[e]||null},remove:function(e){if(this.contains(e)){for(var t=0;t<this.length&&this[t]!=e;t++);r.call(this,t,1),this.el.className=this.toString()}},toString:function(){return a.call(this," ")},toggle:function(e){return this.contains(e)?this.remove(e):this.add(e),this.contains(e)}},window.DOMTokenList=d,e=Element.prototype,t="classList",n=function(){return new d(this)},Object.defineProperty?Object.defineProperty(e,t,{get:n}):e.__defineGetter__(t,n)}function d(e){for(var t=(this.el=e).className.replace(/^\\s+|\\s+$/g,"").split(/\\s+/),n=0;n<t.length;n++)o.call(this,t[n])}}();function a(e){var t,n,i,o;prefixesArray=e||["fa"],prefixesSelectorString="."+Array.prototype.join.call(e,",."),t=document.querySelectorAll(prefixesSelectorString),Array.prototype.forEach.call(t,function(e){n=e.getAttribute("title"),e.setAttribute("aria-hidden","true"),i=!e.nextElementSibling||!e.nextElementSibling.classList.contains("sr-only"),n&&i&&((o=document.createElement("span")).innerHTML=n,o.classList.add("sr-only"),e.parentNode.insertBefore(o,e.nextSibling))})}var d=function(e,t){var n=document.createElement("link");n.href=e,n.media="all",n.rel="stylesheet",n.id="font-awesome-5-kit-css",t&&t.detectingConflicts&&t.detectionIgnoreAttr&&n.setAttributeNode(document.createAttribute(t.detectionIgnoreAttr)),document.getElementsByTagName("head")[0].appendChild(n)},c=function(e,t){!function(e,t){var n,i=t&&t.before||void 0,o=t&&t.media||void 0,r=window.document,a=r.createElement("link");if(t&&t.detectingConflicts&&t.detectionIgnoreAttr&&a.setAttributeNode(document.createAttribute(t.detectionIgnoreAttr)),i)n=i;else{var d=(r.body||r.getElementsByTagName("head")[0]).childNodes;n=d[d.length-1]}var c=r.styleSheets;a.rel="stylesheet",a.href=e,a.media="only x",function e(t){if(r.body)return t();setTimeout(function(){e(t)})}(function(){n.parentNode.insertBefore(a,i?n:n.nextSibling)});var s=function(e){for(var t=a.href,n=c.length;n--;)if(c[n].href===t)return e();setTimeout(function(){s(e)})};function l(){a.addEventListener&&a.removeEventListener("load",l),a.media=o||"all"}a.addEventListener&&a.addEventListener("load",l),(a.onloadcssdefined=s)(l)}(e,t)},e=function(e,t,n){var i=t&&void 0!==t.autoFetchSvg?t.autoFetchSvg:void 0,o=t&&void 0!==t.async?t.async:void 0,r=t&&void 0!==t.autoA11y?t.autoA11y:void 0,a=document.createElement("script"),d=document.scripts[0];a.src=e,void 0!==r&&a.setAttribute("data-auto-a11y",r?"true":"false"),i&&(a.setAttributeNode(document.createAttribute("data-auto-fetch-svg")),a.setAttribute("data-fetch-svg-from",t.fetchSvgFrom)),o&&a.setAttributeNode(document.createAttribute("defer")),n&&n.detectingConflicts&&n.detectionIgnoreAttr&&a.setAttributeNode(document.createAttribute(n.detectionIgnoreAttr)),d.parentNode.appendChild(a)};function s(e,t){var n=t&&t.addOn||"",i=t&&t.baseFilename||e.license+n,o=t&&t.minify?".min":"",r=t&&t.fileSuffix||e.method,a=t&&t.subdir||e.method;return e.baseUrl+"/releases/"+("latest"===e.version?"latest":"v".concat(e.version))+"/"+a+"/"+i+o+"."+r}var t,n,i,o,l;try{if(window.FontAwesomeKitConfig){var u,f=window.FontAwesomeKitConfig,m={detectingConflicts:f.detectConflictsUntil&&new Date<=new Date(f.detectConflictsUntil),detectionIgnoreAttr:"data-fa-detection-ignore",detectionTimeoutAttr:"data-fa-detection-timeout",detectionTimeout:null};"js"===f.method&&(o=m,l={async:(i=f).asyncLoading.enabled,autoA11y:i.autoA11y.enabled},"pro"===i.license&&(l.autoFetchSvg=!0,l.fetchSvgFrom=i.baseUrl+"/releases/"+("latest"===i.version?"latest":"v".concat(i.version))+"/svgs"),i.v4shim.enabled&&e(s(i,{addOn:"-v4-shims",minify:i.minify.enabled})),e(s(i,{minify:i.minify.enabled}),l,o)),"css"===f.method&&function(e,t){var n,i=a.bind(a,["fa","fab","fas","far","fal","fad"]);e.autoA11y.enabled&&(r(i),n=i,"undefined"!=typeof MutationObserver&&new MutationObserver(n).observe(document,{childList:!0,subtree:!0})),e.v4shim.enabled&&(e.license,e.asyncLoading.enabled?c(s(e,{addOn:"-v4-shims",minify:e.minify.enabled}),t):d(s(e,{addOn:"-v4-shims",minify:e.minify.enabled}),t));e.v4FontFaceShim.enabled&&(e.asyncLoading.enabled?c(s(e,{addOn:"-v4-font-face",minify:e.minify.enabled}),t):d(s(e,{addOn:"-v4-font-face",minify:e.minify.enabled}),t));var o=s(e,{minify:e.minify.enabled});e.asyncLoading.enabled?c(o,t):d(o,t)}(f,m),m.detectingConflicts&&((u=document.currentScript.getAttribute(m.detectionTimeoutAttr))&&(m.detectionTimeout=u),document.currentScript.setAttributeNode(document.createAttribute(m.detectionIgnoreAttr)),t=f,n=m,r(function(){var e=document.createElement("script");n&&n.detectionIgnoreAttr&&e.setAttributeNode(document.createAttribute(n.detectionIgnoreAttr)),n&&n.detectionTimeoutAttr&&n.detectionTimeout&&e.setAttribute(n.detectionTimeoutAttr,n.detectionTimeout),e.src=s(t,{baseFilename:"conflict-detection",fileSuffix:"js",subdir:"js",minify:t.minify.enabled}),e.async=!0,document.body.appendChild(e)}))}}catch(e){}}();"""

    def panel_scripts(self):
        """Comportamiento JS del panel lateral izquierdo"""
        return """function initializeJS() {

    //tool tips
    jQuery('.tooltips').tooltip();

    //popovers
    jQuery('.popovers').popover();

    //custom scrollbar
        //for html
    jQuery("html").niceScroll({styler:"fb",cursorcolor:"#007AFF", cursorwidth: '6', cursorborderradius: '10px', background: '#F7F7F7', cursorborder: '', zindex: '1000'});
        //for sidebar
    jQuery("#sidebar").niceScroll({styler:"fb",cursorcolor:"#007AFF", cursorwidth: '3', cursorborderradius: '10px', background: '#F7F7F7', cursorborder: ''});
        // for scroll panel
    jQuery(".scroll-panel").niceScroll({styler:"fb",cursorcolor:"#007AFF", cursorwidth: '3', cursorborderradius: '10px', background: '#F7F7F7', cursorborder: ''});

    //sidebar dropdown menu
    jQuery('#sidebar .sub-menu > a').click(function () {
        var last = jQuery('.sub-menu.open', jQuery('#sidebar'));
        jQuery('.menu-arrow').removeClass('arrow_carrot-right');
        jQuery('.sub', last).slideUp(200);
        var sub = jQuery(this).next();
        if (sub.is(":visible")) {
            jQuery('.menu-arrow').addClass('arrow_carrot-right');
            sub.slideUp(200);
        } else {
            jQuery('.menu-arrow').addClass('arrow_carrot-down');
            sub.slideDown(200);
        }
        var o = (jQuery(this).offset());
        diff = 200 - o.top;
        if(diff>0)
            jQuery("#sidebar").scrollTo("-="+Math.abs(diff),500);
        else
            jQuery("#sidebar").scrollTo("+="+Math.abs(diff),500);
    });

    // sidebar menu toggle
    jQuery(function() {
        function responsiveView() {
            var wSize = jQuery(window).width();
            if (wSize <= 768) {
                jQuery('#container').addClass('sidebar-close');
                jQuery('#sidebar > ul').hide();
            }

            if (wSize > 768) {
                jQuery('#container').removeClass('sidebar-close');
                jQuery('#sidebar > ul').show();
            }
        }
        jQuery(window).on('load', responsiveView);
        jQuery(window).on('resize', responsiveView);
    });

    jQuery('.toggle-nav').click(function () {
        if (jQuery('#sidebar > ul').is(":visible") === true) {
            jQuery('#main-content').css({
                'margin-left': '0px'
            });
            jQuery('#sidebar').css({
                'margin-left': '-180px'
            });
            jQuery('#sidebar > ul').hide();
            jQuery("#container").addClass("sidebar-closed");
        } else {
            jQuery('#main-content').css({
                'margin-left': '180px'
            });
            jQuery('#sidebar > ul').show();
            jQuery('#sidebar').css({
                'margin-left': '0'
            });
            jQuery("#container").removeClass("sidebar-closed");
        }
    });

    //bar chart
    if (jQuery(".custom-custom-bar-chart")) {
        jQuery(".bar").each(function () {
            var i = jQuery(this).find(".value").html();
            jQuery(this).find(".value").html("");
            jQuery(this).find(".value").animate({
                height: i
            }, 2000)
        })
    }

}

jQuery(document).ready(function(){
    initializeJS();
});"""

# =============================
#
#    ARCHIVOS JS ESTÁTICOS
#
# =============================

    def change_password_js(self):
        """JS para cambiar la clave del usuario"""
        return """var vista = {
    controles: {
        formCambiarClave: $('#formCambiarClave'),
    },
    init: function () {
        vista.controles.formCambiarClave.find('#password,#password2').val('');
        vista.eventos();
    },
    eventos: function () {
        vista.controles.formCambiarClave.on('submit', vista.callbacks.eventos.accionesFormRegistro.ejecutar);
    },
    callbacks: {
        eventos: {
            accionesFormRegistro: {
                ejecutar: function (evento) {
                    __app.detenerEvento(evento);
                    var form = vista.controles.formCambiarClave;
                    var objeto = form.getFormData();
                    vista.peticiones.actualizarClave(objeto);
                }
            }
        },
        peticiones: {
            beforeSend: function () {
                vista.controles.formCambiarClave.find('input,select,button').prop('disabled', true);
            },
            completo: function () {
                vista.controles.formCambiarClave.find('input,select,button').prop('disabled', false);
            },
            finalizado: function (respuesta) {
                if (__app.validarRespuesta(respuesta)) {
                    vista.controles.formCambiarClave.find('input').val('');
                    vista.controles.formCambiarClave.find('input').val('');
                    swal('Correcto', respuesta.mensaje, 'success', {
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                    }).then((value) => {
                        if (value) {
                            location.reload();
                        }
                    });
                    return;
                }
                swal('Error', respuesta.mensaje, 'error', {
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                });
            }
        }
    },
    peticiones: {
        actualizarClave: function (objeto) {
            __app.post(RUTAS_API.USUARIOS.ACTUALIZAR_CLAVE, objeto)
                    .beforeSend(vista.callbacks.peticiones.beforeSend)
                    .complete(vista.callbacks.peticiones.completo)
                    .success(vista.callbacks.peticiones.finalizado)
                    .error(vista.callbacks.peticiones.finalizado)
                    .send();
        }
    }
};
$(vista.init);"""

    def update_user_js(self):
        """JS para actualizar formulario del usuario"""
        return """var vista = {
    controles: {
        formUsuarios: $('#formUsuarios'),
        body: $('#bodyUsuarios'),
        contenido: $('#contenido'),
    },
    init: function () {
        vista.eventos();
    },
    eventos: function () {
        vista.controles.formUsuarios.on('submit', vista.callbacks.eventos.accionesFormRegistro.ejecutar);
        vista.controles.body.on('click', '.cambiar-clave', vista.callbacks.eventos.accionesFormRegistro.cambiarClave);
    },
    callbacks: {
        eventos: {
            accionesFormRegistro: {
                ejecutar: function (evento) {
                    evento.preventDefault();
                    evento.stopImmediatePropagation();
                    __app.detenerEvento(evento);
                    var form = vista.controles.formUsuarios;
                    var objeto = form.getFormData();
                    vista.peticiones.actualizarUsuarios(objeto);
                },
                cambiarClave: function (evento) {
                    var value = $(this).attr("value");
                    var id = {"id": value};
                    vista.peticiones.cambiarClave(id);
                }
            }
        },
        peticiones: {
            beforeSend: function () {
                vista.controles.formUsuarios.find('input,select,textarea,button').prop('disabled', true);
            },
            completo: function () {
                vista.controles.formUsuarios.find('input,select,textarea,button').prop('disabled', false);
            },
            finalizado: function (respuesta) {
                if (__app.validarRespuesta(respuesta)) {
                    swal('Correcto', respuesta.mensaje, 'success', {
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                    }).then((value) => {
                        if (value) {
                            location.reload();
                        }
                    });
                    return;
                }
                swal('Error', respuesta.mensaje, 'error', {
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                    }).then((value) => {
                    if (value) {
                        location.reload();
                    }
                });
            },
            cambiarClave: function (respuesta) {
                var datos = __app.parsearRespuesta(respuesta);
                vista.controles.contenido.empty();
                vista.controles.contenido.html(datos);
            }
        }
    },
    peticiones: {
        actualizarUsuarios: function (objeto) {
            __app.post(RUTAS_API.USUARIOS.ACTUALIZAR_USUARIOS, objeto)
                    .beforeSend(vista.callbacks.peticiones.beforeSend)
                    .complete(vista.callbacks.peticiones.completo)
                    .success(vista.callbacks.peticiones.finalizado)
                    .error(vista.callbacks.peticiones.finalizado)
                    .send();
        },
        cambiarClave : function (id) {
            __app.post(RUTAS_API.USUARIOS.CAMBIAR_CLAVE, id)
                    .success(vista.callbacks.peticiones.cambiarClave)
                    .send();
        }
    }
};
$(vista.init);"""

    def list_user_js(self):
        """JS para listar usuario dinamicamente"""
        return """var vista = {
    controles: {
        tbodyListaUsuarios: $('#tablaListaUsuarios tbody')
    },
    init: function () {
        vista.eventos();
        vista.peticiones.listarUsuarios();
    },
    eventos: function () {
        vista.controles.tbodyListaUsuarios.on("click", ".eliminar", vista.callbacks.eventos.eliminarRegistro.ejecutar);
        vista.controles.tbodyListaUsuarios.on("click", ".editar", vista.callbacks.eventos.editarRegistro.ejecutar);
    },
    callbacks: {
        eventos: {
            eliminarRegistro: {
                ejecutar: function (evento) {
                    __app.detenerEvento(evento);
                    var value = $(this).attr("value");
                    var id = {"id": value};
                    swal({
                        title: '¿Seguro que desea eliminar el registro?',
                        text: "No podrá revertir este cambio",
                        icon: 'warning',
                        buttons: {
                            cancel: "Cancelar",
                            confirmar: {
                                text: "Confirmar",
                                value: true
                            },
                        },
                        dangerMode: true,
                    }).then((value) => {
                            if (value) {
                            vista.peticiones.eliminarUsuarios(id);
                        }
                    });
                }
            },
            editarRegistro: {
                ejecutar: function() {
                    var value = $(this).attr("value");
                    vista.peticiones.editarUsuarios(value);
                }
            }
        },
        peticiones: {
            listarUsuarios: {
                beforeSend: function () {
                    var tbody = vista.controles.tbodyListaUsuarios;
                    tbody.html(vista.utils.templates.consultando());
                },
                completo: function (respuesta) {
                    var tbody = vista.controles.tbodyListaUsuarios;
                    var datos = __app.parsearRespuesta(respuesta);
                    if (datos && datos.length > 0) {
                        tbody.html('');
                        for (var i = 0; i < datos.length; i++) {
                            var dato = datos[i];
                            tbody.append(vista.utils.templates.item(dato));
                        }
                    } else {
                        tbody.html(vista.utils.templates.noHayRegistros());
                    }
                }
            },
            eliminarUsuarios: {
                beforeSend: function () {
                    // Validación antes del click
                },
                completo: function () {
                    vista.peticiones.listarUsuarios();
                },
                finalizado: function (respuesta) {
                    if (__app.validarRespuesta(respuesta)) {
                        swal('Correcto', respuesta.mensaje, 'success', {
                             closeOnClickOutside: false,
                             closeOnEsc: false,
                        });
                        return;
                    }
                    swal('Error', respuesta.mensaje, 'error', {
                         closeOnClickOutside: false,
                         closeOnEsc: false,
                    });
                }
            },
            editarUsuarios: {
                completo: function (id) {
                    document.location = __app.urlTo(`usuarios/form/ver?id=${id}`);
                }
            }
        }
    },
    peticiones: {
        listarUsuarios: function () {
            __app.get(RUTAS_API.USUARIOS.LISTAR)
                    .beforeSend(vista.callbacks.peticiones.listarUsuarios.beforeSend)
                    .success(vista.callbacks.peticiones.listarUsuarios.completo)
                    .error(vista.callbacks.peticiones.listarUsuarios.completo)
                    .send();
        },
        eliminarUsuarios: function (id) {
            __app.post(RUTAS_API.USUARIOS.ELIMINAR, id)
                    .beforeSend(vista.callbacks.peticiones.eliminarUsuarios.beforeSend)
                    .complete(vista.callbacks.peticiones.eliminarUsuarios.completo)
                    .success(vista.callbacks.peticiones.eliminarUsuarios.finalizado)
                    .error(vista.callbacks.peticiones.eliminarUsuarios.finalizado)
                    .send();
        },
        editarUsuarios: function (id) {
            __app.post(RUTAS_API.USUARIOS.FORM_EDITAR, id)
                    .complete(vista.callbacks.peticiones.editarUsuarios.completo(id))
                    .send();
        }
    },
    utils: {
        templates: {
            item: function (objeto) {
                var url = __app.urlTo('usuarios/editar/' + objeto.id)
                return `<tr value="${objeto.id}" class="editar" style="cursor: pointer;">`
                        + '<td>' + objeto.nombre + '</td>'
                        + '<td>' + objeto.correo + '</td>'
                        + '<td>' + objeto.activo + '</td>'
                        + '<td>' + objeto.ultima_conexion + '</td>'
                        + '<td>' + objeto.rol_id + '</td>'
                        + '<td>'
                        + '<div class="btn-group">'
                        + `<button value="${objeto.id}" class="btn-accion btn-info editar"><i class="icon_zoom-in_alt"></i></button>`
                        + `<button value="${objeto.id}" class="btn-accion btn-danger eliminar"><i class="icon_trash"></i></button>`
                        + '</div>'                        + '</td>'
                        + '</tr>';
            },
            consultando: function () {
                return '<tr><td colspan="6">Consultando...</td></tr>'
            },
            noHayRegistros: function () {
                return '<tr><td colspan="6">No hay registros...</td></tr>';
            }
        }
    },
};
$(vista.init);"""

    def register_user_js(self):
        """JS para mandar al backend los datos del formulario del usuario
           cuando se crea un registro"""
        return """var vista = {
    controles: {
        formUsuarios: $('#formUsuarios'),
    },
    init: function () {
        vista.eventos();
    },
    eventos: function () {
        vista.controles.formUsuarios.on('submit', vista.callbacks.eventos.accionesFormRegistro.ejecutar);
    },
    callbacks: {
        eventos: {
            accionesFormRegistro: {
                ejecutar: function (evento) {
                    __app.detenerEvento(evento);
                    var form = vista.controles.formUsuarios;
                    var objeto = form.getFormData();
                    vista.peticiones.registrarUsuarios(objeto);
                }
            }
        },
        peticiones: {
            beforeSend: function () {
                vista.controles.formUsuarios.find('input,select,button').prop('disabled', true);
            },
            completo: function () {
                vista.controles.formUsuarios.find('input,select,button').prop('disabled', false);
            },
            finalizado: function (respuesta) {
                if (__app.validarRespuesta(respuesta)) {
                    vista.controles.formUsuarios.find('input').val('');
                    vista.controles.formUsuarios.find('input').val('');
                    vista.controles.formUsuarios.find('select').prop('selectedIndex', 0);
                    var id = respuesta.datos;
                    swal('Correcto', respuesta.mensaje, 'success', {
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                    }).then((value) => {
                        if (value) {
                            document.location = __app.urlTo(`usuarios/form/ver?id=${id}`);
                        }
                    });
                    return;
                }
                swal('Error', respuesta.mensaje, 'error', {
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                });
            }
        }
    },
    peticiones: {
        registrarUsuarios: function (objeto) {
            __app.post(RUTAS_API.USUARIOS.REGISTRAR_USUARIOS, objeto)
                    .beforeSend(vista.callbacks.peticiones.beforeSend)
                    .complete(vista.callbacks.peticiones.completo)
                    .success(vista.callbacks.peticiones.finalizado)
                    .error(vista.callbacks.peticiones.finalizado)
                    .send();
        }
    }
};
$(vista.init);"""

    def update_privileges_js(self):
        """JS para actualizar formulario de los privilegios del usuario"""
        return """var vista = {
    controles: {
        formPrivilegios: $('#formPrivilegios'),
        body: $('#bodyPrivilegios'),
    },
    init: function () {
        vista.eventos();
        vista.controles.formPrivilegios.find('input,select,textarea').prop('readonly', false).prop('disabled', false);
    },
    eventos: function () {
        vista.controles.formPrivilegios.on('submit', vista.callbacks.eventos.accionesFormRegistro.ejecutar);
    },
    callbacks: {
        eventos: {
            accionesFormRegistro: {
                ejecutar: function (evento) {
                    evento.preventDefault();
                    evento.stopImmediatePropagation();
                    __app.detenerEvento(evento);
                    var form = vista.controles.formPrivilegios;
                    var objeto = form.getFormData();
                    vista.peticiones.actualizarPrivilegios(objeto);
                }
            }
        },
        peticiones: {
            beforeSend: function () {
                vista.controles.formPrivilegios.find('input,select,textarea,button').prop('disabled', true);
            },
            completo: function () {
                vista.controles.formPrivilegios.find('input,select,textarea,button').prop('disabled', false);
            },
            finalizado: function (respuesta) {
                if (__app.validarRespuesta(respuesta)) {
                    swal('Correcto', respuesta.mensaje, 'success', {
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                    }).then((value) => {
                        if (value) {
                            location.reload();
                        }
                    });
                    return;
                }
                swal('Error', respuesta.mensaje, 'error', {
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                });
            }
        }
    },
    peticiones: {
        actualizarPrivilegios: function (objeto) {
            __app.post(RUTAS_API.PRIVILEGIOS.ACTUALIZAR_PRIVILEGIOS, objeto)
                    .beforeSend(vista.callbacks.peticiones.beforeSend)
                    .complete(vista.callbacks.peticiones.completo)
                    .success(vista.callbacks.peticiones.finalizado)
                    .error(vista.callbacks.peticiones.finalizado)
                    .send();
        }
    }
};
$(vista.init);"""

    def list_privileges_js(self):
        """JS para listar los privilegios dinamicamente"""
        return """var vista = {
    controles: {
        tbodyListaPrivilegios: $('#tablaListaPrivilegios tbody')
    },
    init: function () {
        vista.eventos();
        vista.peticiones.listarPrivilegios();
    },
    eventos: function () {
        vista.controles.tbodyListaPrivilegios.on("click", ".eliminar", vista.callbacks.eventos.eliminarRegistro.ejecutar);
        vista.controles.tbodyListaPrivilegios.on("click", ".editar", vista.callbacks.eventos.editarRegistro.ejecutar);
    },
    callbacks: {
        eventos: {
            eliminarRegistro: {
                ejecutar: function (evento) {
                    __app.detenerEvento(evento);
                    var value = $(this).attr("value");
                    var id = {"id": value};
                    swal({
                        title: '¿Seguro que desea eliminar el registro?',
                        text: "No podrá revertir este cambio",
                        icon: 'warning',
                        buttons: {
                            cancel: "Cancelar",
                            confirmar: {
                                text: "Confirmar",
                                value: true
                            },
                        },
                        dangerMode: true,
                    }).then((value) => {
                            if (value) {
                            vista.peticiones.eliminarPrivilegios(id);
                        }
                    });
                }
            },
            editarRegistro: {
                ejecutar: function() {
                    var value = $(this).attr("value");
                    vista.peticiones.editarPrivilegios(value);
                }
            }
        },
        peticiones: {
            listarPrivilegios: {
                beforeSend: function () {
                    var tbody = vista.controles.tbodyListaPrivilegios;
                    tbody.html(vista.utils.templates.consultando());
                },
                completo: function (respuesta) {
                    var tbody = vista.controles.tbodyListaPrivilegios;
                    var datos = __app.parsearRespuesta(respuesta);
                    if (datos && datos.length > 0) {
                        tbody.html('');
                        for (var i = 0; i < datos.length; i++) {
                            var dato = datos[i];
                            tbody.append(vista.utils.templates.item(dato));
                        }
                    } else {
                        tbody.html(vista.utils.templates.noHayRegistros());
                    }
                }
            },
            eliminarPrivilegios: {
                beforeSend: function () {
                    // Validación antes del click
                },
                completo: function () {
                    vista.peticiones.listarPrivilegios();
                },
                finalizado: function (respuesta) {
                    if (__app.validarRespuesta(respuesta)) {
                        swal('Correcto', respuesta.mensaje, 'success', {
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                    });
                        return;
                    }
                    swal('Error', respuesta.mensaje, 'error', {
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                    });
                }
            },
            editarPrivilegios: {
                completo: function (id) {
                    document.location = __app.urlTo(`privilegios/form/ver?id=${id}`);
                }
            }
        }
    },
    peticiones: {
        listarPrivilegios: function () {
            __app.get(RUTAS_API.PRIVILEGIOS.LISTAR)
                    .beforeSend(vista.callbacks.peticiones.listarPrivilegios.beforeSend)
                    .success(vista.callbacks.peticiones.listarPrivilegios.completo)
                    .error(vista.callbacks.peticiones.listarPrivilegios.completo)
                    .send();
        },
        eliminarPrivilegios: function (id) {
            __app.post(RUTAS_API.PRIVILEGIOS.ELIMINAR, id)
                    .beforeSend(vista.callbacks.peticiones.eliminarPrivilegios.beforeSend)
                    .complete(vista.callbacks.peticiones.eliminarPrivilegios.completo)
                    .success(vista.callbacks.peticiones.eliminarPrivilegios.finalizado)
                    .error(vista.callbacks.peticiones.eliminarPrivilegios.finalizado)
                    .send();
        },
        editarPrivilegios: function (id) {
            __app.post(RUTAS_API.PRIVILEGIOS.FORM_EDITAR, id)
                    .complete(vista.callbacks.peticiones.editarPrivilegios.completo(id))
                    .send();
        }
    },
    utils: {
        templates: {
            item: function (objeto) {
                var url = __app.urlTo('privilegios/editar/' + objeto.id)
                return `<tr value="${objeto.id}" class="editar" style="cursor: pointer;">`
                        + '<td>' + objeto.entidad + '</td>'
                        + '<td>' + objeto.rol_id + '</td>'
                        + '<td>' + objeto.ver + '</td>'
                        + '<td>' + objeto.listar + '</td>'
                        + '<td>' + objeto.crear + '</td>'
                        + '<td>' + objeto.editar + '</td>'
                        + '<td>' + objeto.eliminar + '</td>'
                        + '<td>'
                        + '<div class="btn-group">'
                        + `<button value="${objeto.id}" class="btn-accion btn-info editar"><i class="icon_zoom-in_alt"></i></button>`
                        + `<button value="${objeto.id}" class="btn-accion btn-danger eliminar"><i class="icon_trash"></i></button>`
                        + '</div>'                        + '</td>'
                        + '</tr>';
            },
            consultando: function () {
                return '<tr><td colspan="6">Consultando...</td></tr>'
            },
            noHayRegistros: function () {
                return '<tr><td colspan="6">No hay registros...</td></tr>';
            }
        }
    },
};
$(vista.init);"""

    def register_privileges_js(self):
        """JS para mandar al backend los datos del formulario de privilegios
           cuando se crea un registro"""
        return """var vista = {
    controles: {
        formPrivilegios: $('#formPrivilegios'),
    },
    init: function () {
        vista.eventos();
    },
    eventos: function () {
        vista.controles.formPrivilegios.on('submit', vista.callbacks.eventos.accionesFormRegistro.ejecutar);
    },
    callbacks: {
        eventos: {
            accionesFormRegistro: {
                ejecutar: function (evento) {
                    __app.detenerEvento(evento);
                    var form = vista.controles.formPrivilegios;
                    var objeto = form.getFormData();
                    vista.peticiones.registrarPrivilegios(objeto);
                }
            }
        },
        peticiones: {
            beforeSend: function () {
                vista.controles.formPrivilegios.find('input,select,button').prop('disabled', true);
            },
            completo: function () {
                vista.controles.formPrivilegios.find('input,select,button').prop('disabled', false);
            },
            finalizado: function (respuesta) {
                if (__app.validarRespuesta(respuesta)) {
                    vista.controles.formPrivilegios.find('input').val('');
                    vista.controles.formPrivilegios.find('input').val('');
                    vista.controles.formPrivilegios.find('select').prop('selectedIndex', 0);
                    var id = respuesta.datos;
                    swal('Correcto', respuesta.mensaje, 'success', {
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                    }).then((value) => {
                        if (value) {
                            document.location = __app.urlTo(`privilegios/form/ver?id=${id}`);
                        }
                    });
                    return;
                }
                swal('Error', respuesta.mensaje, 'error', {
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                });
            }
        }
    },
    peticiones: {
        registrarPrivilegios: function (objeto) {
            __app.post(RUTAS_API.PRIVILEGIOS.REGISTRAR_PRIVILEGIOS, objeto)
                    .beforeSend(vista.callbacks.peticiones.beforeSend)
                    .complete(vista.callbacks.peticiones.completo)
                    .success(vista.callbacks.peticiones.finalizado)
                    .error(vista.callbacks.peticiones.finalizado)
                    .send();
        }
    }
};
$(vista.init);"""

    def update_roles_js(self):
        """JS para actualizar formulario de los roles del usuario"""
        return """var vista = {
    controles: {
        formRoles: $('#formRoles'),
        body: $('#bodyRoles'),
    },
    init: function () {
        vista.eventos();
        vista.controles.formRoles.find('input,select,textarea').prop('readonly', false).prop('disabled', false);
    },
    eventos: function () {
        vista.controles.formRoles.on('submit', vista.callbacks.eventos.accionesFormRegistro.ejecutar);
    },
    callbacks: {
        eventos: {
            accionesFormRegistro: {
                ejecutar: function (evento) {
                    evento.preventDefault();
                    evento.stopImmediatePropagation();
                    __app.detenerEvento(evento);
                    var form = vista.controles.formRoles;
                    var objeto = form.getFormData();
                    vista.peticiones.actualizarRoles(objeto);
                }
            }
        },
        peticiones: {
            beforeSend: function () {
                vista.controles.formRoles.find('input,select,textarea,button').prop('disabled', true);
            },
            completo: function () {
                vista.controles.formRoles.find('input,select,textarea,button').prop('disabled', false);
            },
            finalizado: function (respuesta) {
                if (__app.validarRespuesta(respuesta)) {
                    swal('Correcto', respuesta.mensaje, 'success', {
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                    }).then((value) => {
                        if (value) {
                            location.reload();
                        }
                    });
                    return;
                }
                swal('Error', respuesta.mensaje, 'error', {
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                });
            }
        }
    },
    peticiones: {
        actualizarRoles: function (objeto) {
            __app.post(RUTAS_API.ROLES.ACTUALIZAR_ROLES, objeto)
                    .beforeSend(vista.callbacks.peticiones.beforeSend)
                    .complete(vista.callbacks.peticiones.completo)
                    .success(vista.callbacks.peticiones.finalizado)
                    .error(vista.callbacks.peticiones.finalizado)
                    .send();
        }
    }
};
$(vista.init);"""

    def list_roles_js(self):
        """JS para listar los roles dinamicamente"""
        return """var vista = {
    controles: {
        tbodyListaRoles: $('#tablaListaRoles tbody')
    },
    init: function () {
        vista.eventos();
        vista.peticiones.listarRoles();
    },
    eventos: function () {
        vista.controles.tbodyListaRoles.on("click", ".eliminar", vista.callbacks.eventos.eliminarRegistro.ejecutar);
        vista.controles.tbodyListaRoles.on("click", ".editar", vista.callbacks.eventos.editarRegistro.ejecutar);
    },
    callbacks: {
        eventos: {
            eliminarRegistro: {
                ejecutar: function (evento) {
                    __app.detenerEvento(evento);
                    var value = $(this).attr("value");
                    var id = {"id": value};
                    swal({
                        title: '¿Seguro que desea eliminar el registro?',
                        text: "No podrá revertir este cambio",
                        icon: 'warning',
                        buttons: {
                            cancel: "Cancelar",
                            confirmar: {
                                text: "Confirmar",
                                value: true
                            },
                        },
                        dangerMode: true,
                    }).then((value) => {
                            if (value) {
                            vista.peticiones.eliminarRoles(id);
                        }
                    });
                }
            },
            editarRegistro: {
                ejecutar: function() {
                    var value = $(this).attr("value");
                    vista.peticiones.editarRoles(value);
                }
            }
        },
        peticiones: {
            listarRoles: {
                beforeSend: function () {
                    var tbody = vista.controles.tbodyListaRoles;
                    tbody.html(vista.utils.templates.consultando());
                },
                completo: function (respuesta) {
                    var tbody = vista.controles.tbodyListaRoles;
                    var datos = __app.parsearRespuesta(respuesta);
                    if (datos && datos.length > 0) {
                        tbody.html('');
                        for (var i = 0; i < datos.length; i++) {
                            var dato = datos[i];
                            tbody.append(vista.utils.templates.item(dato));
                        }
                    } else {
                        tbody.html(vista.utils.templates.noHayRegistros());
                    }
                }
            },
            eliminarRoles: {
                beforeSend: function () {
                    // Validación antes del click
                },
                completo: function () {
                    vista.peticiones.listarRoles();
                },
                finalizado: function (respuesta) {
                    if (__app.validarRespuesta(respuesta)) {
                        swal('Correcto', respuesta.mensaje, 'success', {
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                    });
                        return;
                    }
                    swal('Error', respuesta.mensaje, 'error', {
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                    });
                }
            },
            editarRoles: {
                completo: function (id) {
                    document.location = __app.urlTo(`roles/form/ver?id=${id}`);
                }
            }
        }
    },
    peticiones: {
        listarRoles: function () {
            __app.get(RUTAS_API.ROLES.LISTAR)
                    .beforeSend(vista.callbacks.peticiones.listarRoles.beforeSend)
                    .success(vista.callbacks.peticiones.listarRoles.completo)
                    .error(vista.callbacks.peticiones.listarRoles.completo)
                    .send();
        },
        eliminarRoles: function (id) {
            __app.post(RUTAS_API.ROLES.ELIMINAR, id)
                    .beforeSend(vista.callbacks.peticiones.eliminarRoles.beforeSend)
                    .complete(vista.callbacks.peticiones.eliminarRoles.completo)
                    .success(vista.callbacks.peticiones.eliminarRoles.finalizado)
                    .error(vista.callbacks.peticiones.eliminarRoles.finalizado)
                    .send();
        },
        editarRoles: function (id) {
            __app.post(RUTAS_API.ROLES.FORM_EDITAR, id)
                    .complete(vista.callbacks.peticiones.editarRoles.completo(id))
                    .send();
        }
    },
    utils: {
        templates: {
            item: function (objeto) {
                var url = __app.urlTo('roles/editar/' + objeto.id)
                return `<tr value="${objeto.id}" class="editar" style="cursor: pointer;">`
                        + '<td>' + objeto.nombre + '</td>'
                        + '<td>' + objeto.descripcion + '</td>'
                        + '<td>' + objeto.administrador + '</td>'
                        + '<td>' + objeto.activo + '</td>'
                        + '<td>'
                        + '<div class="btn-group">'
                        + `<button value="${objeto.id}" class="btn-accion btn-info editar"><i class="icon_zoom-in_alt"></i></button>`
                        + `<button value="${objeto.id}" class="btn-accion btn-danger eliminar"><i class="icon_trash"></i></button>`
                        + '</div>'                        + '</td>'
                        + '</tr>';
            },
            consultando: function () {
                return '<tr><td colspan="6">Consultando...</td></tr>'
            },
            noHayRegistros: function () {
                return '<tr><td colspan="6">No hay registros...</td></tr>';
            }
        }
    },
};
$(vista.init);"""

    def register_roles_js(self):
        """JS para mandar al backend los datos del formulario de roles
           cuando se crea un registro"""
        return """var vista = {
    controles: {
        formRoles: $('#formRoles'),
    },
    init: function () {
        vista.eventos();
    },
    eventos: function () {
        vista.controles.formRoles.on('submit', vista.callbacks.eventos.accionesFormRegistro.ejecutar);
    },
    callbacks: {
        eventos: {
            accionesFormRegistro: {
                ejecutar: function (evento) {
                    __app.detenerEvento(evento);
                    var form = vista.controles.formRoles;
                    var objeto = form.getFormData();
                    vista.peticiones.registrarRoles(objeto);
                }
            }
        },
        peticiones: {
            beforeSend: function () {
                vista.controles.formRoles.find('input,select,button').prop('disabled', true);
            },
            completo: function () {
                vista.controles.formRoles.find('input,select,button').prop('disabled', false);
            },
            finalizado: function (respuesta) {
                if (__app.validarRespuesta(respuesta)) {
                    vista.controles.formRoles.find('input').val('');
                    vista.controles.formRoles.find('input').val('');
                    vista.controles.formRoles.find('select').prop('selectedIndex', 0);
                    var id = respuesta.datos;
                    swal('Correcto', respuesta.mensaje, 'success', {
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                    }).then((value) => {
                        if (value) {
                            document.location = __app.urlTo(`roles/form/ver?id=${id}`);
                        }
                    });
                    return;
                }
                swal('Error', respuesta.mensaje, 'error', {
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                });
            }
        }
    },
    peticiones: {
        registrarRoles: function (objeto) {
            __app.post(RUTAS_API.ROLES.REGISTRAR_ROLES, objeto)
                    .beforeSend(vista.callbacks.peticiones.beforeSend)
                    .complete(vista.callbacks.peticiones.completo)
                    .success(vista.callbacks.peticiones.finalizado)
                    .error(vista.callbacks.peticiones.finalizado)
                    .send();
        }
    }
};
$(vista.init);"""

    def login_js(self):
        """JS que implementa las validaciones del login dinámicamente"""
        return """var vista = {
    controles: {
        formLogin: $('#formLogin'),
    },
    init: function () {
        vista.controles.formLogin.find('#correo,#password').val('');
        vista.eventos();
    },
    eventos: function () {
        vista.controles.formLogin.on('submit', vista.callbacks.eventos.accionesFormRegistro.ejecutar);
    },
    callbacks: {
        eventos: {
            accionesFormRegistro: {
                ejecutar: function (evento) {
                    __app.detenerEvento(evento);
                    var form = vista.controles.formLogin;
                    var objeto = form.getFormData();
                    vista.peticiones.validarLogin(objeto);
                }
            }
        },
        peticiones: {
            beforeSend: function () {
                vista.controles.formLogin.find('input').prop('disabled', true);
            },
            completo: function () {
                vista.controles.formLogin.find('input').prop('disabled', false);
            },
            finalizado: function (respuesta) {
                if (__app.validarRespuesta(respuesta)) {
                    vista.controles.formLogin.find('#correo,#password').val('');
                    swal('Correcto', respuesta.mensaje, 'success', {
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                    }).then((value) => {
                        if (value) {
                            document.location = __app.urlbase;
                        }
                    });
                    return;
                }
                swal('Error', respuesta.mensaje, 'error', {
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                });
            }
        }
    },
    peticiones: {
        validarLogin: function (objeto) {
            __app.post(RUTAS_API.USUARIOS.LOGIN, objeto)
                    .beforeSend(vista.callbacks.peticiones.beforeSend)
                    .complete(vista.callbacks.peticiones.completo)
                    .success(vista.callbacks.peticiones.finalizado)
                    .error(vista.callbacks.peticiones.finalizado)
                    .send();
        }
    }
};
$(vista.init);"""

# ==================================================
#
# RUTAS, CARGADOR AUTOMÁTICO DE CLASES Y LAUNCHER
#
# ==================================================

    def web_get(self):
        """Archivo que ensambla todas las rutas amigables GET Y POST utilizadas por
           el servidor (código PHP) y por el cliente (código JS)"""
        result = """<?php
Route::get("/", WelcomeController::class);

// LOGIN
Route::get("/login", LoginController::class."@index");
Route::get("/cerrar/sesion", LoginController::class."@cerrarSesion");
Route::post("/iniciar/sesion", LoginController::class."@crearSesion");

// VISTAS USUARIOS
Route::get("/usuarios", UsuariosController::class."@index");
Route::get("/listar_usuarios", UsuariosController::class); // Usado para colocar los datos a disposición en conexiones API
Route::get("/usuarios/form/crear", UsuariosController::class."@formCrearUsuarios");
Route::get("/usuarios/form/editar", UsuariosController::class."@formEditarUsuarios");
Route::get("/usuarios/form/ver", UsuariosController::class."@formVerUsuarios");
// RECURSOS USUARIOS
Route::post("/usuarios/registrar", UsuariosController::class."@insertarUsuarios");
Route::post("/usuarios/actualizar", UsuariosController::class."@actualizarUsuarios");
Route::post("/usuarios/eliminar", UsuariosController::class."@eliminarUsuarios");
Route::post("/usuarios/cambiar/clave", UsuariosController::class."@cambiarClave");
Route::post("/usuarios/actualizar/clave", UsuariosController::class."@actualizarClave");

// VISTAS ROLES
Route::get("/roles", RolesController::class."@index");
Route::get("/listar_roles", RolesController::class); // Usado para colocar los datos a disposición en conexiones API
Route::get("/roles/form/crear", RolesController::class."@formCrearRoles");
Route::get("/roles/form/editar", RolesController::class."@formEditarRoles");
Route::get("/roles/form/ver", RolesController::class."@formVerRoles");
// RECURSOS ROLES
Route::post("/roles/registrar", RolesController::class."@insertarRoles");
Route::post("/roles/actualizar", RolesController::class."@actualizarRoles");
Route::post("/roles/eliminar", RolesController::class."@eliminarRoles");

// VISTAS PRIVILEGIOS
Route::get("/privilegios", PrivilegiosController::class."@index");
Route::get("/listar_privilegios", PrivilegiosController::class); // Usado para colocar los datos a disposición en conexiones API
Route::get("/privilegios/form/crear", PrivilegiosController::class."@formCrearPrivilegios");
Route::get("/privilegios/form/editar", PrivilegiosController::class."@formEditarPrivilegios");
Route::get("/privilegios/form/ver", PrivilegiosController::class."@formVerPrivilegios");
// RECURSOS PRIVILEGIOS
Route::post("/privilegios/registrar", PrivilegiosController::class."@insertarPrivilegios");
Route::post("/privilegios/actualizar", PrivilegiosController::class."@actualizarPrivilegios");
Route::post("/privilegios/eliminar", PrivilegiosController::class."@eliminarPrivilegios");\n\n"""
        for sk in self.klass_names:
            data = self.class_name(sk)
            result += "// VISTAS %s\n" % (data.get('cname').upper())
            result += "Route::get(\"/%(lcname)s\", %(cname)sController::class.\"@index\");\n" % (data)
            result += "Route::get(\"/listar_%(lcname)s\", %(cname)sController::class); // Usado para colocar los datos a disposición en conexiones API\n" % (data)
            result += "Route::get(\"/%(lcname)s/form/crear\", %(cname)sController::class.\"@formCrear%(cname)s\");\n" % (data)
            result += "Route::get(\"/%(lcname)s/form/editar\", %(cname)sController::class.\"@formEditar%(cname)s\");\n" % (data)
            result += "Route::get(\"/%(lcname)s/form/ver\", %(cname)sController::class.\"@formVer%(cname)s\");\n" % (data)
            result += "// RECURSOS %s\n" % (data.get('cname').upper())
            result += "Route::post(\"/%(lcname)s/registrar\", %(cname)sController::class.\"@insertar%(cname)s\");\n" % (data)
            result += "Route::post(\"/%(lcname)s/actualizar\", %(cname)sController::class.\"@actualizar%(cname)s\");\n" % (data)
            result += "Route::post(\"/%(lcname)s/eliminar\", %(cname)sController::class.\"@eliminar%(cname)s\");\n\n" % (data)
        result += "?>\n"
        return result

    def autoloader_config_get(self):
        """Configura las rutas principales para llamar al autoloader"""
        return """<?php

    return [
        PATH_APP,
        PATH_SRC
    ];

?>\n"""

    def htaccess_get(self):
        """Declara el archivo de acceso de hipertexto para implementar las rutas amigables"""
        return """Options -Indexes
RewriteEngine on
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ index.php?uri=$1 [NC,L,QSA]"""

    def index_html(self):
        """Archivo que llama al lanzador de la aplicación"""
        return """<?php

require_once './src/launcher.php';

?>"""

# =============================
#
#   ARCHIVOS DE BASE DE DATOS
#
# =============================

    def mysql_get(self):
        """Crea dinámicamente una estructura sencilla sin validaciones para
           el SGBD MySQL"""
        result = """"""
        type_sql = dict(Char='varchar(255)',
                        Text='varchar(255)',
                        Integer='int(20)',
                        Float='int(20)',
                        Date='timestamp',
                        Datetime='timestamp',
                        Binary='binary',
                        Boolean='int(20)',
                        Many2one='int(11)',
                        Select='varchar(50)',
                        )
        for sk in self.klass_names:
            result += """CREATE TABLE IF NOT EXISTS `%s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,\n""" % sk
            for sa, attr in self.klasses[sk].attributes:
                attr_type = attr[0]
                if attr_type in type_sql.keys():
                    attr_type = type_sql.get(attr_type)
                    values = {'campo': sa, 'tipo': attr_type}
                    result += """  `%(campo)s` %(tipo)s DEFAULT NULL,\n""" % values
            result += """  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;\n\n"""
        return result

    def pgsql_get(self):
        """Crea dinámicamente una estructura robusta con las FN principales para
           el SGBD PostgreSQL"""
        result = """"""
        type_sql = dict(Char='CHARACTER VARYING',
                        Text='TEXT',
                        Integer='INTEGER',
                        Float='NUMERIC',
                        Date='DATE',
                        Datetime='timestamp without time zone',
                        Binary='BYTEA',
                        Boolean='BOOLEAN',
                        Many2one='INTEGER',
                        Select='CHARACTER VARYING',
                        )
        fk = """"""
        for sk in self.klass_names:
            result += """CREATE TABLE IF NOT EXISTS public.%s (
  id SERIAL,\n""" % sk
            for sa, attr in self.klasses[sk].attributes:
                attributes = self.format_attributes(attr[2])
                attributes.update({'tabla': sk, 'campo': sa})
                attr_type = attr[0]
                if attr_type in type_sql.keys():
                    attr_type = type_sql.get(attr_type)
                    values = {'campo': sa, 'tipo': attr_type}
                    result += """  %(campo)s %(tipo)s,\n""" % values
                if attr[0] == 'Many2one':
                    fk += """ALTER TABLE public.%(tabla)s ADD CONSTRAINT %(model)s_%(campo)s_fkey FOREIGN KEY (%(campo)s)
    REFERENCES public.%(model)s (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE SET NULL;\n\n""" % attributes
            result += """  CONSTRAINT %s_pkey PRIMARY KEY (id)\n);\n\n""" % (sk)
        result += fk
        return result

    def pgsql_login(self):
        """Estructura de las tablas en BD del login. Solo corre en el
           SGBD PostgreSQL"""
        return """CREATE TABLE IF NOT EXISTS public.usuarios (
  id SERIAL,
  nombre CHARACTER VARYING,
  correo CHARACTER VARYING,
  password CHARACTER VARYING,
  activo BOOLEAN,
  ultima_conexion TIMESTAMP,
  rol_id INTEGER,
  CONSTRAINT usuarios_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.roles (
  id SERIAL,
  nombre CHARACTER VARYING,
  descripcion TEXT,
  administrador BOOLEAN,
  activo BOOLEAN,
  CONSTRAINT roles_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.privilegios (
  id SERIAL,
  entidad CHARACTER VARYING,
  rol_id INTEGER,
  ver BOOLEAN,
  listar BOOLEAN,
  crear BOOLEAN,
  editar BOOLEAN,
  eliminar BOOLEAN,
  CONSTRAINT privilegios_pkey PRIMARY KEY (id)
);

ALTER TABLE public.privilegios ADD CONSTRAINT roles_rol_id_fkey FOREIGN KEY (rol_id)
    REFERENCES public.roles (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE SET NULL;

ALTER TABLE public.usuarios ADD CONSTRAINT roles_rol_id_fkey FOREIGN KEY (rol_id)
    REFERENCES public.roles (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE SET NULL;

INSERT INTO public.roles(
    id, nombre, descripcion, administrador, activo)
    VALUES (1, 'Administrador', 'Tiene privilegios CRUD en todo el sistema.', true, true);

INSERT INTO public.usuarios(
    nombre, correo, password, activo, rol_id)
    VALUES ('Administrador', 'admin@codegen.com', '$2y$10$JIGoJoQEB9.1/NZRW4Ht6eaQWZR0IfIMe3PztSQy2R0f1o.gJ39.G', true, 1);"""

# =============================
#
#   CONTROLADORES DINÁMICOS
#
# =============================

    def controller_get(self):
        """Ensambla los controladores PHP de la clases declaradas"""
        controllers = {}
        for sk in self.klass_names:
            result = """<?php\n\n"""
            data = self.class_name(sk)
            result += "class %(cname)sController extends Controller {\n\n" % data
            result += """    function __construct() {\n    }\n\n"""
            result += """    public function index() {
        if (!Permisos::permisosCRUD('%(ucname)s', 'listar')) {
            return $this->template('401');
        }
        return $this->view("%(lcname)s/welcome_%(lcname)s");
    }\n\n""" % data
            result += """    public function formCrear%(cname)s() {
        if (!Permisos::permisosCRUD('%(ucname)s', 'crear')) {
            return $this->template('401');
        }\n""" % data
            attr_m2o = False
            record_m2o = {}
            for sa, attr in self.klasses[sk].attributes:
                value = attr[2]
                attr_type = attr[0]
                attributes = self.format_attributes(value)
                attributes, attr_m2o, record_m2o = self.attributes_controller(attributes, data, attr_type, attr_m2o, sa)
                if attr_m2o:
                    pre_result = """"""
                    for i in record_m2o.items():
                        records = {'campo': i[0], 'cname': data.get('cname')}
                        pre_result += """        $%(campo)s = New %(cname)s();\n""" % records
                    pre_result += """        return $this->view("%(lcname)s/registrar_%(lcname)s",
                            array(\n""" % data
                    for i in record_m2o.items():
                        records = {'campo': i[0], 'cname': data.get('cname'), 'title': i[1][0]}
                        pre_result += """                                    "%(campo)s"=>$%(campo)s->get%(title)s(),\n""" % records
                    pre_result += """                                )
                        );
    }\n\n"""
                else:
                    pre_result = """        return $this->view("%(lcname)s/registrar_%(lcname)s");
    }\n\n""" % data
            result += pre_result
            # Se crean las llamadas al formulario de edición
            result += """    public function formEditar%(cname)s(Request $request) {
        if (!is_numeric($request->__get("id"))) { return $this->template('404'); }
        if (!Permisos::permisosCRUD('%(ucname)s', 'editar')) {
            return $this->template('401');
        }
        $id = intval($request->__get("id"));
        $%(lcname)sModel = new %(cname)s();
        $registro = $%(lcname)sModel->where("id", "=", $id)->get();\n""" % data
            attr_m2o = False
            record_m2o = {}
            for sa, attr in self.klasses[sk].attributes:
                value = attr[2]
                attr_type = attr[0]
                attributes = self.format_attributes(value)
                attributes, attr_m2o, record_m2o = self.attributes_controller(attributes, data, attr_type, attr_m2o, sa)
                if attr_m2o:
                    pre_result = """"""
                    for i in record_m2o.items():
                        records = {'campo': i[0], 'cname': data.get('cname')}
                        # pre_result += """        $%(campo)s = New %(cname)s();\n""" % records
                    pre_result += """        return $this->view("%(lcname)s/editar_%(lcname)s",
                            array(\n""" % {'cname': data.get('cname'), 'lcname': data.get('lcname'), }
                    for i in record_m2o.items():
                        records = {'campo': i[0], 'cname': data.get('cname'), 'lcname': data.get('lcname'), 'title': i[1][0]}
                        pre_result += """                                    "%(campo)s"=>$%(lcname)sModel->get%(title)s(),\n""" % records
                    pre_result += """                                    "%(lcname)s"=>$registro,
                                )
                        );
    }\n\n""" % ({'cname': data.get('cname'), 'lcname': data.get('lcname')})
                else:
                    pre_result = """        return $this->view("%(lcname)s/editar_%(lcname)s",
                            array(
                                    "%(lcname)s"=>$registro,
                                )
                        );
    }\n\n""" % ({'cname': data.get('cname'), 'lcname': data.get('lcname')})
            result += pre_result
            # Se crean los formularios de leer
            result += """    public function formVer%(cname)s(Request $request) {
        if (!is_numeric($request->__get("id"))) { return $this->template('404'); }
        if (!Permisos::permisosCRUD('%(ucname)s', 'ver')) {
            return $this->template('401');
        }
        $id = intval($request->__get("id"));
        $%(lcname)sModel = new %(cname)s();
        $registro = $%(lcname)sModel->where("id", "=", $id)->get();\n""" % ({'cname': data.get('cname'), 'lcname': data.get('lcname'), 'ucname': data.get('ucname')})
            attr_m2o = False
            record_m2o = {}
            for sa, attr in self.klasses[sk].attributes:
                value = attr[2]
                attr_type = attr[0]
                attributes = self.format_attributes(value)
                attributes, attr_m2o, record_m2o = self.attributes_controller(attributes, data, attr_type, attr_m2o, sa)
                if attr_m2o:
                    pre_result = """"""
                    for i in record_m2o.items():
                        records = {'campo': i[0], 'cname': data.get('cname')}
                        # pre_result += """        $%(campo)s = New %(cname)s();\n""" % records
                    pre_result += """        return $this->view("%(lcname)s/ver_%(lcname)s",
                            array(\n""" % data
                    for i in record_m2o.items():
                        records = {'campo': i[0], 'cname': data.get('cname'), 'lcname': data.get('lcname'), 'title': i[1][0]}
                        pre_result += """                                    "%(campo)s"=>$%(lcname)sModel->get%(title)s(),\n""" % records
                    pre_result += """                                    "%(lcname)s"=>$registro,
                                )
                        );
    }\n\n""" % data
                else:
                    pre_result = """        return $this->view("%(lcname)s/ver_%(lcname)s",
                            array(
                                    "%(lcname)s"=>$registro,
                                )
                        );
    }\n\n""" % data
            result += pre_result
            # Se crean las inserciones
            result += """    public function insertar%(cname)s(Request $request) {
        if (!csrfToken::validarCsrf()) {return new Respuesta(EMensajes::ERROR_INSERSION);}
        $%(lcname)sModel = new %(cname)s();
        $id = $%(lcname)sModel->insert($request->all());
        $insersionExitosa = ($id > 0);
        $respuesta = new Respuesta($insersionExitosa ? EMensajes::INSERCION_EXITOSA : EMensajes::ERROR_INSERSION);
        $respuesta->setDatos($id);
        return $respuesta;
    }\n\n""" % data
            # Se crea el listado de registros
            result += """    public function listar%(cname)s() {
        if (!Permisos::permisosCRUD('%(ucname)s', 'listar')) {
            header("Location:" . URL::to('login'));
        }
        $%(lcname)sModel = new %(cname)s();
        $lista = $%(lcname)sModel->get();
        $cantidad = count($lista);
        $respuesta = new Respuesta($cantidad ? EMensajes::CORRECTO : EMensajes::ERROR);
        if ($cantidad) {
            $lista = recName::parsearDatos($lista);\n""" % data
            attr_m2o = False
            record_m2o = {}
            pre_result = """"""
            for sa, attr in self.klasses[sk].attributes:
                value = attr[2]
                attr_type = attr[0]
                attributes = self.format_attributes(value)
                attributes, attr_m2o, record_m2o = self.attributes_controller(attributes, data, attr_type, attr_m2o, sa)
                if attr_m2o:
                    pre_result = """            $lista = recName::oneName($lista,
                                      array("""
                    for i in record_m2o.items():
                        records = {'campo': i[0], 'cname': data.get('cname')}
                    pre_result += """"""
                    for i in record_m2o.items():
                        records = {'campo': i[0], 'cname': data.get('cname'), 'lcname': data.get('lcname'), 'title': i[1][0]}
                        pre_result += """'%(campo)s' => 'id', """ % records
                    pre_result += """),
                                      array("""
                    for i in record_m2o.items():
                        records = {'campo': i[0], 'cname': data.get('cname'), 'lcname': data.get('lcname'), 'title': i[1][0]}
                        pre_result += """'%(campo)s' => $%(lcname)sModel->get%(title)s(), """ % records
                    pre_result += """)
                                    );\n"""
            result += pre_result
            result += """        }
        $respuesta->setDatos($lista);
        return $respuesta;
    }\n\n"""
            # Se crean las actualizaciones
            result += """    public function actualizar%(cname)s(Request $request) {
        if (!csrfToken::validarCsrf()) {return new Respuesta(EMensajes::ERROR_ACTUALIZACION);}
        $%(lcname)sModel = new %(cname)s();
        $actualizados = $%(lcname)sModel->where("id", "=", intval($request->__get("id")))->update($request->all());
        $check = ($actualizados > 0);
        $respuesta = new Respuesta($check ? EMensajes::ACTUALIZACION_EXITOSA : EMensajes::ERROR_ACTUALIZACION);
        return $respuesta;
    }\n\n""" % data
            # Se crean las eliminacion del registro
            result += """    public function eliminar%(cname)s(Request $request) {
        if (!Permisos::permisosCRUD('%(ucname)s', 'eliminar')) {
            return new Respuesta(EMensajes::SIN_PERMISOS);
        }
        $%(lcname)sModel = new %(cname)s();
        $id%(cname)s = intval($request->__get("id"));
        $eliminados = $%(lcname)sModel->where("id", "=", $id%(cname)s)->delete();
        $check = ($eliminados > 0);
        $respuesta = new Respuesta($check ? EMensajes::ELIMINACION_EXITOSA : EMensajes::ERROR_ELIMINACION);
        return $respuesta;
    }\n\n""" % data
            # Se crean las busquedas por ID
            result += """    public function buscar%(cname)sPorId($id%(cname)s) {
        $%(lcname)sModel = new %(cname)s();
        $%(lcname)s = $%(lcname)sModel->where("id", "=", $id%(cname)s)->first();
        $check = ($%(lcname)s != null);
        $respuesta = new Respuesta($check ? EMensajes::CORRECTO : EMensajes::NO_HAY_REGISTROS);
        return $respuesta;
    }\n\n""" % data
            result += "}\n"
            result += "?>\n"
            controllers.update({
                sk: {'contenido': result}
            })
        return controllers

# =============================
#
#   MODELOS DINÁMICOS
#
# =============================

    def models_get(self):
        """Ensambla los modelos PHP de la clases declaradas"""
        models = {}
        for sk in self.klass_names:
            result = """<?php\n\n"""
            data = self.class_name(sk)
            result += "class %(cname)s extends ModeloGenerico {\n" % data
            result += "    protected $id;\n"
            # Creando los atributos
            for sa, attr in self.klasses[sk].attributes:
                result += "    protected $%s;\n" % (sa)
            # Creando el constructor de la clase
            result += """\n    public function __construct($propiedades = null, $order_by = "id DESC") {
        parent::__construct("%(lcname)s", %(cname)s::class, $propiedades, $order_by);
    }\n\n""" % data
            # Creando las funciones get de los atributos
            result += """    function getId() {
        return $this->id;
    }\n\n"""
            for sa, attr in self.klasses[sk].attributes:
                attr_type = attr[0]
                attributes = self.format_attributes(attr[2])
                if 'model' in attributes:
                    model = attributes.get('model').replace('.', ' ').replace('_', ' ').title().replace(' ', '')
                    attributes.update({'title': sa.title().replace('_', ''),
                                       'campo': sa,
                                       'model': model
                                       })
                if attr_type == 'Many2one':
                    result += """    function get%(title)s() {
        $this->%(campo)s = New %(model)sController();
        return $this->%(campo)s->listar%(model)s()->datos;
    }\n""" % attributes
                else:
                    result += """    function get%s() {
        return $this->%s;
    }\n\n""" % (sa.title().replace('_', ''), sa)
            # Creando las funciones set de los atributos
            result += """    function setId($valor) {
        return $this->id;
    }\n\n"""
            for sa, attr in self.klasses[sk].attributes:
                result += """    function set%s($valor) {
        return $this->%s;
    }\n\n""" % (sa.title().replace('_', ''), sa)
            result += "}\n"
            result += "?>\n"
            # Llenando el diccionario del modelo
            models.update({
                sk: {'contenido': result}
            })
        return models

# =============================
#
#   VISTAS DINÁMICAS
#
# =============================

    def welcome_view_get(self):
        "Ensambla las vistas principales de cada clase donde se visualizan los registros"
        welcome = {}
        for sk in self.klass_names:
            data = self.class_name(sk)
            result = """<?= (new View())->includeFile(PATH_TEMPLATES . 'header'); ?>
    <!--Contenido start-->
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i>%(ucname)s</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="<?= URL::base() ?>">Inicio</a></li>
                    <li><i class="fa fa-bars"></i>%(ucname)s</li>
                </ol>
            </div>
        </div>
        <div class="row">
                <div class="card-body">
                    <div class="btn-group">
                        <a href="<?= URL::to("%(lcname)s/form/crear") ?>" class="btn btn-primary">Crear %(ucname)s</a>
                    </div>
                    <hr/>
                    <h2 class="card-title mb-4 text-dark"><b>Listar %(ucname)s</b></h4>
                    <table class="table table-condensed table-advance table-hover table-striped table-responsive-lg" width="100%(porcentaje)s" id="tablaLista%(cname)s">
                        <thead class="text-white bg-dark">
                            <tr>\n""" % data
            for sa, attr in self.klasses[sk].attributes:
                attributes = self.format_attributes(attr[2])
                result += "                                <th>%(string)s</th>\n" % attributes
            result += """                                <th><i class="icon_cogs"></i> Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="6">Consultando...</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

        </div>
        </section>
    </section>
    <!--Contenido end-->

    <script src="<?= URL::to("assets/plugins/jquery/jquery.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/plugins/sweetalert/sweetalert.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/helperform.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/rutas.api.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/app.global.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/modulos/%(lcname)s/listar.%(lcname)s.js") ?>" type="text/javascript"></script>
<?= (new View())->includeFile(PATH_TEMPLATES . 'footer'); ?>""" % data
            welcome.update({
                sk: {'contenido': result}
            })
        return welcome

    def create_view_get(self):
        "Se ensambla la vista de crear un registro en cada clase"
        create_view = {}
        for sk in self.klass_names:
            data = self.class_name(sk)
            result = """<?= (new View())->includeFile(PATH_TEMPLATES . 'header'); ?>
    <!--Contenido start-->
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i>%(ucname)s</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="<?= URL::base() ?>">Inicio</a></li>
                    <li><i class="fa fa-bars"></i><a href="<?= URL::to('%(lcname)s') ?>">%(ucname)s</a></li>
                    <li><i class="fa fa-bars"></i>Crear %(ucname)s</li>
                </ol>
            </div>
        </div>
        <div class="card-body">
        <h3 class="card-title mb-4 text-dark"><b>Insertar Registro</b></h3>
        <form id="form%(cname)s" action="%(lcname)s/registrar" method="POST">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <input type="hidden" id="_token" name="_token" value="<?= $_SESSION['csrf_token'] ?>"/>\n""" % data
            for sa, attr in self.klasses[sk].attributes:
                value = attr[2]
                attr_type = attr[0]
                result += self.atributte_format(sa, attr_type, value, data.get('lcname'))
            result += """                    </div>
                </div>
                </div>
                <div class="form-group text-right clearfix">
                    <button type="submit" class="btn btn-primary">
                        Guardar
                        <span class="fa fa-long-arrow-right" />
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
    </section>
    </section>
    <script src="<?= URL::to("assets/plugins/jquery/jquery.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/helperform.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/rutas.api.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/app.global.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/plugins/sweetalert/sweetalert.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/modulos/%(lcname)s/registrar.%(lcname)s.js") ?>" type="text/javascript"></script>
<?= (new View())->includeFile(PATH_TEMPLATES . 'footer'); ?>""" % data

            create_view.update({
                sk: {'contenido': result}
            })
        return create_view

    def watch_view_get(self):
        "Se ensambla la vista de ver un registro despues de que ha sido creado"
        watch_view = {}
        for sk in self.klass_names:
            data = self.class_name(sk)
            result = """<?= (new View())->includeFile(PATH_TEMPLATES . 'header'); ?>
    <!--Contenido start-->
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i>%(ucname)s</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="<?= URL::base() ?>">Inicio</a></li>
                    <li><i class="fa fa-bars"></i><a href="<?= URL::to('%(lcname)s') ?>">%(ucname)s</a></li>
                    <li><i class="fa fa-bars"></i>Ver %(ucname)s</li>
                </ol>
            </div>
        </div>
        <div class="container">
            <div class="card mt-5">
                <div id="body%(cname)s" class="card-body">
                <div class="btn-group float-right">
                    <a href="<?= URL::to("%(lcname)s/form/editar?id=" . $%(lcname)s[0]->id) ?>" class="btn btn-primary">Editar
                    </a>
                </div>
                <h3 class="card-title mb-4 text-dark"><b>Editar Registro</b></h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">\n""" % data
            for sa, attr in self.klasses[sk].attributes:
                value = attr[2]
                attr_type = attr[0]
                result += self.atributte_format(sa, attr_type, value, data.get('lcname'), True)
            result += """                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </section>
    </section>
    <!--Contenido end-->
    <script src="<?= URL::to("assets/plugins/jquery/jquery.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/plugins/sweetalert/sweetalert.js") ?>" type="text/javascript"></script>
<?= (new View())->includeFile(PATH_TEMPLATES . 'footer'); ?>""" % data

            watch_view.update({
                sk: {'contenido': result}
            })
        return watch_view

    def update_view_get(self):
        "Se ensambla la vista de actualizar un registro despues de que ha sido creado"
        update_view = {}
        for sk in self.klass_names:
            data = self.class_name(sk)
            result = """<?= (new View())->includeFile(PATH_TEMPLATES . 'header'); ?>
    <!--Contenido start-->
    <section id="main-content">
        <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa fa-bars"></i>%(ucname)s</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="<?= URL::base() ?>">Inicio</a></li>
                    <li><i class="fa fa-bars"></i><a href="<?= URL::to('%(lcname)s') ?>">%(ucname)s</a></li>
                    <li><i class="fa fa-bars"></i><a href="<?= URL::to("%(lcname)s/form/ver?id=" . $%(lcname)s[0]->id) ?>">Ver %(ucname)s</a></li>
                    <li><i class="fa fa-bars"></i>Editar %(ucname)s</li>
                </ol>
            </div>
        </div>
        <div class="container">
            <div class="card mt-5">
                <div id="body%(cname)s" class="card-body">
                <h3 class="card-title mb-4 text-dark"><b>Editar Registro</b></h3>
                <form id="form%(cname)s" action="%(lcname)s/actualizar" method="POST">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <input type="hidden" id="id" name="id" value="<?php print $%(lcname)s[0]->id ?>"/>
                                <input type="hidden" id="_token" name="_token" value="<?= $_SESSION['csrf_token'] ?>"/>\n""" % data
            for sa, attr in self.klasses[sk].attributes:
                value = attr[2]
                attr_type = attr[0]
                result += self.atributte_format(sa, attr_type, value, data.get('lcname'), True)
            result += """                            </div>
                        </div>
                        </div>
                        <div class="form-group text-right clearfix">
                            <button type="submit" class="btn btn-primary">
                                Guardar
                                <span class="fa fa-long-arrow-right" />
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </section>
    </section>
    <!--Contenido end-->
    <script src="<?= URL::to("assets/plugins/jquery/jquery.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/helperform.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/rutas.api.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/global/app.global.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/plugins/sweetalert/sweetalert.js") ?>" type="text/javascript"></script>
    <script src="<?= URL::to("assets/js/modulos/%(lcname)s/editar.%(lcname)s.js") ?>" type="text/javascript"></script>
<?= (new View())->includeFile(PATH_TEMPLATES . 'footer'); ?>""" % data

            update_view.update({
                sk: {'contenido': result}
            })
        return update_view

# =============================
#
#   JAVASCRIPT DINÁMICOS
#
# =============================

    def list_js_get(self):
        "Ensambla el javascript necesario para listar los registros de las clases"
        list_js = {}
        for sk in self.klass_names:
            data = self.class_name(sk)
            result = """var vista = {
    controles: {
        tbodyLista%(cname)s: $('#tablaLista%(cname)s tbody')
    },
    init: function () {
        vista.eventos();
        vista.peticiones.listar%(cname)s();
    },
    eventos: function () {
        vista.controles.tbodyLista%(cname)s.on("click", ".eliminar", vista.callbacks.eventos.eliminarRegistro.ejecutar);
        vista.controles.tbodyLista%(cname)s.on("click", ".editar", vista.callbacks.eventos.editarRegistro.ejecutar);
    },
    callbacks: {
        eventos: {
            eliminarRegistro: {
                ejecutar: function (evento) {
                    __app.detenerEvento(evento);
                    var value = $(this).attr("value");
                    var id = {"id": value};
                    swal({
                        title: '¿Seguro que desea eliminar el registro?',
                        text: "No podrá revertir este cambio",
                        icon: 'warning',
                        buttons: {
                            cancel: "Cancelar",
                            confirmar: {
                                text: "Confirmar",
                                value: true
                            },
                        },
                        dangerMode: true,
                    }).then((value) => {
                            if (value) {
                            vista.peticiones.eliminar%(cname)s(id);
                        }
                    });
                }
            },
            editarRegistro: {
                ejecutar: function() {
                    var value = $(this).attr("value");
                    vista.peticiones.editar%(cname)s(value);
                }
            }
        },
        peticiones: {
            listar%(cname)s: {
                beforeSend: function () {
                    var tbody = vista.controles.tbodyLista%(cname)s;
                    tbody.html(vista.utils.templates.consultando());
                },
                completo: function (respuesta) {
                    var tbody = vista.controles.tbodyLista%(cname)s;
                    var datos = __app.parsearRespuesta(respuesta);
                    if (datos && datos.length > 0) {
                        tbody.html('');
                        for (var i = 0; i < datos.length; i++) {
                            var dato = datos[i];
                            tbody.append(vista.utils.templates.item(dato));
                        }
                    } else {
                        tbody.html(vista.utils.templates.noHayRegistros());
                    }
                }
            },
            eliminar%(cname)s: {
                beforeSend: function () {
                    // Validación antes del click
                },
                completo: function () {
                    vista.peticiones.listar%(cname)s();
                },
                finalizado: function (respuesta) {
                    if (__app.validarRespuesta(respuesta)) {
                        swal('Correcto', respuesta.mensaje, 'success', {
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                    });
                        return;
                    }
                    swal('Error', respuesta.mensaje, 'error', {
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                    });
                }
            },
            editar%(cname)s: {
                completo: function (id) {
                    document.location = __app.urlTo(`%(lcname)s/form/ver?id=${id}`);
                }
            }
        }
    },
    peticiones: {
        listar%(cname)s: function () {
            __app.get(RUTAS_API.%(ucname)s.LISTAR)
                    .beforeSend(vista.callbacks.peticiones.listar%(cname)s.beforeSend)
                    .success(vista.callbacks.peticiones.listar%(cname)s.completo)
                    .error(vista.callbacks.peticiones.listar%(cname)s.completo)
                    .send();
        },
        eliminar%(cname)s: function (id) {
            __app.post(RUTAS_API.%(ucname)s.ELIMINAR, id)
                    .beforeSend(vista.callbacks.peticiones.eliminar%(cname)s.beforeSend)
                    .complete(vista.callbacks.peticiones.eliminar%(cname)s.completo)
                    .success(vista.callbacks.peticiones.eliminar%(cname)s.finalizado)
                    .error(vista.callbacks.peticiones.eliminar%(cname)s.finalizado)
                    .send();
        },
        editar%(cname)s: function (id) {
            __app.post(RUTAS_API.%(ucname)s.FORM_EDITAR, id)
                    .complete(vista.callbacks.peticiones.editar%(cname)s.completo(id))
                    .send();
        }
    },
    utils: {
        templates: {
            item: function (objeto) {
                var url = __app.urlTo('%(lcname)s/editar/' + objeto.id)
                return `<tr value="${objeto.id}" class="editar" style="cursor: pointer;">`\n""" % data
            for sa, attr in self.klasses[sk].attributes:
                result += """                        + '<td>' + objeto.%s + '</td>'\n""" % sa
            result += """                        + '<td>'
                        + '<div class="btn-group">'
                        + `<button value="${objeto.id}" class="btn-accion btn-info editar"><i class="icon_zoom-in_alt"></i></button>`
                        + `<button value="${objeto.id}" class="btn-accion btn-danger eliminar"><i class="icon_trash"></i></button>`
                        + '</div>'                        + '</td>'
                        + '</tr>';
            },
            consultando: function () {
                return '<tr><td colspan="6">Consultando...</td></tr>'
            },
            noHayRegistros: function () {
                return '<tr><td colspan="6">No hay registros...</td></tr>';
            }
        }
    },
};
$(vista.init);"""
            list_js.update({
                sk: {'contenido': result}
            })
        return list_js

    def create_js_get(self):
        "Ensambla el javascript necesario para crear los registros de las clases"
        create_js = {}
        for sk in self.klass_names:
            data = self.class_name(sk)
            result = """var vista = {
    controles: {
        form%(cname)s: $('#form%(cname)s'),
    },
    init: function () {
        vista.eventos();
    },
    eventos: function () {
        vista.controles.form%(cname)s.on('submit', vista.callbacks.eventos.accionesFormRegistro.ejecutar);
    },
    callbacks: {
        eventos: {
            accionesFormRegistro: {
                ejecutar: function (evento) {
                    __app.detenerEvento(evento);
                    var form = vista.controles.form%(cname)s;
                    var objeto = form.getFormData();
                    vista.peticiones.registrar%(cname)s(objeto);
                }
            }
        },
        peticiones: {
            beforeSend: function () {
                vista.controles.form%(cname)s.find('input,select,button').prop('disabled', true);
            },
            completo: function () {
                vista.controles.form%(cname)s.find('input,select,button').prop('disabled', false);
            },
            finalizado: function (respuesta) {
                if (__app.validarRespuesta(respuesta)) {
                    vista.controles.form%(cname)s.find('input').val('');
                    vista.controles.form%(cname)s.find('input').val('');
                    vista.controles.form%(cname)s.find('select').prop('selectedIndex', 0);
                    var id = respuesta.datos;
                    swal('Correcto', respuesta.mensaje, 'success', {
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                    }).then((value) => {
                        if (value) {
                            document.location = __app.urlTo(`%(lcname)s/form/ver?id=${id}`);
                        }
                    });
                    return;
                }
                swal('Error', respuesta.mensaje, 'error', {
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                });
            }
        }
    },
    peticiones: {
        registrar%(cname)s: function (objeto) {
            __app.post(RUTAS_API.%(ucname)s.REGISTRAR_%(ucname)s, objeto)
                    .beforeSend(vista.callbacks.peticiones.beforeSend)
                    .complete(vista.callbacks.peticiones.completo)
                    .success(vista.callbacks.peticiones.finalizado)
                    .error(vista.callbacks.peticiones.finalizado)
                    .send();
        }
    }
};
$(vista.init);""" % data
            create_js.update({
                sk: {'contenido': result}
            })
        return create_js

    def update_js_get(self):
        update_js = {}
        for sk in self.klass_names:
            data = self.class_name(sk)
            result = """var vista = {
    controles: {
        form%(cname)s: $('#form%(cname)s'),
        body: $('#body%(cname)s'),
    },
    init: function () {
        vista.eventos();
        vista.controles.form%(cname)s.find('input,select,textarea').prop('readonly', false).prop('disabled', false);
    },
    eventos: function () {
        vista.controles.form%(cname)s.on('submit', vista.callbacks.eventos.accionesFormRegistro.ejecutar);
    },
    callbacks: {
        eventos: {
            accionesFormRegistro: {
                ejecutar: function (evento) {
                    evento.preventDefault();
                    evento.stopImmediatePropagation();
                    __app.detenerEvento(evento);
                    var form = vista.controles.form%(cname)s;
                    var objeto = form.getFormData();
                    vista.peticiones.actualizar%(cname)s(objeto);
                }
            }
        },
        peticiones: {
            beforeSend: function () {
                vista.controles.form%(cname)s.find('input,select,textarea,button').prop('disabled', true);
            },
            completo: function () {
                vista.controles.form%(cname)s.find('input,select,textarea,button').prop('disabled', false);
            },
            finalizado: function (respuesta) {
                if (__app.validarRespuesta(respuesta)) {
                    swal('Correcto', respuesta.mensaje, 'success', {
                        closeOnClickOutside: false,
                        closeOnEsc: false,
                    }).then((value) => {
                        if (value) {
                            location.reload();
                        }
                    });
                    return;
                }
                swal('Error', respuesta.mensaje, 'error', {
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                });
            }
        }
    },
    peticiones: {
        actualizar%(cname)s: function (objeto) {
            __app.post(RUTAS_API.%(ucname)s.ACTUALIZAR_%(ucname)s, objeto)
                    .beforeSend(vista.callbacks.peticiones.beforeSend)
                    .complete(vista.callbacks.peticiones.completo)
                    .success(vista.callbacks.peticiones.finalizado)
                    .error(vista.callbacks.peticiones.finalizado)
                    .send();
        }
    }
};
$(vista.init);""" % data
            update_js.update({
                sk: {'contenido': result}
            })
        return update_js

# =============================
#
#   ENSAMBLAJE DEL ZIP
#
# =============================

    def end_render(self):
        controllers = self.controller_get()
        models = self.models_get()
        welcome_view = self.welcome_view_get()
        create_views = self.create_view_get()
        watch_views = self.watch_view_get()
        update_views = self.update_view_get()
        list_js = self.list_js_get()
        create_js = self.create_js_get()
        update_js = self.update_js_get()
        zip = zipfile.ZipFile(self.filename, 'w')
        filewrite = {'core/conexion.php': self.connection_BD(),
                     'core/crud.php': self.crud(),
                     'core/modelo_generico.php': self.generic_model(),
                     'core/EMensajes.php': self.emensajes_get(),
                     'core/rec_name.php': self.rec_name_get(),
                     'controllers/controller.php': self.controller_core_get(),
                     'controllers/login_controller.php': self.login_controller(),
                     'controllers/usuarios_controller.php': self.usuario_controller(),
                     'controllers/privilegios_controller.php': self.privilegios_controller(),
                     'controllers/roles_controller.php': self.roles_controller(),
                     'controllers/welcome_controller.php': self.welcome_controller_get(),
                     'models/usuarios_model.php': self.model_usuario(),
                     'models/privilegios_model.php': self.model_privilegios(),
                     'models/roles_model.php': self.model_roles(),
                     'views/login/welcome_login.php': self.welcome_login_view(),
                     'views/usuarios/cambiar_clave.php': self.cambiar_clave_view(),
                     'views/usuarios/editar_usuarios.php': self.editar_usuarios_view(),
                     'views/usuarios/registrar_usuarios.php': self.registrar_usuarios_view(),
                     'views/usuarios/ver_usuarios.php': self.ver_usuarios_view(),
                     'views/usuarios/welcome_usuarios.php': self.welcome_usuarios_view(),
                     'views/roles/editar_roles.php': self.editar_roles_view(),
                     'views/roles/registrar_roles.php': self.registrar_roles_view(),
                     'views/roles/ver_roles.php': self.ver_roles_view(),
                     'views/roles/welcome_roles.php': self.welcome_roles_view(),
                     'views/privilegios/editar_privilegios.php': self.editar_privilegios_view(),
                     'views/privilegios/registrar_privilegios.php': self.registrar_privilegios_view(),
                     'views/privilegios/ver_privilegios.php': self.ver_privilegios_view(),
                     'views/privilegios/welcome_privilegios.php': self.welcome_privilegios_view(),
                     'views/welcome.php': self.welcome_get(),
                     '../templates/header.php': self.header_get(),
                     '../templates/menus.php': self.menus_get(),
                     '../templates/footer.php': self.footer_get(),
                     '../templates/404.php': self.error_404_get(),
                     '../templates/401.php': self.error_401_get(),
                     '../src/roots.php': self.roots_get(),
                     '../src/launcher.php': self.launcher_get(),
                     '../src/autoloader/autoloader.php': self.autoloader_get(),
                     '../src/router/request.php': self.request_get(),
                     '../src/router/route.php': self.route_get(),
                     '../src/router/uri.php': self.uri_get(),
                     '../src/bin/respuesta.php': self.respuesta_get(),
                     '../src/bin/url.php': self.url_get(),
                     '../src/bin/view.php': self.view_get(),
                     '../src/security/csrf_token.php': self.csrf_token_get(),
                     '../src/security/xss_input.php': self.xss_input_get(),
                     '../src/security/permisos.php': self.privileges_get(),
                     '../assets/js/global/app.global.js': self.app_global_get(),
                     '../assets/js/global/helperform.js': self.helperform_get(),
                     '../assets/js/global/rutas.api.js': self.routes_api(),
                     '../assets/js/global/font.awesone.js': self.font_awesone_get(),
                     '../assets/js/panel/scripts.js': self.panel_scripts(),
                     '../assets/js/modulos/usuarios/cambiar.clave.js': self.change_password_js(),
                     '../assets/js/modulos/usuarios/editar.usuarios.js': self.update_user_js(),
                     '../assets/js/modulos/usuarios/listar.usuarios.js': self.list_user_js(),
                     '../assets/js/modulos/usuarios/registrar.usuarios.js': self.register_user_js(),
                     '../assets/js/modulos/privilegios/editar.privilegios.js': self.update_privileges_js(),
                     '../assets/js/modulos/privilegios/listar.privilegios.js': self.list_privileges_js(),
                     '../assets/js/modulos/privilegios/registrar.privilegios.js': self.register_privileges_js(),
                     '../assets/js/modulos/roles/editar.roles.js': self.update_roles_js(),
                     '../assets/js/modulos/roles/listar.roles.js': self.list_roles_js(),
                     '../assets/js/modulos/roles/registrar.roles.js': self.register_roles_js(),
                     '../assets/js/modulos/login.js': self.login_js(),
                     '../routes/web.php': self.web_get(),
                     '../config/autoloader.php': self.autoloader_config_get(),
                     '../.htaccess': self.htaccess_get(),
                     '../index.php': self.index_html(),
                     '../bd.sql': self.mysql_get(),
                     '../pgsql.sql': self.pgsql_get(),
                     '../pgsql_login.sql': self.pgsql_login(),
                     }
        for sk in self.klass_names:
            name_archivo = sk.replace('.', '_').replace(' ', '').lower()
            controller = controllers.get(sk).get('contenido')
            model = models.get(sk).get('contenido')
            welcome = welcome_view.get(sk).get('contenido')
            create_view = create_views.get(sk).get('contenido')
            watch_view = watch_views.get(sk).get('contenido')
            update_view = update_views.get(sk).get('contenido')
            list_ajax = list_js.get(sk).get('contenido')
            create_ajax = create_js.get(sk).get('contenido')
            update_ajax = update_js.get(sk).get('contenido')
            filewrite.update(
                {
                    'controllers/' + name_archivo + '_controller.php': controller,
                    'models/' + name_archivo + '_model.php': model,
                    'views/' + name_archivo + '/welcome_' + name_archivo + '.php': welcome,
                    'views/' + name_archivo + '/registrar_' + name_archivo + '.php': create_view,
                    'views/' + name_archivo + '/ver_' + name_archivo + '.php': watch_view,
                    'views/' + name_archivo + '/editar_' + name_archivo + '.php': update_view,
                    '../assets/js/modulos/' + name_archivo + '/listar.' + name_archivo + '.js': list_ajax,
                    '../assets/js/modulos/' + name_archivo + '/registrar.' + name_archivo + '.js': create_ajax,
                    '../assets/js/modulos/' + name_archivo + '/editar.' + name_archivo + '.js': update_ajax,
                }
            )
        for name, datastr in filewrite.items():
            info = zipfile.ZipInfo('app' + '/' + name)
            info.compress_type = zipfile.ZIP_DEFLATED
            info.external_attr = 2175008768
            zip.writestr(info, datastr)
        zip.close()
        ObjRenderer.end_render(self)

# dia-python keeps a reference to the renderer class and uses it on demand


dia.register_export("PyDia Generador de Código (PHP)", "zip", PHPRenderer())
